from typing import List, Union, Iterable

operators = {'+': 1, '*': 1}
operators_with_other_precedence = {'+': 2, '*': 1}


def parse(line_data: Iterable[str]) -> List[List[Union[str, int]]]:
    out = []

    for line in line_data:
        parts = line.split()
        tokens = []
        for token in parts:
            if len(token) == 1:
                tokens.append(token)
            else:
                for c in token:
                    tokens.append(c)
        out.append(tokens)

    return out


def create_rpn(expression: Iterable[str], same_precedence: bool = False) -> List[Union[str, int]]:
    output = []
    ops = []

    for token in expression:
        if token.isnumeric():
            output.append(int(token))
        elif token in operators:
            pop = []
            while True:
                if not ops:
                    break
                if ops[-1] not in operators:
                    break
                if same_precedence and (operators_with_other_precedence[ops[-1]] < operators_with_other_precedence[token]):
                    break
                if not same_precedence and (operators[ops[-1]] < operators[token]):
                    break
                pop.append(ops.pop(-1))
            output.extend(pop)
            ops.append(token)
        elif token == '(':
            ops.append(token)
        elif token == ')':
            while ops[-1] != '(':
                output.append(ops.pop(-1))
            ops.pop(-1)  # Discard the '(

    output.extend(reversed(ops))

    return output


def eval_rpn(rpn: Iterable[Union[str, int]]) -> int:
    stack = []
    for token in rpn:
        if isinstance(token, int):
            stack.append(token)
        elif stack:
            if token == '+':
                res = stack.pop(-1) + stack.pop(-1)
            elif token == '*':
                res = stack.pop(-1) * stack.pop(-1)
            else:
                raise ValueError(f"Unknown operator {token}, {stack}")

            stack.append(res)

    return stack.pop()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    expressions = parse(lines)

    total = 0
    for line in expressions:
        rpn = create_rpn(line)
        total += eval_rpn(rpn)
    print(f"Part 1: {total}")

    total = 0
    for line in expressions:
        rpn = create_rpn(line, same_precedence=True)
        total += eval_rpn(rpn)
    print(f"Part 2: {total}")
