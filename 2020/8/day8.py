from typing import List, Tuple


def parse(line_data: List[str]) -> List:
    prog = []

    for line in line_data:
        ins, val = line.split()
        prog.append((ins, int(val)))

    return prog


def run_until_loop(prog: List[Tuple[str, int]]) -> Tuple[bool, int]:
    acc = 0
    pc = 0

    visited = set()

    while True:
        if pc in visited:
            loop = True
            break

        visited.add(pc)

        ins = prog[pc]

        if ins[0] == 'nop':
            pass
        elif ins[0] == 'acc':
            acc += ins[1]
        elif ins[0] == 'jmp':
            pc += ins[1] - 1  # Offset -1 for the +1 later on

        pc += 1

        if pc == len(prog):
            loop = False
            break

    return loop, acc


def try_fix(prog: List[Tuple[str, int]]) -> int:
    for idx, line in enumerate(prog):
        old_line = line

        op, val = line
        if op == 'acc':
            continue
        elif val == 0:
            continue

        prog[idx] = ('jmp' if op == 'nop' else 'nop', val)

        fail_loop, accumulator = run_until_loop(prog)

        if fail_loop:
            prog[idx] = old_line
            continue
        else:
            # Note that the program is modified, and remains "fixed" upon returning
            return accumulator


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = parse(lines)

    print(f"Part 1: {run_until_loop(program)[1]}")

    print(f"Part 2: {try_fix(program)}")
