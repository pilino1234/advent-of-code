from collections import defaultdict
from typing import List, Tuple, Set

# Typing alias
Hex = Tuple[int, int, int]

directions = {
    "e": (1, -1, 0),
    "se": (0, -1, 1),
    "sw": (-1, 0, 1),
    "w": (-1, 1, 0),
    "nw": (0, 1, -1),
    "ne": (1, 0, -1)
}


def hex_nb6(hextile: Hex):
    for dir_ in directions:
        yield hex_neighbour(hextile, dir_)


def hex_neighbour(hextile: Hex, dir_):
    offsets = directions[dir_]
    return hextile[0] + offsets[0], hextile[1] + offsets[1], hextile[2] + offsets[2]


def parse(line_data: List[str]) -> List[List[str]]:
    parsed_directions = []

    for line in line_data:
        line_dirs = []
        while line:
            for direction in directions:
                if line.startswith(direction):
                    line_dirs.append(direction)
                    line = line.replace(direction, '', 1)
        parsed_directions.append(line_dirs)

    return parsed_directions


def flip_tiles(tiles_to_flip: List[List[str]]) -> Set[Hex]:
    black_tiles = set()
    reference = (0, 0, 0)  # type: Hex
    for directions in tiles_to_flip:
        cur = reference
        for step in directions:
            cur = hex_neighbour(cur, step)

        if cur not in black_tiles:
            black_tiles.add(cur)
        else:
            black_tiles.remove(cur)
    return black_tiles


def evolve(black_tiles: Set[Hex], days: int) -> Set[Hex]:
    for _ in range(days):
        nb_counts = defaultdict(int)
        for pos in black_tiles:
            for nb in hex_nb6(pos):
                nb_counts[nb] += 1

        new_floor = set()
        for pos in black_tiles:
            black_nbs = nb_counts.pop(pos, 0)
            if not (black_nbs == 0 or black_nbs > 2):
                new_floor.add(pos)
        for nb, black_nbs in nb_counts.items():
            if nb not in black_tiles and black_nbs == 2:
                new_floor.add(nb)

        black_tiles = new_floor
    return black_tiles


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    tile_dirs = parse(lines)

    floor = flip_tiles(tile_dirs)
    print("Part 1:", len(floor))

    evolved_floor = evolve(floor, days=100)
    print("Part 2:", len(evolved_floor))
