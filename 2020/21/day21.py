from typing import List, Dict, Set, Tuple


def parse(line_data: List[str]) -> Tuple[Dict[str, Set[str]], Set[str]]:
    parsed_allergens = dict()
    all_ingredients = set()

    for line in line_data:
        ingredient_part, _, allergen_part = line.partition('(')
        ingredients = ingredient_part.split()

        all_ingredients.update(set(ingredients))

        allergens = allergen_part[len('contains '):].rstrip(')').split(', ')

        for allg in allergens:
            if allg.strip() not in parsed_allergens:
                parsed_allergens[allg.strip()] = set(ingredients)
            else:
                parsed_allergens[allg.strip()] &= (set(ingredients))

    return parsed_allergens, all_ingredients


def count_safe_occurrences(allergens: Dict[str, Set[str]], all_ings: Set[str], data):

    safe_ingredients = all_ings - set(ing for potentially_unsafe_ings in allergens.values()
                                      for ing in potentially_unsafe_ings)

    definite = 0
    for line in data:
        ings, _, _ = line.partition('(')
        ings = ings.split()
        definite += sum(ing in safe_ingredients for ing in ings)

    return definite


def canonical_dangerous_ingredient_list(allg_to_ing):
    matched = {}

    while any(ings for ings in allg_to_ing.values()):
        for allg, ing in allg_to_ing.items():
            if len(ing) == 1:
                ingredient = ing.pop()
                matched[allg] = ingredient

                for allergen in allg_to_ing.keys():
                    if allergen != allg:
                        allg_to_ing[allergen].discard(ingredient)

    return ','.join(matched[allg] for allg in sorted(matched))


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    possible_allergens, all_ingredients = parse(lines)

    print("Part 1:", count_safe_occurrences(possible_allergens, all_ingredients, lines))

    print("Part 2:", canonical_dangerous_ingredient_list(possible_allergens))
