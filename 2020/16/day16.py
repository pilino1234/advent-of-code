import itertools
import re
from typing import List, Dict, Set, Tuple
from collections import defaultdict


def parse_constraints(line_data: List[str]) -> Dict[str, List[range]]:
    constraints = defaultdict(list)

    for line in line_data:
        if not line:
            break

        name, lo1, hi1, lo2, hi2 = re.fullmatch(r'(.+): (\d+)-(\d+) or (\d+)-(\d+)', line).groups()
        constraints[name.strip()].append(range(int(lo1), int(hi1) + 1))
        constraints[name.strip()].append(range(int(lo2), int(hi2) + 1))

    return constraints


def parse_tickets(line_data: List[str]) -> List[Tuple[int]]:
    tickets = []

    start_tickets = False
    for line in line_data:
        if not start_tickets:
            if 'nearby tickets' in line:
                start_tickets = True
            continue

        # nearby tickets
        tickets.append(tuple(map(int, line.split(','))))

    return tickets


def parse_my_ticket(line_data: List[str]) -> Tuple[int]:
    started = False
    for line in line_data:
        if not started:
            if 'your ticket' in line:
                started = True
                continue
            continue

        return tuple(map(int, line.split(',')))


def validate(ticket: Tuple[int], constraints: Dict[str, List[range]]) -> Tuple[bool, int]:
    for field in ticket:
        if not any(field in con for con in itertools.chain.from_iterable(constraints.values())):
            return False, field

    return True, 0


def find_fields(tickets: List[Tuple[int]], constraints: Dict[str, List[range]]) -> Dict[str, Set[int]]:
    matchings = defaultdict(set)

    for name, con in constraints.items():
        for field in range(len(tickets[0])):
            if all((ticket[field] in con[0]) or (ticket[field] in con[1]) for ticket in tickets):
                matchings[name].add(field)

    while any(len(s) > 1 for s in matchings.values()):
        for name, matches in matchings.items():
            if len(matches) == 1:
                for k2 in matchings.keys():
                    if k2 == name:
                        continue
                    matchings[k2].discard(next(iter(matches)))

    return matchings


def get_departure_fields_multiple(my_ticket: Tuple[int], matchings: Dict[str, Set[int]]):
    product = 1

    for field in matchings:
        if field.startswith('departure'):
            field_idx = next(iter(matchings[field]))
            product *= my_ticket[field_idx]

    return product


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    constraints = parse_constraints(lines)
    my_ticket = parse_my_ticket(lines)
    tickets = parse_tickets(lines)

    import time
    t1 = time.time()

    ticket_error_rate = 0
    failed_tickets = []
    for idx, ticket in enumerate(tickets):
        passed, failing_field = validate(ticket, constraints)
        if not passed:
            ticket_error_rate += failing_field
        failed_tickets.append(passed)

    print(f"Part 1: {ticket_error_rate}")
    print(time.time() - t1)

    t1 = time.time()
    # Throw away failed tickets
    tickets = list(itertools.compress(tickets, failed_tickets))

    field_names = find_fields(tickets, constraints)

    print(f"Part 2: {get_departure_fields_multiple(my_ticket, field_names)}")
    print(time.time() - t1)
