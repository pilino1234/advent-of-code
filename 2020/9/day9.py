from collections import deque
from itertools import accumulate
from typing import List


def parse_preamble(line_data: List[int], length: int = 25) -> List:
    return line_data[0:length]


def validate_sequence(nums: List[int], preamble_list: List[int]) -> int:
    working_list = preamble_list.copy()

    for next_number in nums[len(preamble_list):]:
        for num in working_list:
            remaining = next_number - num

            if remaining != num and remaining in working_list:
                # valid
                working_list.pop(0)
                working_list.append(next_number)
                break

        else:
            # invalid
            return next_number


def find_contiguous_sum2(nums: List[int], target: int) -> List[int]:
    partial_sums = [0] + list(accumulate(nums))

    for i in range(len(partial_sums)):
        j = i + 2
        while j < len(partial_sums) and partial_sums[j] - partial_sums[i] <= target:
            if partial_sums[j] - partial_sums[i] == target:
                return nums[i:j]
            j += 1


def find_contiguous_sum3(nums: List[int], target: int) -> List[int]:
    block = deque()
    block_sum = 0

    left = right = 0

    while block_sum != target:
        block.append(nums[right])
        block_sum += nums[right]
        right += 1

        if block_sum < target:
            continue

        while block_sum > target:
            left += 1
            block_sum -= block.popleft()

    return list(block)


if __name__ == '__main__':
    with open("input.txt") as file:
        numbers = list(map(int, [line.strip() for line in file]))

    preamble = parse_preamble(numbers, 25)

    p1 = validate_sequence(numbers, preamble)
    print(f"Part 1: {p1}")

    import time
    t1 = time.time()
    p2 = find_contiguous_sum2(numbers, p1)
    t2 = time.time()
    print(f"Part 2: {min(p2) + max(p2)}")
    print(t2 - t1)

    t1 = time.time()
    p2 = find_contiguous_sum3(numbers, p1)
    t2 = time.time()
    print(f"Part 2: {min(p2) + max(p2)}")
    print(t2 - t1)
