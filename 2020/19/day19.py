from typing import Dict, Iterable
import re


def parse_rules(line_data: Iterable[str]) -> Dict[str, str]:
    return {idx: rule.strip()
            for idx, _, rule in
            (line.partition(':') for line in line_data)}


def resolve_rule(num: str, rules: Dict[str, str], p2: bool = False) -> str:
    rule = rules[num]

    if p2:
        if num == '8':
            # Greedy match any number of rule 42
            return f"{resolve_rule('42', rules)}+"
        elif num == '11':
            # New 11 becomes 42 31 | 42 (42 31 | 42 (...) 31) 31, which matches
            # 42 42 42 ... 31 31 31, i.e. an arbitrary number of 42 followed by an arbitrary number of 31.
            r42 = resolve_rule('42', rules)
            r31 = resolve_rule('31', rules)
            # todo: find a better way than trial and error for this 5
            return f"({'|'.join(f'{r42}{{{x}}}{r31}{{{x}}}' for x in range(1, 5))})"

    if re.fullmatch(r'"[ab]"', rule):
        return rule.replace('\"', '')
    else:
        subrules = []
        for part in rule.split(' | '):
            subrules.append(''.join(resolve_rule(rule_num, rules, p2=p2) for rule_num in part.split()))
        return f"({'|'.join(subrules)})"


if __name__ == '__main__':
    with open("input.txt") as file:
        rule_lines, message_lines = file.read().split('\n\n')

    rules = parse_rules(rule_lines.splitlines())

    messages = message_lines.splitlines()

    rule = resolve_rule('0', rules)
    print(f"Part 1: {sum(bool(re.fullmatch(rule, msg)) for msg in messages)}")

    rule_p2 = resolve_rule('0', rules, p2=True)
    print(f"Part 2: {sum(bool(re.fullmatch(rule_p2, msg)) for msg in messages)}")
