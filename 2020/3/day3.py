from typing import List

from util import prod


def travel(area: List[str], dx: int, dy: int) -> int:
    width = len(area[0])
    target_y = len(area)

    x, y = 0, 0
    tree_count = 0

    while y < target_y:
        if area[y][x] == '#':
            tree_count += 1
        y += dy
        x += dx
        x %= width

    return tree_count


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    print(f"Part 1: {travel(lines, 3, 1)}")

    slopes = [
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2)
    ]

    results = [travel(lines, *slope) for slope in slopes]

    print(f"Part 2: {prod(results)}")
