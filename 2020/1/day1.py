from typing import List
from itertools import combinations

from util import to_ints


def find_sum_to(number_list: List[int], r: int, total: int) -> List[int]:
    for pair in combinations(number_list, r):
        if sum(pair) == total:
            return pair


if __name__ == "__main__":
    with open("input.txt") as file:
        numbers = to_ints([line.strip() for line in file])
    
    two_entries = find_sum_to(numbers, 2, 2020)
    print(f"Part 1: {two_entries[0] * two_entries[1]}")

    three_entries = find_sum_to(numbers, 3, 2020)
    print(f"Part 2: {three_entries[0] * three_entries[1] * three_entries[2]}")

