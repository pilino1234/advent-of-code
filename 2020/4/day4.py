import re
from typing import List, Dict, Set

VALID_KEYS = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid'}
VALID_EYE_COLS = 'amb blu brn gry grn hzl oth'.split()


def parse(passport_data: List[str]) -> List[Dict[str, str]]:
    parsed = []

    current = {}
    for line in passport_data:
        if line.strip() == '':
            parsed.append(current)
            current = {}
            continue
        else:
            fields = line.split()
            for f in fields:
                k, v = f.split(':')
                current[k] = v.strip()

    parsed.append(current)

    return parsed


def validate(passport: Dict[str, str], optional: Set[str] = ('cid',), validate_contents: bool = False) -> bool:
    present_keys = set(passport.keys())
    diff = VALID_KEYS.difference(present_keys)

    for k in optional:
        diff.discard(k)

    if validate_contents and not diff:
        return all(validate_field(k, v) for k, v in passport.items())
    else:
        return not diff


def validate_field(key: str, value: str) -> bool:
    if key == 'byr':
        return 1920 <= int(value) <= 2002
    elif key == 'iyr':
        return 2010 <= int(value) <= 2020
    elif key == 'eyr':
        return 2020 <= int(value) <= 2030
    elif key == 'hgt':
        if value.endswith('cm'):
            return 150 <= int(value[:-2]) <= 193
        elif value.endswith('in'):
            return 59 <= int(value[:-2]) <= 76
        else:
            return False
    elif key == 'hcl':
        return bool(re.fullmatch(r'#[0-9a-f]{6}', value))
    elif key == 'ecl':
        return value in VALID_EYE_COLS
    elif key == 'pid':
        return bool(re.fullmatch(r'[0-9]{9}', value))

    return True


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    passports = parse(lines)

    print(f"Part 1: {sum(validate(pport) for pport in passports)}")
    print(f"Part 2: {sum(validate(pport, validate_contents=True) for pport in passports)}")
