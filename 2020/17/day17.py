from collections import defaultdict
from typing import List, Tuple


def parse_active_coords(line_data: List[str], ndims: int = 3) -> List[Tuple[int, ...]]:
    grid = []

    extra_coords = (0,) * (ndims - 2)
    for y, line in enumerate(line_data):
        for x, char in enumerate(line):
            if char == '#':
                grid.append((x, y) + extra_coords)

    return grid


neighbours26 = list((x, y, z) for x in range(-1, 2) for y in range(-1, 2) for z in range(-1, 2))
neighbours26.remove((0, 0, 0))
neighbours80 = list((x, y, z, w) for x in range(-1, 2) for y in range(-1, 2) for z in range(-1, 2) for w in range(-1, 2))
neighbours80.remove((0, 0, 0, 0))


def nb80(pos: Tuple[int, int, int, int]):
    return [(pos[0] + dx, pos[1] + dy, pos[2] + dz, pos[3] + dw) for dx, dy, dz, dw in neighbours80]


def nb26(pos: Tuple[int, ...]):
    return [(pos[0] + dx, pos[1] + dy, pos[2] + dz) for dx, dy, dz in neighbours26]


def nbAny(pos: Tuple[int, ...]):
    if len(pos) == 3:
        return nb26(pos)
    elif len(pos) == 4:
        return nb80(pos)


def evolve(previous_active: List[Tuple[int, ...]]):
    active_nb_counts = defaultdict(int)

    for pos in previous_active:
        for nb in nbAny(pos):
            active_nb_counts[nb] += 1

    next_active = []
    for pos in previous_active:
        if active_nb_counts.pop(pos, 0) in (2, 3):
            next_active.append(pos)
    for pos, count in active_nb_counts.items():
        if count == 3:
            next_active.append(pos)
    return next_active


def pprint_3d(grid):
    min_x = min(grid.keys(), key=lambda t: t[0])[0]
    min_y = min(grid.keys(), key=lambda t: t[1])[1]
    min_z = min(grid.keys(), key=lambda t: t[2])[2]

    max_x = max(grid.keys(), key=lambda t: t[0])[0]
    max_y = max(grid.keys(), key=lambda t: t[1])[1]
    max_z = max(grid.keys(), key=lambda t: t[2])[2]

    for z in range(min_z, max_z+1):
        print(f"\nz={z}")
        for y in range(min_y, max_y+1):
            for x in range(min_x, max_x+1):
                print('#' if grid[(x, y, z)] else '.', end='')
            print()
    print()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    import time
    t1 = time.time()
    active_coords = parse_active_coords(lines)

    for _ in range(6):
        active_coords = evolve(active_coords)
    print(f"Part 1: {len(active_coords)}")
    print(time.time() - t1)

    t1 = time.time()
    active_coords = parse_active_coords(lines, ndims=4)

    for _ in range(6):
        active_coords = evolve(active_coords)
    print(f"Part 2: {len(active_coords)}")
    print(time.time() - t1)
