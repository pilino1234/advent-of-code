from collections import defaultdict
from functools import lru_cache
from typing import List, Dict, Tuple


def build_full_adapter_chain(adapters: List[int]) -> Dict[int, int]:
    diffs = defaultdict(int)
    current = 0

    for ad in adapters:
        diff = ad - current
        diffs[diff] += 1
        current = ad

    diffs[3] += 1

    return diffs


@lru_cache(maxsize=None)
def build_all_chains(adapters: Tuple[int], start: int, goal: int) -> int:
    chains = 0

    # Add the last step of the chain
    if goal - start <= 3:
        chains += 1

    if not adapters:
        return chains

    if adapters[0] - start <= 3:
        chains += build_all_chains(adapters[1:], adapters[0], goal)

    chains += build_all_chains(adapters[1:], start, goal)

    return chains


def build_all_chains2(adapters: List[int]) -> Dict[int, int]:
    distances = defaultdict(int)
    distances[max(adapters) + 3] = 1

    for ad in reversed([0] + adapters):
        distances[ad] = distances[ad + 1] + distances[ad + 2] + distances[ad + 3]

    return distances


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = list(map(int, (line.strip() for line in file)))

    lines.sort()
    differences = build_full_adapter_chain(lines)
    print(f"Part 1: {differences[1] * differences[3]}")

    import time

    t1 = time.time()
    print(f"Part 2: {build_all_chains(tuple(lines), 0, max(lines) + 3)}")
    print(time.time() - t1)
    print(build_all_chains.cache_info())

    print('---')

    t1 = time.time()
    print(f"Part 2: {build_all_chains2(lines)[0]}")
    print(time.time() - t1)
