
"""
Only solves part 1 using a shortcut
"""

from typing import List
from collections import namedtuple, defaultdict

from util import extract_all_numbers, prod

Tile = namedtuple('Tile', ['ID', 'image'])


def parse(tile_data: List[str]) -> List:
    parsed = []

    for tile in tile_data:
        lines = tile.splitlines()
        [id_] = extract_all_numbers(lines[0])
        parsed.append(Tile(id_, lines[1:]))

    return parsed


def get_borders(tiles: List[Tile]):
    border_occurrences = defaultdict(list)

    for tile in tiles:
        # top
        border_occurrences[tile.image[0]].append(tile.ID)
        border_occurrences[tile.image[0][::-1]].append(tile.ID)

        # bottom
        border_occurrences[tile.image[-1]].append(tile.ID)
        border_occurrences[tile.image[-1][::-1]].append(tile.ID)

        # left
        border_occurrences[''.join(row[0] for row in tile.image)].append(tile.ID)
        border_occurrences[''.join(row[0] for row in reversed(tile.image))].append(tile.ID)

        # right
        border_occurrences[''.join(row[-1] for row in tile.image)].append(tile.ID)
        border_occurrences[''.join(row[-1] for row in reversed(tile.image))].append(tile.ID)

    return border_occurrences


if __name__ == '__main__':
    with open("input.txt") as file:
        tile_data = file.read().split('\n\n')

    tiles = parse(tile_data)

    borders = get_borders(tiles)

    counts = defaultdict(int)
    for border in borders:
        if len(borders[border]) == 1:
            counts[borders[border][0]] += 1

    corners = [id_ for id_, count in counts.items() if count == 4]

    print(prod(corners))

