from collections import defaultdict
from typing import List, Dict, Set, Tuple

from util import extract_all_numbers


SEA_MONSTER = """                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """


def parse(tile_data: List[str]) -> Dict[int, List[str]]:
    parsed = {}

    for tile in tile_data:
        lines = tile.splitlines()
        [tile_id] = extract_all_numbers(lines[0])
        parsed[tile_id] = lines[1:]

    return parsed


def edges(tile: List[str]):
    return [
        ''.join(line[0] for line in tile)[::-1],  # Left
        tile[0],  # Top
        ''.join(line[-1] for line in tile),  # Right
        tile[-1][::-1],  # Bottom
    ]


def min_edge(edge: str):
    return min(edge, edge[::-1])


def process_edges(tiles: Dict[int, List[str]]) -> Tuple[Dict[str, int], Dict[str, List[int]]]:
    edge_counts = defaultdict(int)
    edge_to_tile_map = defaultdict(list)

    for tile_id, tile in tiles.items():
        for edge in edges(tile):
            minimal_edge = min_edge(edge)
            edge_counts[minimal_edge] += 1
            edge_to_tile_map[minimal_edge].append(tile_id)

    return edge_counts, edge_to_tile_map


def flip(tile: List[str]) -> List[str]:
    return [line[::-1] for line in tile]


def rotate(tile: List[str]) -> List[str]:
    return [
        ''.join(line[-1 - idx] for line in tile)
        for idx, row in enumerate(tile)
    ]


def find_and_place_corner(tiles: Dict[int, List[str]], edge_counts: Dict[str, int], only_find_corners=False):
    used = set()
    assembled_img = [[]]

    if only_find_corners:  # Part 1
        corner_id_product = 1

    for tile_id, tile in tiles.items():
        unmatched_edges = 0
        tile_edges = edges(tile)
        for edge in tile_edges:
            if edge_counts[min_edge(edge)] == 1:
                unmatched_edges += 1
        if unmatched_edges == 2:
            if only_find_corners:  # Part 1
                corner_id_product *= tile_id
                continue

            # Part 2 runs this instead
            # Fit a corner to the top left of the image
            tile_img = tile
            for _ in range(4):
                # If the top and left edge are not matched anywhere else, this tile is the corner
                if edge_counts[min_edge(tile_edges[0])] == 1 and edge_counts[min_edge(tile_edges[1])] == 1:
                    assembled_img[0].append(tile_img)
                    used.add(tile_id)
                    return assembled_img, used

                # Flip the tile and check again
                tile_img = flip(tile_img)
                if edge_counts[min_edge(tile_edges[0])] == 1 and edge_counts[min_edge(tile_edges[1])] == 1:
                    assembled_img[0].append(tile_img)
                    used.add(tile_id)
                    return assembled_img, used

                # Flip back and rotate for next loop iteration
                tile_img = flip(tile_img)
                tile_img = rotate(tile_img)

    if only_find_corners:  # Part 1
        return corner_id_product


def fill_first_row(started_assembly: List[List[List[str]]], tiles: Dict[int, List[str]],
                   used: Set[int], edge_to_tile: Dict[str, List[int]]):
    while True:
        latest_tile = started_assembly[0][-1]
        right_edge = edges(latest_tile)[2][::-1]

        next_matching_tile_id = next(
            (tile for tile in edge_to_tile[min_edge(right_edge)] if tile not in used),
            None
        )

        if not next_matching_tile_id:
            # End of the row, no more matching tiles
            break

        next_tile = tiles[next_matching_tile_id]
        for _ in range(4):
            # Flip and rotate the tile until the edge matches exactly
            if edges(next_tile)[0] == right_edge:
                break
            # flip and try again
            next_tile = flip(next_tile)
            if edges(next_tile)[0] == right_edge:
                break

            next_tile = flip(next_tile)
            next_tile = rotate(next_tile)

        # Next tile is oriented properly now
        started_assembly[0].append(next_tile)
        used.add(next_matching_tile_id)
    return started_assembly, used


def fill_img(first_row_img: List[List[List[str]]], tiles: Dict[int, List[str]],
             used: Set[int], edge_to_tile: Dict[str, List[int]]):
    # Fill the rest of the rows until there are no more tiles remaining
    while len(used) < len(tiles):
        new_row = []

        # From each tile so far, fill another row
        for tile in first_row_img[-1]:
            bottom_edge = edges(tile)[3][::-1]

            next_matching_tile_id = next(
                (tile for tile in edge_to_tile[min_edge(bottom_edge)] if tile not in used),
                None
            )

            # End of the row, no more matching tiles
            if not next_matching_tile_id:
                break

            next_tile = tiles[next_matching_tile_id]
            for _ in range(4):
                # Flip and rotate the tile until the edge matches exactly
                if edges(next_tile)[1] == bottom_edge:
                    break
                # flip and try again
                next_tile = flip(next_tile)
                if edges(next_tile)[1] == bottom_edge:
                    break

                next_tile = flip(next_tile)
                next_tile = rotate(next_tile)

            # Next tile is oriented properly now
            new_row.append(next_tile)
            used.add(next_matching_tile_id)
        first_row_img.append(new_row)

    return first_row_img, used


def combine(filled_img: List[List[List[str]]]):
    final = []

    for tile_row in filled_img:
        combined_row = []

        for row in range(1, len(tile_row[0][0])-1):
            combined_row.append(''.join(t[row][1:-1] for t in tile_row))
        final.extend(combined_row)

    return final


def find_pattern(img: List[str], pattern: List[str]) -> Tuple[List[str], int]:
    def match(img, pattern) -> int:
        count = 0

        for row_num, row in enumerate(img):
            for col_num, col in enumerate(row):

                found = True

                for mrow_num, monster_row in enumerate(pattern):
                    for mcol_num, monster_col in enumerate(monster_row):
                        if monster_col != '#':
                            continue

                        try:
                            if img[row_num + mrow_num][col_num + mcol_num] != '#':
                                found = False
                        except IndexError:
                            found = False
                            break

                if found:
                    # print("Found at", row_num, col_num)
                    count += 1
        return count

    for _ in range(4):
        matches = match(img, pattern)
        if matches:
            return img, matches
        img = flip(img)
        matches = match(img, pattern)
        if matches:
            return img, matches
        img = flip(img)
        img = rotate(img)


if __name__ == '__main__':
    with open("input.txt") as file:
        tile_data = file.read().split('\n\n')

    tiles = parse(tile_data)

    edge_counts, edge_to_tile_map = process_edges(tiles)

    print("Part 1:", find_and_place_corner(tiles, edge_counts, only_find_corners=True))

    only_corner, used = find_and_place_corner(tiles, edge_counts)

    first_row, used = fill_first_row(only_corner, tiles, used, edge_to_tile_map)

    filled_img, used = fill_img(first_row, tiles, used, edge_to_tile_map)

    final_img = combine(filled_img)

    aligned_img, monster_sightings = find_pattern(final_img, SEA_MONSTER.splitlines())
    # print('\n'.join(row for row in aligned_img))

    print("Part 2:", ''.join(row for row in aligned_img).count('#') - SEA_MONSTER.count('#') * monster_sightings)



