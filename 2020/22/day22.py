from typing import List, Tuple


def parse_cards(line_data: List[str]) -> Tuple[List[int], List[int]]:
    c1 = list(map(int, line_data[0].splitlines()[1:]))
    c2 = list(map(int, line_data[1].splitlines()[1:]))
    return c1, c2


def play_combat(c1: List[int], c2: List[int]) -> List[int]:
    while c1 and c2:
        p1 = c1.pop(0)
        p2 = c2.pop(0)

        if p1 > p2:
            c1.extend([p1, p2])
        elif p2 > p1:
            c2.extend(([p2, p1]))
        else:
            assert False

    return c1 if c1 else c2


def rec_combat(c1: List[int], c2: List[int]) -> Tuple[int, List[int]]:
    seen = set()

    while c1 and c2:
        state = (tuple(c1), tuple(c2))
        if state in seen:
            return 1, c1
        else:
            seen.add(state)

        p1 = c1.pop(0)
        p2 = c2.pop(0)

        if p1 <= len(c1) and p2 <= len(c2):
            winner, _ = rec_combat(c1[:p1], c2[:p2])
            if winner == 1:
                c1.extend([p1, p2])
            elif winner == 2:
                c2.extend([p2, p1])
            else:
                assert False
        else:
            if p1 > p2:
                c1.extend([p1, p2])
            elif p2 > p1:
                c2.extend(([p2, p1]))
            else:
                assert False
    return (1, c1) if c1 else (2, c2)


def calc_score(cards: List[int]):
    return sum(pos * card for pos, card in enumerate(reversed(cards), start=1))


if __name__ == '__main__':
    with open("input.txt") as file:
        card_data = file.read().split('\n\n')

    player_cards = parse_cards(card_data)
    winning_deck = play_combat(*player_cards)
    print("Part 1:", calc_score(winning_deck))

    player_cards = parse_cards(card_data)
    winner, winning_deck = rec_combat(*player_cards)
    print("Part 2:", calc_score(winning_deck))
