from typing import List, Tuple, Optional


class Ship:
    def __init__(self, x, y, dir_):
        self.x = x
        self.y = y
        self.facing = dir_

    def __str__(self):
        return f"Ship @({self.x}, {self.y}), facing {self.facing}"

    @property
    def distance(self):
        return abs(self.x) + abs(self.y)


def parse(line_data: List[str]) -> List:
    return [(line[0], int(line[1:])) for line in line_data]


def drive(ship: Ship, actions: List[Tuple[str, int]], waypoint: Optional[Ship]):
    entity = waypoint if waypoint else ship

    for act, num in actions:
        if act == 'N':
            entity.y += num
        elif act == 'S':
            entity.y -= num
        elif act == 'E':
            entity.x += num
        elif act == 'W':
            entity.x -= num
        elif act == 'L':
            if waypoint:
                for _ in range(num // 90):
                    waypoint.x, waypoint.y = -waypoint.y, waypoint.x
            else:
                ship.facing += num
                ship.facing %= 360
        elif act == 'R':
            if waypoint:
                for _ in range(num // 90):
                    waypoint.x, waypoint.y = waypoint.y, -waypoint.x
            else:
                ship.facing -= num
                ship.facing %= 360
        elif act == 'F':
            if waypoint:
                for _ in range(num):
                    ship.x += waypoint.x
                    ship.y += waypoint.y
            else:
                if entity.facing == 0:
                    entity.x += num
                elif entity.facing == 90:
                    entity.y += num
                elif entity.facing == 180:
                    entity.x -= num
                elif entity.facing == 270:
                    entity.y -= num
        # print(ship, waypoint)
    return ship


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    actions = parse(lines)

    ship = Ship(0, 0, 0)
    destination = drive(ship, actions, None)
    print(f"Part 1: {destination.distance}")

    ship = Ship(0, 0, 0)
    waypoint = Ship(10, 1, 0)
    destination = drive(ship, actions, waypoint)
    print(f"Part 2: {destination.distance}")
