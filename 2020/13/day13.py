import math
from typing import List, Tuple

from util import prod


def parse(line_data: List[str]) -> Tuple[int, List[str]]:
    return int(line_data[0]), line_data[1].split(',')


def crt(n, a):
    nprod = prod(n)
    sum_ = 0
    for n_i, a_i in zip(n, a):
        sum_ += a_i * (nprod // n_i) * mod_inv((nprod // n_i), n_i)
    return sum_ % nprod


def mod_inv(num, mod):
    prev_r = num
    prev_s = 1

    r = mod
    s = 0

    while r != 0:
        q = prev_r // r
        prev_r, r = r, prev_r - q * r
        prev_s, s = s, prev_s - q * s

    return prev_s


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    timestamp, buses = parse(lines)

    first = None
    lowest = 1e10
    for bus in buses:
        if bus == 'x':
            continue
        id_ = int(bus)
        q = math.ceil(timestamp / id_)
        arrival = id_ * q
        if arrival - timestamp < lowest:
            first = bus
            lowest = arrival - timestamp

    print(f"Part 1: {int(first) * lowest}")

    n_list = [int(bus) for bus in buses if bus.isnumeric()]
    a_list2 = [(-offset) % int(bus) for offset, bus in zip(range(len(buses)), buses) if bus != 'x']

    print(f"Part 2: {crt(n_list, a_list2)}")
