from typing import List


def play(starting_numbers: List[int], rounds: int = 2020) -> List:
    recent_spoke = {}
    before_that_spoke = {}

    round_num = 0
    last_spoken = None

    while round_num < rounds:
        if round_num % 100000 == 0:
            print(round_num)
        if round_num < len(starting_numbers):
            to_speak = starting_numbers[round_num]
        else:
            if last_spoken in before_that_spoke:
                to_speak = recent_spoke[last_spoken] - before_that_spoke[last_spoken]
            else:
                to_speak = 0

        if to_speak in recent_spoke:
            before_that_spoke[to_speak] = recent_spoke[to_speak]
        recent_spoke[to_speak] = round_num

        last_spoken = to_speak
        round_num += 1

    return last_spoken


if __name__ == '__main__':
    with open("input.txt") as file:
        numbers = list(map(int, file.readline().split(',')))

    print(play(numbers))
    print(play(numbers, 30000000))
