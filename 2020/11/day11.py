from typing import List, Dict, Tuple


def parse_seats(line_data: List[str]) -> Dict[Tuple[int, int], str]:
    return {(row, col): char for row, line in enumerate(line_data) for col, char in enumerate(line)}


neighbours8 = ((0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1))


def nb8(pos: Tuple[int, int]):
    return [(pos[0] + dx, pos[1] + dy) for dx, dy in neighbours8]


def nb_far(pos: Tuple[int, int], seat_map: Dict[Tuple[int, int], str]):
    for dx, dy in neighbours8:
        new_pos = (pos[0] + dx, pos[1] + dy)
        while new_pos in seat_map and seat_map[new_pos] not in ('#', 'L'):
            new_pos = (new_pos[0] + dx, new_pos[1] + dy)

        if new_pos in seat_map:
            yield new_pos


def evolve(seat_map: Dict[Tuple[int, int], str], extended: bool):
    new_map = seat_map.copy()

    for pos in seat_map:
        if seat_map[pos] == '.':
            continue

        neighbours = (nb8(pos) if not extended else nb_far(pos, seat_map))

        nbs = [seat_map[nb] for nb in neighbours if nb in seat_map]

        if seat_map[pos] == 'L' and nbs.count('#') == 0:
            new_map[pos] = '#'
        elif seat_map[pos] == '#' and nbs.count('#') >= 4 + int(extended):
            new_map[pos] = 'L'

    return new_map


def next_until_convergence(seat_map: Dict[Tuple[int, int], str], extended: bool = False):
    new_seat_map = evolve(seat_map, extended)
    while new_seat_map != seat_map:
        seat_map, new_seat_map = new_seat_map, evolve(new_seat_map, extended)
    return new_seat_map


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    seats = parse_seats(lines)

    new_seats = next_until_convergence(seats)
    print(f"Part 1: {list(new_seats.values()).count('#')}")

    new_seats = next_until_convergence(seats, extended=True)
    print(f"Part 2: {list(new_seats.values()).count('#')}")

