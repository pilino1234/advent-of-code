import re
from collections import defaultdict, namedtuple
from typing import List, Dict

Bag_req = namedtuple('Bag_req', ['colour', 'amount'])


def parse(line_data: List[str]) -> Dict[str, List[Bag_req]]:
    # parsed = defaultdict(list)
    #
    # for line in line_data:
    #     from_, _, to = line.strip('.').partition('bags contain')
    #
    #     if 'no other bags' in to:
    #         continue
    #
    #     from_ = from_.strip()
    #
    #     for bag in to.split(','):
    #         [amount] = extract_all_numbers(bag)
    #         colour = bag.replace(f'{amount}', '').replace(f'bag{"s" if amount > 1 else ""}', '').strip()
    #         parsed[from_].append(Bag_req(colour, amount))

    parsed2 = defaultdict(list)
    for line in line_data:
        from_ = re.match(r'(.+?) bags contain', line).group(1)
        for amount, colour in re.findall(r'(\d+) (.+?) bags?[,.]', line):
            parsed2[from_].append(Bag_req(colour, int(amount)))

    return parsed2


def ways_to_contain(bag_rules: Dict[str, List[Bag_req]], colour: str) -> set:
    possible_holders = set()

    for holder_color, contained_bags in bag_rules.items():
        for contained in contained_bags:
            if colour == contained.colour:
                possible_holders.add(holder_color)
                # Find ways to contain the parent bag
                possible_holders.update(ways_to_contain(bag_rules, holder_color))

    return possible_holders


def must_contain(bag_rules: Dict[str, List[Bag_req]], colour) -> Dict[Bag_req, Dict]:
    if colour not in bag_rules:
        return {}

    total = {}
    for contained in bag_rules[colour]:
        total[contained] = must_contain(bag_rules, contained.colour)

    return total


def count(nested_bags):
    return sum(bag.amount + bag.amount * count(nested_bags[bag]) for bag in nested_bags)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {len(ways_to_contain(data, 'shiny gold'))}")

    print(f"Part 2: {count(must_contain(data, 'shiny gold'))}")
