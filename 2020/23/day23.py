from typing import List, Dict


class Cup:
    def __init__(self, val: int, right: 'Cup' = None):
        self.val = val
        self.right = right


def crab_cups(current: Cup, cup_map: Dict[int, Cup],
              max_cup: int, rounds: int):
    for _ in range(rounds):
        a = current.right
        b = a.right
        c = b.right
        current.right = c.right

        dest = current.val - 1 or max_cup
        while dest == a.val or dest == b.val or dest == c.val:
            dest = dest - 1 or max_cup

        dest_node = cup_map[dest]
        c.right = dest_node.right
        dest_node.right = a

        current = current.right


def rotate_until_1(cups: List[int]):
    while not cups[0] == 1:
        num = cups.pop(0)
        cups.append(num)
    cups.pop(0)
    return cups


if __name__ == '__main__':
    with open("input.txt") as file:
        cups = list(map(int, (c for c in file.readline().strip())))

    nodes = [Cup(v) for v in cups]
    for cup, next_ in zip(nodes, nodes[1:]):
        cup.right = next_
    nodes[-1].right = nodes[0]
    node_map = {cup.val: cup for cup in nodes}

    crab_cups(nodes[0], node_map, max_cup=max(cups), rounds=100)

    cur = node_map[1].right
    result = []
    while cur.val != 1:
        result.append(cur.val)
        cur = cur.right
    print("Part 1:", ''.join(str(n) for n in result))

    cups.extend(range(max(cups) + 1, int(1e6) + 1))

    nodes = [Cup(v) for v in cups]
    for cup, next_ in zip(nodes, nodes[1:]):
        cup.right = next_
    nodes[-1].right = nodes[0]
    node_map = {cup.val: cup for cup in nodes}

    crab_cups(nodes[0], node_map, max_cup=int(1e6), rounds=int(1e7))
    node1 = node_map[1]
    print("Part 2:", node1.right.val * node1.right.right.val)
