from collections import namedtuple

Policy = namedtuple('Policy', ['letter', 'mincount', 'maxcount'])


def parse_policy(policy_text: str) -> Policy:
    minmax, letter = policy_text.split(' ')
    mincount, maxcount = minmax.split('-')
    return Policy(letter=letter, mincount=int(mincount), maxcount=int(maxcount))


def matches_sled(password: str, pol: Policy) -> bool:
    return pol.mincount <= password.count(pol.letter) <= pol.maxcount


def matches_toboggan(password: str, pol: Policy) -> bool:
    first_pos = password[pol.mincount - 1] == pol.letter
    second_pos = password[pol.maxcount - 1] == pol.letter

    # XOR
    return first_pos != second_pos


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    valid_sled_count = 0
    valid_toboggan_count = 0

    for line in lines:
        policy_part, password_part = line.split(': ')
        policy = parse_policy(policy_part)

        if matches_sled(password_part, policy):
            valid_sled_count += 1

        if matches_toboggan(password_part, policy):
            valid_toboggan_count += 1

    print(f"Part 1: {valid_sled_count}")
    print(f"Part 2: {valid_toboggan_count}")

