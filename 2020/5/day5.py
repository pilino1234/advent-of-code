

def parse(seat_data: str) -> int:
    trans = str.maketrans('FBLR', '0101')
    return int(seat_data.translate(trans), 2)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    seats = set(map(parse, lines))
    print(f"Part 1: {max(seats)}")

    missing = max(set(range(max(seats))) - seats)
    print(f"Part 2: {missing}")
