

def find_loop_size(result: int, subject: int, modulo: int) -> int:
    tmp = 1
    for loop_size in range(modulo):
        tmp *= subject
        tmp %= modulo
        if tmp == result:
            return loop_size + 1


if __name__ == '__main__':
    with open("input.txt") as file:
        card_pk, door_pk = list(map(int, (line.strip() for line in file)))

    subject = 7
    modulo = 20201227

    print(card_pk)
    card_loop = find_loop_size(card_pk, subject, modulo=modulo)
    print("Part 1:", pow(door_pk, card_loop, modulo))

    print(door_pk)
    door_loop = find_loop_size(door_pk, subject, modulo=modulo)
    print("Part 1 (again):", pow(card_pk, door_loop, modulo))
