from functools import reduce


if __name__ == '__main__':
    with open("input.txt") as file:
        groups = [g.strip() for g in file.read().split('\n\n')]

    print("Part 1:", sum(map(len, (set(g.replace('\n', '')) for g in groups))))

    print("Part 2:", sum(len(reduce(set.intersection, (set(p) for p in group.split('\n')))) for group in groups))
