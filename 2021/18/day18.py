import ast
import itertools
import math
from functools import reduce
from typing import List, Tuple, Optional, Union


def parse(line_data: List[str]) -> List:
    return [ast.literal_eval(line) for line in line_data]


def add_left(pair: List, n: Optional[int]) -> Union[List, int]:
    """Add n to the leftmost literal number in pair"""
    if n is None:
        return pair
    if isinstance(pair, int):
        return pair + n
    return [add_left(pair[0], n), pair[1]]


def add_right(pair: List, n: Optional[int]) -> Union[List, int]:
    """Add n to the rightmost literal number in pair"""
    if n is None:
        return pair
    if isinstance(pair, int):
        return pair + n
    return [pair[0], add_right(pair[1], n)]


def explode(num: List, level: int = 4) -> Tuple[bool, Optional[int], Union[List, int], Optional[int]]:
    # Can't recurse further, return early
    if isinstance(num, int):
        return False, None, num, None
    if level == 0:
        # Nesting level too high, explode this pair
        # print(f"{' ' * (4 - level)}Exploding: {num}")
        return True, num[0], 0, num[1]

    [left, right] = num
    # print(f"{' ' * (4 - level)}Explode: {num}, left={left}, right={right}")

    # Try exploding left side
    should_explode, l, left, r = explode(left, level=level - 1)
    if should_explode:
        # print(f"{' ' * (4 - level)}Exploded left: {l} {left} {r}")
        return True, l, [left, add_left(right, r)], None

    # Try exploding right side
    should_explode, l, right, r = explode(right, level=level - 1)
    if should_explode:
        # print(f"{' ' * (4 - level)}Exploded right: {l} {right} {r}")
        return True, None, [add_right(left, l), right], r

    return False, None, num, None


def split(num: Union[List, int]) -> Tuple[bool, Union[List, int]]:
    if isinstance(num, int):
        if num >= 10:
            split_num = [math.floor(num / 2), math.ceil(num / 2)]
            # print(f"Splitting number {num} -> {split_num}")
            return True, split_num
        return False, num

    [left, right] = num
    split_left, left = split(left)

    if split_left:
        # print(f"split left")
        return True, [left, right]

    split_right, right = split(right)
    if split_right:
        # print(f"split right")
        return True, [left, right]

    return False, [left, right]


def add(n1, n2):
    num = [n1, n2]
    # print(f"adding {n1} + {n2}")
    while True:
        did_explode, _, num, _ = explode(num)
        if did_explode:
            # Keep going until no more explodes
            continue
        did_split, num = split(num)
        if not did_split:
            # Stop after no more splits
            break
    # print(f"added {n1} + {n2} -> {num}")
    return num


def magnitude(num: Union[List, int]):
    if isinstance(num, int):
        return num
    return 3 * magnitude(num[0]) + 2 * magnitude(num[1])


def p2(nums: List) -> int:
    return max(magnitude(add(*pair)) for pair in itertools.permutations(nums, r=2))


if __name__ == '__main__':
    assert add(*parse(["[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]"])) == [[[[0, 7], 4], [[7, 8], [6, 0]]], [8, 1]]

    with open("test1.txt") as file:
        lines = [line.strip() for line in file]
        assert reduce(add, parse(lines)) == [[[[1, 1], [2, 2]], [3, 3]], [4, 4]]

    with open("test2.txt") as file:
        lines = [line.strip() for line in file]
        assert reduce(add, parse(lines)) == [[[[3, 0], [5, 3]], [4, 4]], [5, 5]]

    with open("test3.txt") as file:
        lines = [line.strip() for line in file]
        assert reduce(add, parse(lines)) == [[[[5, 0], [7, 4]], [5, 5]], [6, 6]]

    with open("test4.txt") as file:
        lines = [line.strip() for line in file]
        assert reduce(add, parse(lines)) == [[[[8, 7], [7, 7]], [[8, 6], [7, 7]]], [[[0, 7], [6, 6]], [8, 7]]]

    assert magnitude([[1, 2], [[3, 4], 5]]) == 143
    assert magnitude([[[[0, 7], 4], [[7, 8], [6, 0]]], [8, 1]]) == 1384
    assert magnitude([[[[1, 1], [2, 2]], [3, 3]], [4, 4]]) == 445
    assert magnitude([[[[3, 0], [5, 3]], [4, 4]], [5, 5]]) == 791
    assert magnitude([[[[5, 0], [7, 4]], [5, 5]], [6, 6]]) == 1137
    assert magnitude([[[[8, 7], [7, 7]], [[8, 6], [7, 7]]], [[[0, 7], [6, 6]], [8, 7]]]) == 3488

    with open("test5.txt") as file:
        lines = [line.strip() for line in file]
        numbers = parse(lines)
        result = reduce(add, numbers)
        assert result == [[[[6, 6], [7, 6]], [[7, 7], [7, 0]]], [[[7, 7], [7, 7]], [[7, 8], [9, 9]]]]
        assert magnitude(result) == 4140
        assert p2(numbers) == 3993

    with open("input.txt") as file:
        lines = [line.strip() for line in file]
    numbers = parse(lines)
    print(f"Part 1: {magnitude(reduce(add, parse(lines)))}")
    print(f"Part 2: {p2(numbers)}")
