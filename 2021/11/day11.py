from typing import List, Dict, Tuple

neighbours8 = ((0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1))


def nb8(pos: Tuple[int, int]) -> List[Tuple[int, int]]:
    return [(pos[0] + dx, pos[1] + dy) for dx, dy in neighbours8]


def parse(line_data: List[str]) -> Dict[Tuple[int, int], int]:
    grid = {}
    for row_idx, row in enumerate(line_data):
        for col_idx, col in enumerate(row):
            grid[(row_idx, col_idx)] = int(col)

    return grid


def generation(grid: Dict[Tuple[int, int], int]) -> Tuple[Dict[Tuple[int, int], int], int]:
    flash_queue = []

    for pos in grid:
        grid[pos] += 1
        if grid[pos] > 9:
            flash_queue.append(pos)

    flashed_pos = set()
    while flash_queue:
        pos = flash_queue.pop()

        if pos in flashed_pos:
            continue

        assert grid[pos] > 9, grid[pos]

        for nb in nb8(pos):
            if nb not in grid:
                continue
            grid[nb] += 1
            if grid[nb] > 9:
                if nb not in flashed_pos:
                    flash_queue.append(nb)

        flashed_pos.add(pos)

    flashes = len(flashed_pos)

    for flashed in flashed_pos:
        grid[flashed] = 0

    return grid, flashes


def print_map(dict_map: Dict[Tuple[int, int], int]):
    xs = [key[0] for key in dict_map]
    ys = [key[1] for key in dict_map]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    for row in range(miny, maxy + 1):
        for col in range(minx, maxx + 1):
            val = dict_map[(row, col)]
            print(val, end='')
        print()
    print()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    total_flashes = 0
    for gen in range(100):
        data, more_flashes = generation(data)
        total_flashes += more_flashes
        # print(f"After {gen+1} steps", more_flashes)
        # print_map(data)
    print(f"Part 1: {total_flashes}")

    gen = 100
    while True:
        data, more_flashes = generation(data)
        total_flashes += more_flashes

        gen += 1
        if more_flashes == len(data):
            break
    print(f"Part 2: {gen}")
