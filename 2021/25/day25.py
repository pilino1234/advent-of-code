from typing import List, Set, Tuple


def parse(line_data: List[str]) -> Tuple[Set[Tuple[int, int]], Set[Tuple[int, int]], int, int]:
    east = set()
    south = set()

    for y, row in enumerate(line_data):
        for x, spot in enumerate(row):
            if spot == '>':
                east.add((x, y))
            elif spot == 'v':
                south.add((x, y))

    map_height = len(line_data)
    map_width = len(line_data[0])

    return east, south, map_height, map_width


def nb_east(pos: Tuple[int, int], w) -> Tuple[int, int]:
    return (pos[0] + 1) % w, pos[1]


def nb_south(pos: Tuple[int, int], h) -> Tuple[int, int]:
    return pos[0], (pos[1] + 1) % h


def move(east: Set[Tuple[int, int]], south: Set[Tuple[int, int]],
         h: int, w: int) -> int:
    generation = 0
    while True:
        generation += 1
        # print_map(h, w, east, south)
        changed = False
        new_east = set()
        for cucumber in east:
            nextpos = nb_east(cucumber, w)
            if nextpos not in east and nextpos not in south:
                changed = True
                new_east.add(nextpos)
            else:
                new_east.add(cucumber)

        east = new_east
        new_south = set()

        for cucumber in south:
            nextpos = nb_south(cucumber, h)
            if nextpos not in east and nextpos not in south:
                changed = True
                new_south.add(nextpos)
            else:
                new_south.add(cucumber)

        south = new_south

        if not changed:
            return generation


def print_map(map_height, map_width, east: Set, south: Set):
    for row in range(map_height):
        for col in range(map_width):
            pos = (col, row)
            if pos in east:
                assert pos not in south
                val = '>'
            elif pos in south:
                assert pos not in east
                val = 'v'
            else:
                val = '.'
            print(val, end='')
        print()
    print()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    east_herd, south_herd, height, width = parse(lines)
    # print_map(height, width, east_herd, south_herd)
    print(f"Part 1: {move(east_herd, south_herd, h=height, w=width)}")
