from collections import Counter
from typing import List


def parse(line_data: List[str]) -> List:
    return list(map(int, line_data[0].split(',')))


def spawn(fish: List[int], days: int = 80):
    ages = Counter(fish)

    for _ in range(days):
        new_ages = Counter()
        for age, counts in ages.items():
            if age == 0:
                new_ages[6] += counts
                new_ages[8] += counts
            else:
                new_ages[age - 1] += counts
        ages = new_ages

    return sum(ages.values())


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(spawn(data))
    print(spawn(data, days=256))
