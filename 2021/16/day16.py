import math
from collections import namedtuple
from typing import Tuple, Union

LiteralPacket = namedtuple('LiteralPacket', ['version', 'type', 'value'])
OperatorPacket = namedtuple('OperatorPacket', ['version', 'type', 'length_type_id', 'subpackets'])


def to_bits(line: str) -> str:
    bitstring = []

    for char in line:
        bin_value = bin(int(char, base=16))[2:].rjust(4, '0')
        bitstring.extend(bin_value)
    return ''.join(bitstring)


def parse(bits: str) -> Tuple[Union[LiteralPacket, OperatorPacket], int]:
    base = 0
    version = int(bits[base:base + 3], base=2)
    type_id = int(bits[base + 3:base + 6], base=2)
    base = base + 6

    if type_id == 4:
        # Literal value
        number_bits = []
        finished = False

        while not finished:
            if base >= len(bits):
                break

            startbit, *number = bits[base:base + 5]

            if startbit == '0':
                finished = True

            number_bits.extend(number)
            base += 5

        number_value = int(''.join(number_bits), base=2)
        packet = LiteralPacket(version, type_id, number_value)
        return packet, base
    else:
        # Operator
        length_type_id = bits[base]
        base += 1

        if length_type_id == '0':
            subpacket_bits = int(bits[base:base+15], base=2)
            base += 15

            used_bits = 0
            subpackets = []

            while used_bits < subpacket_bits:
                packet, bits_used = parse(bits[base + used_bits:])
                subpackets.append(packet)
                used_bits += bits_used

            packet = OperatorPacket(version, type_id, length_type_id, subpackets)
            return packet, base + used_bits
        elif length_type_id == '1':
            subpacket_count = int(bits[base:base+11], base=2)
            base += 11

            subpackets = []

            for _ in range(subpacket_count):
                packet, bits_used = parse(bits[base:])
                subpackets.append(packet)
                base += bits_used

            packet = OperatorPacket(version, type_id, length_type_id, subpackets)
            return packet, base

        else:
            raise RuntimeError()


def total_version(packet: Union[LiteralPacket, OperatorPacket]) -> int:
    if isinstance(packet, LiteralPacket):
        return int(packet.version)
    elif isinstance(packet, OperatorPacket):
        return int(packet.version) + sum(total_version(p) for p in packet.subpackets)


def evaluate(packet: Union[LiteralPacket, OperatorPacket]) -> int:
    if isinstance(packet, LiteralPacket):
        assert packet.type == 4
        return packet.value
    elif isinstance(packet, OperatorPacket):
        assert packet.type != 4

        subpacket_values = (evaluate(subpacket) for subpacket in packet.subpackets)

        if packet.type == 0:
            # Sum
            result = sum(subpacket_values)
        elif packet.type == 1:
            # Product
            result = math.prod(subpacket_values)
        elif packet.type == 2:
            # Minimum
            result = min(subpacket_values)
        elif packet.type == 3:
            # Maximum
            result = max(subpacket_values)
        elif packet.type == 5:
            # Greater than
            values = list(subpacket_values)
            assert len(values) == 2
            result = int(values[0] > values[1])
        elif packet.type == 6:
            # Less than
            values = list(subpacket_values)
            assert len(values) == 2
            result = int(values[0] < values[1])
        elif packet.type == 7:
            # Equal to
            values = list(subpacket_values)
            assert len(values) == 2
            result = int(values[0] == values[1])
        else:
            raise RuntimeError()
        return result


if __name__ == '__main__':
    with open("input.txt") as file:
        line = file.readline().strip()

    assert parse(to_bits("D2FE28"))[0].value == 2021
    assert len(parse(to_bits("38006F45291200"))[0].subpackets) == 2
    assert len(parse(to_bits("EE00D40C823060"))[0].subpackets) == 3

    assert total_version(parse(to_bits("8A004A801A8002F478"))[0]) == 16
    assert total_version(parse(to_bits("620080001611562C8802118E34"))[0]) == 12
    assert total_version(parse(to_bits("C0015000016115A2E0802F182340"))[0]) == 23
    assert total_version(parse(to_bits("A0016C880162017C3686B18A3D4780"))[0]) == 31

    assert evaluate(parse(to_bits("C200B40A82"))[0]) == 3
    assert evaluate(parse(to_bits("04005AC33890"))[0]) == 54
    assert evaluate(parse(to_bits("880086C3E88112"))[0]) == 7
    assert evaluate(parse(to_bits("CE00C43D881120"))[0]) == 9
    assert evaluate(parse(to_bits("D8005AC2A8F0"))[0]) == 1
    assert evaluate(parse(to_bits("F600BC2D8F"))[0]) == 0
    assert evaluate(parse(to_bits("9C005AC2F8F0"))[0]) == 0
    assert evaluate(parse(to_bits("9C0141080250320F1802104A08"))[0]) == 1

    bits = to_bits(line)
    parsed_packet, total_bits_used = parse(bits)

    print(f"Part 1: {total_version(parsed_packet)}")

    print(f"Part 2: {evaluate(parsed_packet)}")
