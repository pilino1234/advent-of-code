from typing import List


def parse(line_data: List[str]) -> List:
    return [int(num, base=2) for num in line_data]


def p1(numbers: List[int], num_length: int):
    bits = [0 for _ in range(num_length)]

    for bit in range(num_length):
        for num in numbers:
            bits[bit] += (num & (1 << bit)) >> bit

    common = [int(bit > len(numbers) / 2) for bit in bits]

    gamma = int("".join(map(str, reversed(common))), base=2)
    print(gamma)

    eps = ~gamma & (1 << num_length) - 1
    print(eps)

    return gamma * eps


def oxygen_generator_rating(numbers, bit):
    print(f"{' ' * bit}{len(numbers)}, {bit}")
    print(numbers)
    if len(numbers) == 1:
        return numbers

    counter = 0
    for num in numbers:
        counter += (num & (1 << bit)) >> bit

    print(f"{counter}/{len(numbers)}", counter >= len(numbers) / 2)

    if counter >= len(numbers) / 2:
        filtered_numbers = list(filter(lambda n: (n & (1 << bit)) >> bit == 1, numbers))
    else:
        filtered_numbers = list(filter(lambda n: (n & (1 << bit)) >> bit == 0, numbers))

    print(filtered_numbers)

    return oxygen_generator_rating(filtered_numbers, bit=bit-1)


def co2_scrubber_rating(numbers, bit):
    print(f"{' ' * bit}{len(numbers)}, {bit}")
    print(numbers)
    if len(numbers) == 1:
        return numbers

    counter = 0
    for num in numbers:
        counter += (num & (1 << bit)) >> bit

    print(f"{counter}/{len(numbers)}", counter < len(numbers) / 2)

    if counter < len(numbers) / 2:
        filtered_numbers = list(filter(lambda n: (n & (1 << bit)) >> bit == 1, numbers))
    else:
        filtered_numbers = list(filter(lambda n: (n & (1 << bit)) >> bit == 0, numbers))

    print(filtered_numbers)

    return co2_scrubber_rating(filtered_numbers, bit=bit-1)


if __name__ == '__main__':
    with open("input.txt") as file:
    # with open("test1.txt") as file:
        lines = [line.strip() for line in file]

    num_len = len(lines[0])

    data = parse(lines)

    print(f"Part 1: {p1(data, num_len)}")
    ox = oxygen_generator_rating(data, bit=num_len-1)[0]
    co2 = co2_scrubber_rating(data, bit=num_len-1)[0]
    print(f"Part 2: {ox*co2}")
