from collections import Counter
from typing import List


def p1(numbers: List[str]) -> int:
    gamma = epsilon = ""
    for bit in range(len(numbers[0])):
        freqs = Counter((num[bit] for num in numbers)).most_common(2)
        gamma += freqs[0][0]
        epsilon += freqs[1][0]

    return int(gamma, base=2) * int(epsilon, base=2)


def p2(numbers: List[str]):
    gamma_numbers = epsilon_numbers = numbers
    for bit in range(len(numbers[0])):
        # Gamma
        if (len(gamma_numbers)) > 1:
            freqs = Counter((num[bit] for num in gamma_numbers))
            most_common = '1' if freqs['1'] >= freqs['0'] else '0'
            gamma_numbers = [num for num in gamma_numbers if num[bit] == most_common]

        # Epsilon
        if len(epsilon_numbers) > 1:
            freqs = Counter((num[bit] for num in epsilon_numbers))
            least_common = '1' if freqs['1'] < freqs['0'] else '0'
            epsilon_numbers = [num for num in epsilon_numbers if num[bit] == least_common]

    return int(gamma_numbers[0], base=2) * int(epsilon_numbers[0], base=2)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    print(f"Part 1: {p1(lines)}")
    print(f"Part 2: {p2(lines)}")
