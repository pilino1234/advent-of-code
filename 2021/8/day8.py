import itertools
from typing import List, Dict


def parse(line_data: List[str]) -> List:
    patterns = []
    for line in line_data:
        pattern, _, output = line.partition(' | ')
        patterns.append((pattern.split(),
                         output.split()))
    return patterns


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {sum(len(signal) in (2, 3, 4, 7) for entry in data for signal in entry[1])}")

    # "True" signals:
    #  aaa
    # b   c
    # b   c
    #  ddd
    # e   f
    # e   f
    #  ggg

    digits: Dict[str, int] = {
        "abcefg": 0,
        "cf": 1,
        "acdeg": 2,
        "acdfg": 3,
        "bcdf": 4,
        "abdfg": 5,
        "abdefg": 6,
        "acf": 7,
        "abcdefg": 8,
        "abcdfg": 9
    }

    sum_ = 0
    for entry in data:
        for permut in itertools.permutations("abcdefg"):
            # Mapping from single input signal -> single true output segment
            signal_mapping: Dict[str, str] = {in_: out for in_, out in zip(permut, "abcdefg")}

            # Use the above mapping to translate scrambled digit signals to true signals
            digit_signals: List[str] = ["".join(signal_mapping[c] for c in pattern) for pattern in entry[0]]

            # If all the un-scrambled, true signals exist in the digits map, the mapping is right
            if all("".join(sorted(signal)) in digits for signal in digit_signals):
                # Using the correct signal mapping, convert given output value signals to "true" output signals
                output_values: List[str] = ["".join(signal_mapping[c] for c in pattern) for pattern in entry[1]]
                # print(signal_mapping, output_values, entry[1])

                # Then, look up the "true" output signals using the digits map
                output_digits: List[int] = [digits["".join(sorted(signal))] for signal in output_values]
                sum_ += int("".join(map(str, output_digits)))  # todo: nicer way to construct number
                break

    print(f"Part 2: {sum_}")
