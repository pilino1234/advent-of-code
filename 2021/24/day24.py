from collections import defaultdict, deque, namedtuple
from typing import List, Dict, Set, Tuple

#
# class ALU:
#     def __init__(self):
#         self.w = 0
#         self.x = 0
#         self.y = 0
#         self.z = 0
#
#     def inp(self, data):
#         ...
# don't even think about it


def parse(line_data: List[str]) -> List:
    return line_data


if __name__ == '__main__':
    # with open("input.txt") as file:
    with open("test1.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    print(data)
