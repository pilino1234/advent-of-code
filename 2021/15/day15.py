import heapq
from typing import List, Dict, Tuple, Generator


def parse(line_data: List[str]) -> Dict[Tuple[int, int], int]:
    grid = {}

    for y, row in enumerate(line_data):
        for x, pos in enumerate(row):
            grid[(x, y)] = int(pos)

    return grid


def expand(line_data) -> Dict[Tuple[int, int], int]:
    expanded = {}

    for yrep in range(5):
        for y, row in enumerate(line_data):
            for xrep in range(5):
                for x, risk in enumerate(row):
                    pos = (xrep * len(row) + x, yrep * len(line_data) + y)
                    value = ((int(risk) + xrep + yrep - 1) % 9) + 1
                    expanded[pos] = value
    return expanded


def nb4(pos: Tuple[int, int]) -> Generator[Tuple[int, int], None, None]:
    for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
        yield pos[0] + dx, pos[1] + dy


def find_scores(grid: Dict[Tuple[int, int], int], target: Tuple[int, int]) -> int:
    start = (0, 0)

    costs = {}
    queue = [(0, start)]

    while queue:
        cost, pos = heapq.heappop(queue)

        if pos not in grid:
            continue

        new_cost = cost + grid[pos]
        if pos in costs and costs[pos] <= new_cost:
            continue
        costs[pos] = new_cost

        if pos == target:
            return new_cost - grid[(0, 0)]

        for nb in nb4(pos):
            if nb not in grid:
                continue
            heapq.heappush(queue, (costs[pos], nb))


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    goal = (len(lines[0])-1, len(lines)-1)
    print(f"Part 1: {find_scores(data, goal)}")

    grid2 = expand(lines)
    goal2 = ((goal[0]+1) * 5 - 1, (goal[1] + 1) * 5 - 1)
    print(f"Part 2: {find_scores(grid2, goal2)}")
