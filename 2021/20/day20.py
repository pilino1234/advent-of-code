#!/home/martin/bin/pypy
from collections import defaultdict
from functools import reduce, cache
from typing import List, Dict, Tuple


def parse(line_data: str) -> defaultdict[Tuple[int, int], bool]:
    grid = defaultdict(bool)

    for y, row in enumerate(line_data.split('\n')):
        for x, char in enumerate(row.strip()):
            grid[(x, y)] = (char == '#')

    return grid


nbs9 = ((-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1))


@cache
def nb9(pos: Tuple[int, int]) -> List[Tuple[int, int]]:
    return [(pos[0] + dx, pos[1] + dy) for dx, dy in nbs9]


def enhance(img: defaultdict[Tuple[int, int], bool], algo: str) -> defaultdict[Tuple[int, int], bool]:
    xs = [key[0] for key in img]
    ys = [key[1] for key in img]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    default_pixel = False
    if algo[0] == '#' and algo[-1] == '.':
        default_pixel = not img.default_factory()
    enhanced = defaultdict(lambda: default_pixel)

    for x in range(minx - 2, maxx + 2 + 1):
        for y in range(miny - 2, maxy + 2 + 1):
            # print(x, y)
            pixels = [img[pos] for pos in nb9((x, y))]
            # print(pixels)
            index = reduce(lambda a, b: (a << 1) + int(b), pixels)
            new_pixel = algo[index] == '#'
            enhanced[(x, y)] = new_pixel
    return enhanced


def print_map(dict_map: Dict[Tuple[int, int], int]):
    xs = [key[0] for key in dict_map]
    ys = [key[1] for key in dict_map]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    for row in range(miny, maxy + 1):
        for col in range(minx, maxx + 1):
            val = dict_map[(col, row)]
            char = '#' if val else '.'
            print(char, end='')
        print()
    print()


if __name__ == '__main__':
    with open("input.txt") as file:
        blocks = [block.strip() for block in file.read().split('\n\n')]

    enhancement = blocks[0]

    image = parse(blocks[1])
    # print_map(image)

    for gen in range(50):
        image = enhance(image, enhancement)
        if gen == 2:
            print(f"Part 1: {sum(image.values())}")

    print(f"Part 2: {sum(image.values())}")
