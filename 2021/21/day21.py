import re
from functools import cache
from typing import List, Dict, Tuple, Generator


def parse(line_data: List[str]) -> Dict[int, int]:
    pattern = r'Player (-?\d+) starting position: (-?\d+)'
    positions = {}
    for line in line_data:
        match = re.fullmatch(pattern, line)
        player, pos = map(int, match.groups())
        positions[player] = pos - 1

    return positions


def roll() -> Generator[int, None, None]:
    while True:
        yield from range(1, 101)


def play(board: Dict[int, int]) -> int:
    scores = {player: 0 for player in board}
    die = roll()
    total_rolls = 0

    while True:
        for p in board:
            board[p] += sum(next(die) for _ in range(3))
            total_rolls += 3
            board[p] %= 10
            scores[p] += board[p] + 1

            if scores[p] >= 1_000:
                return min(scores.values()) * total_rolls


@cache
def count_dirac(p1_pos: int, p1_score: int, p2_pos: int, p2_score: int) -> Tuple[int, int]:
    frequencies = {3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
    wins = (0, 0)

    for die_roll, freq in frequencies.items():
        new_p1_pos = p1_pos + die_roll
        new_p1_pos %= 10
        new_p1_score = p1_score + new_p1_pos + 1

        if new_p1_score >= 21:
            wins = (wins[0] + freq, wins[1])
        else:
            # Swap players for next turn
            next_wins = count_dirac(p2_pos, p2_score, new_p1_pos, new_p1_score)
            wins = (wins[0] + freq * next_wins[1], wins[1] + freq * next_wins[0])

    return wins


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {play(data.copy())}")
    print(f"Part 2: {max(count_dirac(data[1], 0, data[2], 0))}")
