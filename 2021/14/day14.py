from collections import defaultdict
from typing import List, Dict, Tuple


def parse(line_data: List[str]) -> Tuple[List[str], Dict[Tuple[str, str], str]]:
    formula = list(line_data[0])

    rules = {}
    for rule in line_data[1].split('\n'):
        a, _, b = rule.partition(' -> ')
        rules[(a[0], a[1])] = b

    return formula, rules


def generate(pairs: Dict[Tuple[str, str], int],
             rules: Dict[Tuple[str, str], str]) -> Dict[Tuple[str, str], int]:
    out = defaultdict(int)

    for pair, count in pairs.items():
        out[(pair[0], rules[pair])] += count
        out[(rules[pair], pair[1])] += count

    return out


def score(pairs: Dict[Tuple[str, str], int], last_letter: str):
    freqs = defaultdict(int)
    for pair, count in pairs.items():
        freqs[pair[0]] += count
    freqs[last_letter] += 1
    return max(freqs.values()) - min(freqs.values())


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [block.strip() for block in file.read().split('\n\n')]

    molecule, insertions = parse(lines)

    pairs = defaultdict(int)
    for pair in zip(molecule, molecule[1:]):
        pairs[pair] += 1

    for gen in range(1, 41):
        pairs = generate(pairs, insertions)
        if gen == 10:
            print(f"Part 1: {score(pairs, molecule[-1])}")
    print(f"Part 2: {score(pairs, molecule[-1])}")
