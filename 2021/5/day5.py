from collections import defaultdict, namedtuple
from typing import List, Dict, Any

Line = namedtuple('Line', ['x1', 'y1', 'x2', 'y2'])


def parse(line_data: List[str]) -> List:
    vent_lines = []
    for line in line_data:
        point1, point2 = line.split(' -> ')
        vent_lines.append(Line(*tuple(map(int, point1.split(','))),
                               *tuple(map(int, point2.split(',')))))
    return vent_lines


def count_overlaps(grid: Dict[Any, int]):
    return sum(value > 1 for value in grid.values())


def plot_vents(vent_lines: List[Line], diagonals: bool = False):
    grid = defaultdict(int)

    for line in vent_lines:
        if line.x1 == line.x2:
            points = [(line.x1, y) for y in range(min(line.y1, line.y2), max(line.y1, line.y2)+1)]
            # print(f"Adding vertical {points}")
            for point in points:
                grid[point] += 1
        elif line.y1 == line.y2:
            points = [(x, line.y1) for x in range(min(line.x1, line.x2), max(line.x1, line.x2)+1)]
            # print(f"Adding horizontal {points}")
            for point in points:
                grid[point] += 1
        else:
            if not diagonals:
                # skip diagonals
                continue

            step_x = 1 if line.x1 < line.x2 else -1
            step_y = 1 if line.y1 < line.y2 else -1

            points = [(px, py) for px, py in zip(range(line.x1, line.x2 + step_x, step_x),
                                                 range(line.y1, line.y2 + step_y, step_y))]
            # print(f"Adding diagonal {points}")
            for point in points:
                grid[point] += 1

    return count_overlaps(grid)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    vents = parse(lines)

    print(f"Part 1: {plot_vents(vents)}")
    print(f"Part 2: {plot_vents(vents, diagonals=True)}")
