from typing import List


def parse(line_data: List[str]) -> List:
    return list(map(int, line_data))


def p1(measurements: List[int]) -> int:
    return sum(n2 > n1 for n1, n2 in zip(measurements, measurements[1:]))


def p2(measurements: List[int]) -> int:
    window_sums = [sum(w) for w in zip(measurements, measurements[1:], measurements[2:])]
    return p1(window_sums)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
