from typing import List, Tuple


def parse(line_data: List[str]) -> List[Tuple[str, int]]:
    course = []
    for line in line_data:
        move, size = line.split()
        course.append((move, int(size)))
    return course


def follow_course(course_steps: List[Tuple[str, int]]):
    pos = 0
    depth_p1 = 0
    depth_p2 = 0

    for move in course_steps:
        if move[0] == "forward":
            pos += move[1]
            depth_p2 += depth_p1 * move[1]
        elif move[0] == "up":
            depth_p1 -= move[1]
        elif move[0] == "down":
            depth_p1 += move[1]

    return pos * depth_p1, pos * depth_p2


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    p1, p2 = follow_course(data)

    print(f"Part 1: {p1}")
    print(f"Part 2: {p2}")
