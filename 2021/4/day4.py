from typing import List, Tuple, Optional


def parse(line_data: str) -> Tuple[List[int], List[List[int]]]:
    line_data = line_data.split('\n\n')

    draw_numbers = list(map(int, line_data[0].split(',')))
    boards = [list(map(int, board.split())) for board in line_data[1:]]

    return draw_numbers, boards


def find_board_has_won(board: List[int]):
    # Horizontals
    for row in range(5):
        won = True
        for col in range(5):
            if board[5*row + col] is not None:
                won = False
                break
        if won:
            return won

    # Verticals
    for col in range(5):
        won = True
        for row in range(5):
            if board[5*row + col] is not None:
                won = False
                break
        if won:
            return won


def score_board(board: List[int]):
    return sum(num for num in board if num is not None)


def play(numbers: List[int], boards: List[List[Optional[int]]], last_wins: bool = False):
    for drawn_number in numbers:
        # Update all boards
        for board in boards:
            try:
                pos = board.index(drawn_number)
            except ValueError:  # board does not have drawn number
                continue
            board[pos] = None

        # Find winners, remove from game
        remaining_boards = []
        for idx, board in enumerate(boards):
            if find_board_has_won(board):
                if not last_wins or len(boards) == 1:
                    return drawn_number * score_board(board)

            remaining_boards.append(board)
        boards = remaining_boards


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = file.read()

    draw_list, board_list = parse(lines)
    print(f"Part 1: {play(draw_list, board_list)}")

    draw_list, board_list = parse(lines)
    print(f"Part 2: {play(draw_list, board_list, last_wins=True)}")
