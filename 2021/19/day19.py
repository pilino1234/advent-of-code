import itertools
import operator
from functools import lru_cache
from typing import List, Dict, Set, Tuple, Generator, Optional


def parse(line_data: List[str]) -> Dict[str, Tuple[Tuple[int, int, int], ...]]:
    probe_map = {}

    for block in line_data:
        [scanner, *probes] = [line.strip() for line in block.split('\n')]
        _, _, num, _ = scanner.split(' ')
        probe_positions = tuple(tuple(map(int, p.split(','))) for p in probes)
        probe_map[num] = probe_positions

    return probe_map


def roll(positions: Set[Tuple[int, int, int]]) -> Set[Tuple[int, int, int]]:
    return {(pos[0], pos[2], -pos[1]) for pos in positions}


def turn(positions: Set[Tuple[int, int, int]]) -> Set[Tuple[int, int, int]]:
    return {(-pos[1], pos[0], pos[2]) for pos in positions}


def rotations(scan: Tuple[Tuple[int, int, int]]) -> Generator[Set[Tuple[int, int, int]], None, None]:
    rot = scan
    for _ in range(2):
        for _ in range(3):
            rot = roll(rot)
            yield rot
            for _ in range(3):
                rot = turn(rot)
                yield rot
        rot = roll(turn(roll(rot)))


def tuple_diff(t1: Tuple, t2: Tuple) -> Tuple:
    return tuple(map(operator.sub, t1, t2))


@lru_cache(maxsize=500)
def find_overlap(scan1: Tuple, scan2: Tuple) -> Optional[Tuple[Tuple, Tuple[int, int, int]]]:
    for rot in rotations(scan2):
        for pos_1, pos_2 in itertools.product(scan1, rot):
            offset_vector = tuple_diff(pos_2, pos_1)

            remapped = tuple(map(tuple_diff, rot, itertools.repeat(offset_vector)))

            matches = len(set(scan1) & set(remapped))

            if matches >= 12:
                print(f"matches: {matches} {len(remapped)} {offset_vector}")
                return remapped, offset_vector
    return None


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [block.strip() for block in file.read().split('\n\n')]

    data = parse(lines)

    matched_scanners = {'0': data.pop('0')}
    scanner_locations = [(0, 0, 0)]
    while data:
        for idx1, idx2 in itertools.product(matched_scanners, data):
            scanner1 = matched_scanners[idx1]
            scanner2 = data[idx2]
            overlap = find_overlap(scanner1, scanner2)
            if overlap is not None:
                overlap, offset = overlap
                print(f"Matched scanner {idx1} with {idx2}, offset {offset}")
                matched_scanners[idx2] = overlap
                scanner_locations.append(offset)
                data.pop(idx2)
                break

    final_points = {p for scanpoints in matched_scanners.values() for p in scanpoints}
    print(f"Part 1: {len(final_points)}")

    distances = []
    for p1, p2 in itertools.combinations(scanner_locations, r=2):
        distances.append(sum(map(abs, tuple_diff(p1, p2))))
    print(f"Part 2: {max(distances)}")
