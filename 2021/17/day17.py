import re
from typing import List, Set, Tuple


def parse(line_data: List[str]) -> Tuple[range, range]:
    pattern = r'target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)'
    match = re.fullmatch(pattern, line_data[0])
    min_x, max_x, min_y, max_y = map(int, match.groups())

    return range(min_x, max_x + 1), range(min_y, max_y + 1)


def simulate(velocity: Tuple[int, int], target: Tuple[range, range]) -> Tuple[bool, int]:
    probe_pos = (0, 0)
    max_y = 0

    while True:
        if probe_pos[1] > max_y:
            max_y = probe_pos[1]

        if probe_pos[0] in target[0] and probe_pos[1] in target[1]:
            return True, max_y

        if probe_pos[1] < target[1].start:
            # Overshoot and pass through target without being inside
            return False, 0
        elif probe_pos[0] > target[0].stop:
            # Overshoot to the right
            return False, 0

        probe_pos = (probe_pos[0] + velocity[0], probe_pos[1] + velocity[1])

        new_vx = velocity[0]
        if velocity[0] > 0:
            new_vx = velocity[0] - 1
        elif velocity[0] < 0:
            new_vx = velocity[0] + 1
        velocity = (new_vx, velocity[1] - 1)


def find_highest_y(target: Tuple[range, range]) -> Tuple[int, Set[Tuple[int, int]]]:
    overall_max_y = 0
    success_vels = set()

    for v_x in range(target[0].stop):
        for v_y in range(-150, 1_000):
            target_hit, max_y = simulate((v_x, v_y), target)
            if target_hit:
                success_vels.add((v_x, v_y))
                if max_y > overall_max_y:
                    overall_max_y = max_y

    return overall_max_y, success_vels


if __name__ == '__main__':
    with open("input.txt") as file:
        line = [line.strip() for line in file]

    x_range, y_range = parse(line)

    assert simulate((7, 2), (range(20, 30+1), range(-10, -5+1))) == (True, 3)
    assert simulate((6, 3), (range(20, 30+1), range(-10, -5+1))) == (True, 6)
    assert simulate((9, 0), (range(20, 30+1), range(-10, -5+1))) == (True, 0)
    assert simulate((17, -4), (range(20, 30+1), range(-10, -5+1))) == (False, 0)

    p1, p2 = find_highest_y((x_range, y_range))
    print(f"Part 1: {p1}")
    print(f"Part 2: {len(p2)}")
