from collections import defaultdict, deque, namedtuple
from typing import List, Dict, Set, Tuple


def parse(line: str) -> List[int]:
    return list(map(int, line.split(',')))


def find_cost(crab_positions: List[int], target: int):
    return sum(abs(crab_pos - target) for crab_pos in crab_positions)


def triangle_num(n: int):
    return (n * (n + 1)) // 2


def find_cost_2(crab_positions: List[int], target: int):
    return sum(triangle_num(abs(crab_pos - target)) for crab_pos in crab_positions)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = file.readline()

    data = parse(lines)
    min_ = min(data)
    max_ = max(data)

    print(f"Part 1: {min(find_cost(data, target) for target in range(min_, max_ + 1))}")
    print(f"Part 2: {min(find_cost_2(data, target) for target in range(min_, max_ + 1))}")
