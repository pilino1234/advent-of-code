import math
from typing import List, Dict, Tuple


def parse(line_data: List[str]) -> Dict[Tuple[int, int], int]:
    heightmap = {}
    for row_idx, row in enumerate(line_data):
        for col_idx, col in enumerate(row):
            heightmap[(row_idx, col_idx)] = int(col)

    return heightmap


def nb4(pos: Tuple[int, int]):
    for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
        yield pos[0] + dx, pos[1] + dy


def find_low_points(grid: Dict[Tuple[int, int], int]) -> List[Tuple[Tuple[int, int], int]]:
    low = []

    for pos, value in grid.items():
        is_low = True
        for nb_pos in nb4(pos):
            if nb_pos not in grid:
                continue
            if grid[nb_pos] <= value:
                is_low = False
                break

        if is_low:
            low.append((pos, value))

    return low


def risk_levels(points: List[Tuple[Tuple[int, int], int]]) -> int:
    total = 0
    for point in points:
        total += point[1] + 1
    return total


def find_basins(grid: Dict[Tuple[int, int], int],
                low: List[Tuple[Tuple[int, int], int]]) -> int:
    basins = {p[0]: set() for p in low}

    for pos in low_points:
        pos, value = pos

        queue = [p for p in nb4(pos)]
        while queue:
            next_pos = queue.pop()
            if next_pos not in grid:
                continue
            next_val = grid[next_pos]
            if next_val == 9:
                continue
            else:
                basins[pos].add(next_pos)
                queue.extend((p for p in nb4(next_pos) if p not in basins[pos]))

    largest = sorted(basins.values(), key=lambda it: len(it), reverse=True)

    return math.prod(list(map(len, largest[0:3])))


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    low_points = find_low_points(data)
    print(f"Part 1: {risk_levels(low_points)}")

    print(f"Part 2: {find_basins(data, low_points)}")
