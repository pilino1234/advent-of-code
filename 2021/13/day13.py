from typing import List, Dict, Tuple


def parse(line_data: str) -> Tuple[Dict[Tuple[int, int], bool], List[Tuple[str, int]]]:
    dots, instructions = line_data.split('\n\n')

    grid = {}

    for dot in dots.strip().split():
        grid[tuple(map(int, dot.split(',')))] = True

    lines = []
    for ins in instructions.strip().split('\n'):
        s = ins.split()[2]
        ax, num = s.split('=')
        lines.append((ax, int(num)))

    return grid, lines


def apply_fold(grid: Dict[Tuple[int, int], bool], fold_line: Tuple[str, int]) -> Dict[Tuple[int, int], bool]:
    new_grid = {}

    dim = 1 if fold_line[0] == 'y' else 0

    for point in grid:
        assert grid[point]
        if point[dim] < fold_line[1]:
            # Keep original
            new_grid[point] = True
        else:
            # Fold
            new_pos = fold_line[1] - (point[dim] - fold_line[1])
            if dim == 0:
                new_pos = (new_pos, point[1])
            else:
                new_pos = (point[0], new_pos)
            new_grid[new_pos] = True
    return new_grid


def print_map(dict_map: Dict[Tuple[int, int], int]):
    xs = [key[0] for key in dict_map]
    ys = [key[1] for key in dict_map]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    for row in range(miny, maxy + 1):
        for col in range(minx, maxx + 1):
            pos = (col, row)
            if pos in dict_map:
                val = dict_map[pos]
            else:
                val = False
            if val:
                char = '#'
            else:
                char = ' '
            print(char, end='')
        print()
    print()


if __name__ == '__main__':
    with open("input.txt") as file:
        content = file.read()

    paper, folds = parse(content)

    paper = apply_fold(paper, folds[0])
    print(f"Part 1: {len(paper)}")

    for line in folds[1:]:
        paper = apply_fold(paper, line)
        # print_map(paper)
    print("Part 2:")
    print_map(paper)
