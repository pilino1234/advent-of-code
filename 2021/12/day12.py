from collections import defaultdict
from typing import List, Dict, Set, Tuple


def parse(line_data: List[str]) -> Tuple[Dict[str, List[str]], Set[str]]:
    cave_links = defaultdict(list)
    lowercase_caves = set()

    for line in line_data:
        a, b = line.split('-')
        cave_links[a].append(b)
        cave_links[b].append(a)

        if a.islower():
            lowercase_caves.add(a)
        if b.islower():
            lowercase_caves.add(b)

    return cave_links, lowercase_caves


# def find_paths(transitions: Dict[str, List[str]], start: str, path: List[str],
#                visited: Dict[str, int], level=0) -> List:
#     path.append(start)
#     print(f"{' ' * level}start: {start}")
#
#     if start == 'end':
#         print(f"{' '*level}path: {path}")
#         return path
#
#     if start.islower():
#         visited[start] += 1
#
#     paths = []
#     for nb in transitions[start]:
#         if nb not in visited:
#             print(f"{' '*level}nb: {start}->{nb}")
#             result = find_paths(transitions, nb, path.copy(), visited.copy(), level+1)
#             print(f"{' '*level}result: {result}")
#             if result is not None:
#                 if isinstance(result[0], str):
#                     paths.append(result)
#                 else:
#                     paths.extend(result)
#     path.pop()
#     return paths or None


def find_paths2(transitions: Dict[str, List[str]], current: str,
                visited: Set[str], small_caves: Set[str], visited_twice: bool = False) -> int:
    if current == 'end':
        return 1
    elif current == 'start' and visited:
        return 0

    paths = 0
    for nb in transitions[current]:
        vt2 = visited_twice
        if nb in small_caves and nb in visited:
            if visited_twice or nb in ('start', 'end'):
                continue
            vt2 = True
        paths += find_paths2(transitions, nb, visited | {nb}, small_caves, visited_twice=vt2)

    return paths


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    edges, small = parse(lines)

    print(f"Part 1: {find_paths2(edges, 'start', set(), small, visited_twice=True)}")
    print(f"Part 2: {find_paths2(edges, 'start', set(), small)}")
