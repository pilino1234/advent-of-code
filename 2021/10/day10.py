from typing import List


OPENING = ('(', '[', '{', '<')
CLOSING = (')', ']', '}', '>')
CHUNKS = {op: cl for op, cl in zip(OPENING, CLOSING)}
SCORES_P1 = {cl: sc for cl, sc in zip(CLOSING, (3, 57, 1197, 25137))}
SCORES_P2 = {cl: sc for cl, sc in zip(CLOSING, (1, 2, 3, 4))}


def parse(line_data: List[str]) -> List[List[str]]:
    return [list(line) for line in line_data]


def validate_chunks(line: List[str], fix_incomplete: bool = False):
    stack = []

    first_illegal_char = None
    score = 0

    for c in line:
        if c in OPENING:
            stack.append(c)
        if c in CLOSING:
            opener = stack.pop()
            if not CHUNKS[opener] == c and not fix_incomplete \
                    and (first_illegal_char is None or c == first_illegal_char):
                # print(f"Illegal character, expected {CHUNKS[opener]} but got {c}")
                first_illegal_char = c
                score += SCORES_P1[c]

    if fix_incomplete and stack:
        # print(f"Incomplete line, remaining chunks to close: {''.join(reversed(stack))}")
        for opener in reversed(stack):
            score *= 5
            score += SCORES_P2[CHUNKS[opener]]

    return score


if __name__ == '__main__':
    with open('input.txt') as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    total = 0
    complete_lines = []
    for line in data:
        line_score = validate_chunks(line[:])
        total += line_score
        if line_score == 0:  # Line is complete
            complete_lines.append(line)
    print(f"Part 1: {total}")

    total2 = [validate_chunks(line[:], fix_incomplete=True) for line in complete_lines]
    total2.sort()
    print(f"Part 2: {total2[len(total2) // 2]}")
