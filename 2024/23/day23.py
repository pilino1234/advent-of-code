from collections import defaultdict
from itertools import combinations


def parse(line_data: list[str]) -> dict[str, set[str]]:
    network = defaultdict(set)
    for line in line_data:
        c1, c2 = line.split("-")
        network[c1].add(c2)
        network[c2].add(c1)
    return network


def p1(connections: dict[str, set[str]]) -> int:
    triplets = set()

    for triple in combinations(connections.keys(), 3):
        for pair in combinations(triple, 2):
            if pair[1] not in connections[pair[0]]:
                break
        else:
            triplets.add(triple)

    result = sum(any(computer.startswith("t") for computer in triple) for triple in triplets)
    return result


def p2(connections: dict[str, set[str]]) -> str:
    networks = [{computer} for computer in connections.keys()]
    for network in networks:
        for computer in connections.keys():
            if all(existing_member in connections[computer] for existing_member in network):
                network.add(computer)

    lan_party_size = max(map(len, networks))
    for network in networks:
        if len(network) == lan_party_size:
            return ",".join(sorted(network))


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    # print(data)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
