from collections import defaultdict
from math import degrees


def parse(line_data: str) -> tuple[dict[int, range], list[range]]:
    currently_in_file = True
    current_pos = 0
    file_id = 0

    file_map: dict[int, range] = {}
    free_space: list[range] = []
    for number in line_data:
        length = int(number)
        region_range = range(current_pos, current_pos + length)
        if currently_in_file:
            file_map[file_id] = region_range
            file_id += 1
        else:
            free_space.append(region_range)

        currently_in_file = not currently_in_file
        current_pos += length

    return file_map, free_space


def print_disk_map(file_map: dict[int, range], free_space: list[range]) -> None:
    # Reverse file_map
    range_to_file_id = {file_range: file_id for file_id, file_range in file_map.items()}

    # Extend with free space regions
    range_to_file_id.update({free_range: -1 for free_range in free_space})

    # Iterate regions in order
    for filesystem_range in sorted(range_to_file_id, key=lambda r: r.start):
        file_id = range_to_file_id[filesystem_range]
        char = str(file_id) if file_id != -1 else "."
        print(char * len(filesystem_range), end="")
    print("")


def filesystem_checksum(filesystem: list[int]) -> int:
    result = 0
    for block_id, file_id in enumerate(filesystem):
        if file_id == -1:
            continue
        result += block_id * file_id
    return result


def p1(file_map: dict[int, range], free_space: list[range]) -> int:
    # File map is sorted by key (file ID) in ascending order
    to_defrag = file_map.copy()
    last_file_id = max(to_defrag.keys())

    # Free ranges are sorted by starting point in ascending order
    free_ranges = free_space.copy()
    first_free_range = free_ranges.pop(0)

    # Fill in already defragmented files at beginning of filesystem
    defragmented = []
    for file_id in sorted(to_defrag.keys()):
        file_range = to_defrag[file_id]
        if file_range.stop > first_free_range.start:
            break
        to_defrag.pop(file_id)
        defragmented.extend([file_id] * len(file_range))

    while to_defrag:
        # Move last used to first empty, decrement both
        last_file_range = to_defrag.pop(last_file_id)
        # print(f"{first_free_range=} ({len(first_free_range)} blocks)", f"{last_file_range=} (file ID: {last_file_id})")

        if len(last_file_range) < len(first_free_range):
            # Move the entire last file into place
            # print(f"Moving entire {last_file_id=} into place ({len(last_file_range)} blocks)")
            defragmented.extend([last_file_id] * len(last_file_range))

            # Update what is left of the empty region
            first_free_range = range(first_free_range.start + len(last_file_range),
                                     first_free_range.stop)
            assert len(first_free_range) > 0

            # Finished this file, go to next last one
            # last_file_id = max(to_defrag.keys())
            last_file_id -= 1
        else:
            # Will fill this free space, move as much as possible into place
            # print(f"Moving as much as possible of {last_file_id=} into place ({len(first_free_range)} blocks)")
            defragmented.extend([last_file_id] * len(first_free_range))

            # Put the rest of the file back to be defragged next
            remaining_last_file_range = range(last_file_range.start,
                                              last_file_range.stop - len(first_free_range))
            to_defrag[last_file_id] = remaining_last_file_range

            # Advance to next empty region
            first_free_range = free_ranges.pop(0)

            # Fill in already defragmented files up to the next first empty block
            for file_id in sorted(to_defrag.keys()):
                file_range = to_defrag[file_id]
                if file_range.stop > first_free_range.start:
                    break
                to_defrag.pop(file_id)
                defragmented.extend([file_id] * len(file_range))
        # print(f"New first empty region: {first_free_range=}")

    return filesystem_checksum(defragmented)


def p2(file_map: dict[int, range], free_space: list[range]) -> int:
    # File map is sorted by key (file ID) in ascending order
    to_defrag = file_map.copy()
    last_file_id = max(to_defrag.keys())

    # Free ranges are sorted by starting point in ascending order
    free_ranges = free_space.copy()

    defragmented_fs: dict[range, int] = {}
    while to_defrag:
        last_file_range = to_defrag.pop(last_file_id)
        # print(f"Trying to defrag {last_file_id=} (length={len(last_file_range)})")

        for free in free_ranges:
            if free.start > last_file_range.start:
                continue
            if len(last_file_range) <= len(free):
                # print(f"Moving to {free}")
                new_file_range = range(free.start, free.start + len(last_file_range))
                defragmented_fs[new_file_range] = last_file_id
                free_ranges.remove(free)

                # Mark previous file range as free
                free_ranges.append(last_file_range)

                remaining_space = range(free.start + len(last_file_range), free.stop)
                if remaining_space:
                    # print(f"Putting back remaining free space: {remaining_space=}")
                    free_ranges.append(remaining_space)
                    free_ranges.sort(key=lambda r: r.start)
                break
        else:
            # print(f"Could not move {last_file_id=} (length={len(last_file_range)})")
            defragmented_fs[last_file_range] = last_file_id

        last_file_id -= 1

    # Add free ranges to defragmented fs
    defragmented_fs.update({free_range: -1 for free_range in free_ranges})

    final_filesystem = []
    for file_range in sorted(defragmented_fs.keys(), key=lambda r: r.start):
        file_id = defragmented_fs[file_range]
        final_filesystem.extend([file_id] * len(file_range))
    return filesystem_checksum(final_filesystem)

if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test3.txt") as file:
    with open("input.txt") as file:
        lines = file.readline().strip()

    data = parse(lines)
    # print_disk_map(*data)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(*data)}")
