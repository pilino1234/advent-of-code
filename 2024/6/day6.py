from typing import Callable

Point = tuple[int, int]
Grid = dict[Point, str]

MovementFunction = Callable[[Point], Point]
up: MovementFunction = lambda p: (p[0] - 1, p[1])
down: MovementFunction = lambda p: (p[0] + 1, p[1])
left: MovementFunction = lambda p: (p[0], p[1] - 1)
right: MovementFunction = lambda p: (p[0], p[1] + 1)


def turn_right(direction: MovementFunction) -> MovementFunction:
    # dirs = [up, right, down, left]
    # return dirs[(dirs.index(direction) + 1) % len(dirs)]
    return {
        up: right,
        right: down,
        down: left,
        left: up,
    }[direction]


def parse(line_data: list[str]) -> tuple[Grid, Point]:
    grid = {}
    start_pos = None
    for row_idx, row in enumerate(line_data):
        for col_idx, char in enumerate(row):
            if char == "^":
                start_pos = (row_idx, col_idx)
                char = "."
            grid[(row_idx, col_idx)] = char
    return grid, start_pos


def print_grid(
        dict_map: Grid,
        guard_pos: Point,
        guard_dir: MovementFunction,
        visited: set[tuple[Point, MovementFunction]],
        obstruction: Point = None,
) -> None:
    xs = [key[0] for key in dict_map]
    ys = [key[1] for key in dict_map]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    dir_chars = {
        up: "^",
        down: "v",
        left: "<",
        right: ">",
    }

    visited_pos = {p[0] for p in visited}

    for row in range(miny, maxy + 1):
        for col in range(minx, maxx + 1):
            pos = (row, col)
            char = dict_map[pos]
            if pos in visited_pos:
                char = "X"
            if pos == obstruction:
                char = "O"
            if pos == guard_pos:
                char = dir_chars[guard_dir]
            print(char, end="")
        print()
    print()


class CycleDetected(Exception):
    def __init__(self, visited: set) -> None:
        self.visited = visited


def simulate_guard(
        grid: Grid,
        guard_pos: Point,
        guard_dir: MovementFunction,
        raise_for_cycle: bool = False
) -> set[tuple[Point, MovementFunction]]:
    visited: set[tuple[Point, MovementFunction]] = {(guard_pos, guard_dir)}

    while True:
        next_pos = guard_dir(guard_pos)
        if next_pos not in grid:
            # Reached out of bounds
            break
        if raise_for_cycle and (next_pos, guard_dir) in visited:
            raise CycleDetected(visited)
        if grid[next_pos] == ".":
            # If next pos is empty, move forward
            guard_pos = next_pos
            visited.add((next_pos, guard_dir))
        else:
            # If next pos is blocked, stay and turn right
            guard_dir = turn_right(guard_dir)

    return visited


def p1(grid: Grid, guard_pos: Point, guard_dir: MovementFunction) -> set[tuple[Point, MovementFunction]]:
    visited = simulate_guard(grid, guard_pos, guard_dir)
    # print_grid(grid, guard_pos, guard_dir, visited)
    return visited


def p2(
        grid: Grid,
        guard_pos: Point,
        guard_dir: MovementFunction,
        visited_pos: set[tuple[Point, MovementFunction]]
) -> int:
    possible_positions = {p[0] for p in visited_pos}
    possible_positions.remove(guard_pos)

    cycle_counter = 0
    for obstruction_pos in possible_positions:
        grid[obstruction_pos] = "#"
        try:
            simulate_guard(grid, guard_pos, guard_dir, raise_for_cycle=True)
        except CycleDetected as e:
            # print_grid(grid, guard_pos, guard_dir, e.visited, obstruction_pos)
            cycle_counter += 1
        grid[obstruction_pos] = "."
    return cycle_counter


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    lab_map, guard_start = parse(lines)
    starting_direction = up
    # print_grid(lab_map, guard_start, starting_direction, set())

    visited_p1 = p1(lab_map, guard_start, starting_direction)
    print(f"Part 1: {len(visited_p1)}")
    print(f"Part 2: {p2(lab_map, guard_start, starting_direction, visited_p1)}")
