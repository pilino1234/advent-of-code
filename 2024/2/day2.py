from collections.abc import Generator
from itertools import pairwise


def parse(line_data: list[str]) -> list[list[int]]:
    return [list(map(int, line.split())) for line in line_data]


def is_safe(report: list[int]) -> bool:
    diffs = [a - b for a, b in pairwise(report)]
    if not all(1 <= abs(diff) <= 3 for diff in diffs):
        return False
    if not (all(diff > 0 for diff in diffs) or all(diff < 0 for diff in diffs)):
        return False
    return True


def dampen(report: list[int]) -> Generator[list[int], None, None]:
    for pos in range(len(report)):
        yield report[:pos] + report[pos+1:]


def p1(reports: list[list[int]]) -> int:
    return sum(is_safe(report) for report in reports)


def p2(reports: list[list[int]]) -> int:
    count = 0
    for report in reports:
        safe = is_safe(report)
        if not safe:
            for dampened in dampen(report):
                if safe := is_safe(dampened):
                    break
        count += safe
    return count


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
