import itertools
import operator

Equation = tuple[int, list[int]]

def parse(line_data: list[str]) -> list[Equation]:
    parsed = []

    for line in line_data:
        result, numbers = line.split(":")
        numbers = numbers.split()
        parsed.append((int(result), list(map(int, numbers))))

    return parsed


def can_solve(result: int, digits: list[int], concat_operator: bool = False) -> int:
    operators_needed = len(digits) - 1
    operators = [operator.add, operator.mul]

    if concat_operator:
        operators.append(lambda a, b: int(f"{a}{b}"))

    solutions = 0
    for operators in itertools.product(operators, repeat=operators_needed):
        current_result = digits[0]
        for op, rhs in zip(operators, digits[1:]):
            current_result = op(current_result, rhs)
            if current_result > result:
                break

        if current_result == result:
            solutions += 1

    return solutions


def p1(equations: list[Equation]) -> int:
    result = 0
    for eq in equations:
        if can_solve(*eq):
            result += eq[0]
    return result


def p2(equations: list[Equation]) -> int:
    result = 0
    for eq in equations:
        if can_solve(*eq, concat_operator=True):
            result += eq[0]
    return result


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
