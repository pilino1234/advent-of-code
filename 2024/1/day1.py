import collections


def parse(line_data: list[str]) -> tuple[list[int], list[int]]:
    left, right = [], []
    for line in line_data:
        # l, r = line.split()
        # left.append(int(l))
        # right.append(int(r))
        left[0:0], right[0:0] = zip(map(int, line.split()))
    return left, right


def p1(left: list[int], right: list[int]) -> int:
    return sum(abs(l - r) for l, r in zip(sorted(left), sorted(right)))


def p2(left: list[int], right: list[int]) -> int:
    right_occs = collections.Counter(right)
    return sum(left_num * right_occs[left_num] for left_num in left)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    left_right = parse(lines)

    print(f"Part 1: {p1(*left_right)}")
    print(f"Part 2: {p2(*left_right)}")
