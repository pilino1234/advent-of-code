from collections import Counter
from itertools import pairwise


def generate_next(prev: int) -> int:
    # 16777216 == 1<<24
    prune_mask = (1 << 24) - 1

    # result = prune(mix(prev, prev * 64))
    # (prev ^ (prev<<6)) % 1<<24
    result = (prev ^ (prev << 6)) & prune_mask

    # result = prune(mix(result, result // 32))
    # (prev ^ (prev>>32)) % 1<<24
    result ^= (result >> 5) & prune_mask

    # result = prune(mix(result, result * 2048))
    # (prev ^ (prev<<11)) % 1<<24
    result ^= (result << 11) & prune_mask

    return result


def p1_and_p2(seeds: list[int], how_many: int = 2000, history_size: int = 4) -> tuple[int, int]:
    p1_result = 0
    pattern_to_price = Counter()

    for num in seeds:
        # Build sequence
        # seq = [num] + [num := generate_next(num) for _ in range(how_many)]
        seq = [num]
        for _ in range(how_many):
            seq.append(generate_next(seq[-1]))
        p1_result += seq[-1]

        # prices = [n % 10 for n in seq]
        diffs = [n2 % 10 - n1 % 10 for n1, n2 in pairwise(seq)]

        # Count diff histories and their price
        seen = set()
        for pos in range(len(seq) - history_size):
            history = tuple(diffs[pos:pos + history_size])
            if history not in seen:
                price = seq[pos + history_size] % 10
                pattern_to_price[history] += price
                seen.add(history)

    return p1_result, pattern_to_price.most_common(1)[0][1]


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        data = [int(line.strip()) for line in file]

    results = p1_and_p2(data)
    print(f"Part 1: {results[0]}")
    print(f"Part 2: {results[1]}")
