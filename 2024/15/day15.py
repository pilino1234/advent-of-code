from typing import Callable

Point = tuple[int, int]
Grid = dict[Point, str]

MovementFunction = Callable[[Point], Point]
up: MovementFunction = lambda p: (p[0] - 1, p[1])
down: MovementFunction = lambda p: (p[0] + 1, p[1])
left: MovementFunction = lambda p: (p[0], p[1] - 1)
right: MovementFunction = lambda p: (p[0], p[1] + 1)

CHAR_TO_DIRECTION = {
    "^": up,
    "v": down,
    "<": left,
    ">": right,
}


def parse(line_data: str) -> tuple[Grid, Point, str]:
    grid_str, instructions = line_data.split("\n\n")
    instructions = instructions.replace("\n", "")

    grid = {}
    start_pos = None
    for row_idx, row in enumerate(grid_str.splitlines()):
        for col_idx, char in enumerate(row):
            if char == "@":
                start_pos = (row_idx, col_idx)
                char = "."
            grid[(row_idx, col_idx)] = char

    return grid, start_pos, instructions


def parse_wide(line_data: str) -> tuple[Grid, Point, str]:
    grid_str, instructions = line_data.split("\n\n")
    instructions = instructions.replace("\n", "")

    widen = {
        "#": tuple("##"),
        "O": tuple("[]"),
        ".": tuple(".."),
        "@": tuple(".."),
    }

    grid = {}
    start_pos = None
    for row_idx, row in enumerate(grid_str.splitlines()):
        for narrow_col_idx, char in enumerate(row):
            if char == "@":
                start_pos = (row_idx, 2 * narrow_col_idx)
            l_char, r_char = widen[char]
            grid[(row_idx, 2 * narrow_col_idx)] = l_char
            grid[(row_idx, 2 * narrow_col_idx + 1)] = r_char
    return grid, start_pos, instructions


def print_grid(dict_map: Grid, robot_pos: Point) -> None:
    rows = [key[0] for key in dict_map]
    cols = [key[1] for key in dict_map]
    min_row = min(rows)
    max_row = max(rows)
    min_col = min(cols)
    max_col = max(cols)

    for row in range(min_row, max_row + 1):
        for col in range(min_col, max_col + 1):
            pos = (row, col)
            char = dict_map[pos]
            if pos == robot_pos:
                char = "@"
            print(char, end="")
        print()


def move_robot(grid: Grid, robot_pos: Point, movement: MovementFunction) -> Point:
    next_pos = movement(robot_pos)
    assert next_pos in grid

    if grid[next_pos] == ".":
        # Free space, move
        return next_pos
    elif grid[next_pos] == "#":
        # Wall, don't move
        return robot_pos
    else:
        # Box, check if boxes can move
        assert grid[next_pos] == "O"

        # Find next free space (if any) in this direction
        search_point = movement(next_pos)
        while search_point in grid:
            if grid[search_point] == "#":
                # Cannot move
                return robot_pos
            elif grid[search_point] == ".":
                # Found free space
                break
            search_point = movement(search_point)
        else:
            # Did not find free space (break in loop), cannot move
            return robot_pos

        # Free space found; instead of moving all boxes,
        # just move the box from next_pos to search_point...
        grid[search_point] = grid[next_pos]
        grid[next_pos] = "."
        # ... and put the robot at next_pos
        return next_pos


def gps_coordinate(box_pos: Point) -> int:
    return 100 * box_pos[0] + box_pos[1]


def p1(grid: Grid, robot_pos: Point, instructions: str) -> int:
    for instruction in instructions:
        robot_pos = move_robot(grid, robot_pos, CHAR_TO_DIRECTION[instruction])
    return sum(gps_coordinate(pos) for pos, char in grid.items() if char == "O")


def other_half(grid: Grid, box_pos: Point) -> Point:
    assert grid[box_pos] in "[]"
    if grid[box_pos] == "[":
        return right(box_pos)
    elif grid[box_pos] == "]":
        return left(box_pos)
    else:
        raise Exception


def move_robot_wide(grid: Grid, robot_pos: Point, movement: MovementFunction) -> Point:
    next_pos = movement(robot_pos)
    assert next_pos in grid

    if grid[next_pos] == ".":
        # Free space, move
        return next_pos
    elif grid[next_pos] == "#":
        # Wall, don't move
        return robot_pos
    else:
        # Box, check if boxes can move
        assert grid[next_pos] in "[]"

        # Find all adjacent boxes to move
        if movement in (up, down):
            box_parts_to_check = {next_pos, other_half(grid, next_pos)}
            checked = set()
        else:
            # assert movement in (left, right)
            # assert grid[next_pos] == "]"
            box_parts_to_check = {other_half(grid, next_pos)}
            checked = {next_pos}

        # Follow from box parts in movement direction
        while box_parts_to_check:
            box_part = box_parts_to_check.pop()
            checked.add(box_part)

            search_point = movement(box_part)
            if grid[search_point] == ".":
                # If next tile in movement direction is free, ok, continue checking other box parts
                continue
            elif grid[search_point] == "#":
                # If hit wall, cannot move anything, stop trying
                return robot_pos
            elif grid[search_point] in "[]":
                # Another box to move, check its components
                if movement in (up, down):
                    box_parts_to_check.add(search_point)
                    box_parts_to_check.add(other_half(grid, search_point))
                else:
                    # assert movement in (left, right)
                    box_parts_to_check.add(other_half(grid, search_point))
                    checked.add(search_point)
        else:
            # Checked all box parts, move boxes
            sort_fn = {
                up: {"key": lambda p: p[0], "reverse": False},
                down: {"key": lambda p: p[0], "reverse": True},
                left: {"key": lambda p: p[1], "reverse": False},
                right: {"key": lambda p: p[1], "reverse": True},
            }
            for point in sorted(checked, **sort_fn[movement]):
                to_point = movement(point)
                grid[to_point] = grid[point]
                grid[point] = "."

            # Move robot to next position
            return next_pos


def p2(grid: Grid, robot_pos: Point, instructions: str) -> int:
    for instruction in instructions:
        robot_pos = move_robot_wide(grid, robot_pos, CHAR_TO_DIRECTION[instruction])
    return sum(gps_coordinate(pos) for pos, char in grid.items() if char == "[")


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        lines = file.read()

    data = parse(lines)
    # print_grid(data[0], data[1])
    print(f"Part 1: {p1(*data)}")

    wide_data = parse_wide(lines)
    # print_grid(wide_data[0], wide_data[1])
    print(f"Part 2: {p2(*wide_data)}")
