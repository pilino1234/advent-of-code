from functools import cache


def parse(line_data: str) -> tuple[list[str], list[str]]:
    patterns, designs = line_data.split("\n\n")

    patterns = patterns.split(", ")
    designs = designs.splitlines()

    return patterns, designs


def can_make(design: str, patterns: list[str]) -> bool:
    to_check = [([], design)]

    while to_check:
        current, remaining = to_check.pop()
        if not remaining:
            return True
        for pattern in patterns:
            if remaining.startswith(pattern):
                to_check.append((current + [pattern], remaining.removeprefix(pattern)))
    return False


def ways_to_make(design: str, patterns: list[str]) -> int:
    @cache
    def count_ways(remaining: str) -> int:
        if not remaining:
            return 1

        ways = 0
        for pattern in patterns:
            if remaining.startswith(pattern):
                ways += count_ways(remaining.removeprefix(pattern))
        return ways

    return count_ways(design)


def p1(patterns: list[str], designs: list[str]) -> int:
    return sum(can_make(design, patterns) for design in designs)


def p2(patterns: list[str], designs: list[str]) -> int:
    return sum(ways_to_make(design, patterns) for design in designs)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
    #     lines = [line.strip() for line in file]
        lines = file.read()

    data = parse(lines)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(*data)}")
