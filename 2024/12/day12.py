from typing import Callable

Point = tuple[int, int]

Region = set[Point]

MovementFunction = Callable[[Point], Point]
up: MovementFunction = lambda p: (p[0] - 1, p[1])
down: MovementFunction = lambda p: (p[0] + 1, p[1])
left: MovementFunction = lambda p: (p[0], p[1] - 1)
right: MovementFunction = lambda p: (p[0], p[1] + 1)


def parse(line_data: list[str]) -> list[Region]:
    grid: dict[Point, str] = {}
    for row_idx, row in enumerate(line_data):
        for col_idx, region_label in enumerate(row):
            grid[(row_idx, col_idx)] = region_label

    regions = []
    to_visit = set(grid.keys())
    while to_visit:
        pos = to_visit.pop()
        region_label = grid[pos]
        region = {pos}

        q = [pos]
        while q:
            p = q.pop()
            for nb in [up, down, left, right]:
                next_point = nb(p)
                if next_point not in grid:
                    continue
                if next_point in region:
                    continue

                next_region = grid[next_point]
                if next_region != region_label:
                    continue

                region.add(next_point)
                q.append(next_point)

        to_visit.difference_update(region)
        regions.append(region)

    return regions


def region_perimeter(region: Region) -> int:
    perimeter = 0
    for p in region:
        for nb in [up, down, left, right]:
            if nb(p) not in region:
                perimeter += 1
    return perimeter


def region_sides(region: Region) -> int:
    sides = 0

    fence_segments: set[tuple[Point, MovementFunction]] = set()
    for p in region:
        for nb in [up, down, left, right]:
            if nb(p) not in region:
                fence_segments.add((p, nb))

    while fence_segments:
        (point, fence_side) = fence_segments.pop()

        # Neighbours in these 2 directions can have fence that is part of the same side
        directions_to_check = {
            up: (left, right),
            down: (left, right),
            left: (up, down),
            right: (up, down),
        }[fence_side]

        for direction in directions_to_check:
            # Follow fence in this direction until fence ends
            nb_pos = direction(point)
            while nb_pos in region:
                if (nb_pos, fence_side) in fence_segments:
                    # Fence continues, still same side
                    assert (nb_pos, fence_side) in fence_segments
                    fence_segments.remove((nb_pos, fence_side))
                else:
                    # Fence ends, stop following
                    break
                nb_pos = direction(nb_pos)
        sides += 1

    return sides


def p1(regions: list[Region]) -> int:
    result = 0
    for region in regions:
        area = len(region)
        perimeter = region_perimeter(region)
        result += area * perimeter
    return result


def p2(regions: list[Region]) -> int:
    result = 0
    for region in regions:
        area = len(region)
        sides = region_sides(region)
        result += area * sides
    return result


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    # with open("test3.txt") as file:
    # with open("test4.txt") as file:
    # with open("test5.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
