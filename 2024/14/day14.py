import collections
import re
from dataclasses import dataclass
from typing import NamedTuple, Self


# PointT = tuple[int, int]
class Point(NamedTuple):
    x: int
    y: int

    @property
    def row(self):
        return self.x

    @property
    def col(self):
        return self.y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __mod__(self, other):
        return Point(self.x % other[0], self.y % other[1])


@dataclass
class Robot:
    pos: Point
    vel: Point

    @classmethod
    def from_str(cls, string: str) -> Self:
        pattern = re.compile(r"p=(?P<col>-?\d+),(?P<row>-?\d+) v=(?P<v_col>-?\d+),(?P<v_row>-?\d+)")
        match = pattern.match(string)
        return Robot(
            pos=Point(int(match.group("row")), int(match.group("col"))),
            vel=Point(int(match.group("v_row")), int(match.group("v_col"))),
        )


def print_grid(robots: list[Robot], room_height: int, room_width: int) -> None:
    robots_at_pos = collections.Counter(r.pos for r in robots)
    for row in range(room_height):
        for col in range(room_width):
            pos = Point(row, col)
            if pos in robots_at_pos:
                char = robots_at_pos[pos]
            else:
                char = "."
            print(char, end="")
        print()


def parse(line_data: list[str]) -> list[Robot]:
    return [Robot.from_str(line) for line in line_data]


def simulate(robots: list[Robot], room_height: int, room_width: int):
    for robot in robots:
        robot.pos += robot.vel
        robot.pos %= (room_height, room_width)


def p1(robots: list[Robot], room_height: int, room_width: int, seconds: int) -> int:
    for _ in range(seconds):
        simulate(robots, room_height, room_width)
    # print_grid(robots, room_height, room_width)

    vertical = room_width // 2
    horizontal = room_height // 2
    q1 = sum(r.pos.col < vertical and r.pos.row < horizontal for r in robots)
    q2 = sum(r.pos.col > vertical and r.pos.row < horizontal for r in robots)
    q3 = sum(r.pos.col < vertical and r.pos.row > horizontal for r in robots)
    q4 = sum(r.pos.col > vertical and r.pos.row > horizontal for r in robots)
    return q1 * q2 * q3 * q4


def p2_manual(robots: list[Robot], room_height: int, room_width: int) -> None:
    seconds = 0
    how_many_more = 1
    while True:
        for _ in range(how_many_more):
            simulate(robots, room_height, room_width)
            seconds += 1
        print_grid(robots, room_height, room_width)
        print(f"{seconds} seconds")
        how_many_more = input()
        if not how_many_more.strip():
            how_many_more = 1
        else:
            how_many_more = int(how_many_more)


def p2_automatic(robots: list[Robot], room_height: int, room_width: int) -> int:
    # Could do CRT, but robot movements cycle after room_height*room_width seconds so this is fast enough
    cycle_length = find_cycle(robots[0], room_height, room_width)
    # for r in robots[1:]:
    #     assert find_cycle(r, room_height, room_width) == cycles

    # Find the first common frame in the vertical and horizontal alignment sequences
    s1_offset = 4
    s1_period = room_height
    s1_value = s1_offset
    s1_elements = {s1_value}
    for _ in range(cycle_length):
        s1_value += s1_period
        s1_elements.add(s1_value)

    s2_offset = 29
    s2_period = room_width
    s2_value = s2_offset
    for _ in range(cycle_length):
        s2_value += s2_period
        if s2_value in s1_elements:
            return s2_value


def find_cycle(robot: Robot, room_height: int, room_width: int) -> int:
    r2 = Robot(robot.pos, robot.vel)
    seen = {r2.pos}
    while True:
        simulate([r2], room_height, room_width)
        if r2.pos in seen:
            assert r2.pos == robot.pos
            return len(seen)
        seen.add(r2.pos)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    # data = [Robot.from_str("p=2,4 v=2,-3")]
    # print_grid(data, 7, 11)

    print(f"Part 1: {p1(data, room_height=103, room_width=101, seconds=100)}")

    data = parse(lines)  # Fresh input
    # print(f"Part 2: {p2_manual(data, room_height=103, room_width=101)}")
    christmas_tree_seconds = p2_automatic(data, room_height=103, room_width=101)
    print(f"Part 2: {christmas_tree_seconds}")

    # Print the frame
    data = parse(lines)  # Fresh input
    for _ in range(christmas_tree_seconds):
        simulate(data, room_height=103, room_width=101)
    print_grid(data, room_height=103, room_width=101)
