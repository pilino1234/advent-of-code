import re


def p1(program: str) -> int:
    mul_pattern = re.compile(r"mul\((\d{1,3}),(\d{1,3})\)")

    result = 0
    for match in re.findall(mul_pattern, program):
        result += (int(match[0]) * int(match[1]))
    return result


def p2(program: str) -> int:
    mul_pattern = re.compile(r"^mul\((\d{1,3}),(\d{1,3})\)")
    do_pattern = re.compile(r"^do\(\)")
    dont_pattern = re.compile(r"^don't\(\)")

    result = 0
    do_mul = True
    for pos in range(len(program)):
        if re.match(dont_pattern, program[pos:]):
            do_mul = False
        elif re.match(do_pattern, program[pos:]):
            do_mul = True
        elif do_mul and (match := re.match(mul_pattern, program[pos:])):
            result += (int(match.groups()[0]) * int(match.groups()[1]))
    return result


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]
        data = "".join(lines)

    assert p1("mul(4*") == 0
    assert p1("mul(6,9!") == 0
    assert p1("?(12,34)") == 0
    assert p1("mul ( 2 , 4 )") == 0

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
