from collections import defaultdict


OrderingRules = dict[int, set[int]]
Update = list[int]


def parse(line_data: str) -> tuple[OrderingRules, list[Update]]:
    raw_rules, raw_updates = line_data.split("\n\n")
    raw_rules = [r.strip() for r in raw_rules.splitlines()]
    raw_updates = [p.strip() for p in raw_updates.splitlines()]

    rules = defaultdict(set)
    for rule in raw_rules:
        first, second = map(int, rule.split("|"))
        rules[first].add(second)

    return (
        rules,
        [list(map(int, u.split(","))) for u in raw_updates]
    )


def validate_update(update: Update, rules: OrderingRules) -> bool:
    for idx, num in enumerate(update):
        if num not in rules:
            continue
        preceding_nums = set(update[:idx])

        if not preceding_nums.isdisjoint(rules[num]):
            return False
    return True


def p1(updates: list[Update], rules: OrderingRules) -> int:
    result = 0
    for upd in updates:
        if validate_update(upd, rules):
            middle = upd[len(upd)//2]
            result += middle
    return result


def fix_update(update: Update, rules: OrderingRules) -> Update:
    upd = update.copy()
    while not validate_update(upd, rules):
        for idx, num in enumerate(upd):
            if num not in rules:
                continue
            for idx2, preceding_num in enumerate(upd[:idx]):
                if preceding_num in rules[num]:
                    upd[idx], upd[idx2] = preceding_num, num
                    break

    return upd


def p2(updates: list[Update], rules: OrderingRules) -> int:
    result = 0
    for upd in updates:
        if not validate_update(upd, rules):
            fixed = fix_update(upd, rules)
            middle = fixed[len(fixed)//2]
            result += middle
    return result


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = file.read()

    ordering_rules, updates = parse(lines)

    print(f"Part 1: {p1(updates, ordering_rules)}")
    print(f"Part 2: {p2(updates, ordering_rules)}")
