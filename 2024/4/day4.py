

def flip_horizontal(grid: list[str]) -> list[str]:
    return [line[::-1] for line in grid]


def flip_vertical(grid: list[str]) -> list[str]:
    return list(reversed(grid))


def transpose(grid: list[str]) -> list[str]:
    return ["".join(line[idx] for line in grid) for idx in range(len(grid[0]))]


def rotate(grid: list[str]) -> list[str]:
    return flip_horizontal(transpose(grid))


def count(grid: list[str], pattern: str = "XMAS") -> int:
    return sum(l.count(pattern) for l in grid)


def check_diagonal(pos: tuple[int, int], grid: list[str], pattern: str) -> bool:
    row, col = pos
    for offset, char in enumerate(pattern):
        try:
            if grid[row + offset][col + offset] != char:
                return False
        except IndexError:
            return False
    return True


def count_diagonal(grid: list[str], pattern: str = "XMAS") -> int:
    total = 0
    for row_idx, row in enumerate(grid):
        for col_idx in range(len(row)):
            total += check_diagonal((row_idx, col_idx), grid, pattern)
    return total


def check_x(pos: tuple[int, int], grid: list[str], pattern: str) -> bool:
    row, col = pos
    found_mas = check_diagonal((row, col), grid, pattern)
    if found_mas:
        flipped_grid = flip_vertical(grid)
        # Start row of second MAS in flipped grid
        flipped_row = (len(flipped_grid) - 1 - row) - 2
        found_matching_mas = check_diagonal((flipped_row, col), flipped_grid, pattern)
        if found_matching_mas:
            return True
    return False


def count_x(grid: list[str], pattern: str = "MAS") -> int:
    total = 0
    for row_idx, row in enumerate(grid):
        for col_idx in range(len(row)):
            total += check_x((row_idx, col_idx), grid, pattern)
    return total


def p1(grid: list[str]) -> int:
    total = 0
    for _ in range(4):
        grid = rotate(grid)
        total += count(grid)
        total += count_diagonal(grid)
    return total


def p2(grid: list[str]) -> int:
    total = 0
    for _ in range(4):
        grid = rotate(grid)
        total += count_x(grid)
    return total


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    # with open("test3.txt") as file:
    # with open("test4.txt") as file:
    # with open("test5.txt") as file:
    with open("input.txt") as file:
        data = [line.strip() for line in file]

    horizontal1 = """\
XMAS
....
....
....""".splitlines()
    assert p1(horizontal1) == 1

    horizontal2 = """\
....
SAMX
....
....""".splitlines()
    assert p1(horizontal2) == 1

    vertical1 = """\
X...
M...
A...
S...""".splitlines()
    assert p1(vertical1) == 1

    vertical2 = """\
..S.
..A.
..M.
..X.""".splitlines()
    assert p1(vertical2) == 1

    diagonal1 = """\
X...
.M..
..A.
...S
""".splitlines()
    assert p1(diagonal1) == 1

    diagonal2 = """\
...S
..A.
.M..
X...""".splitlines()
    assert p1(diagonal2) == 1

    diagonal3 = """\
S...
.A..
..M.
...X""".splitlines()
    assert p1(diagonal3) == 1

    diagonal4 = """\
...X
..M.
.A..
S...""".splitlines()
    assert p1(diagonal4) == 1

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
