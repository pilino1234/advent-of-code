from itertools import product

Lock = tuple[int, int, int, int, int]
Key = tuple[int, int, int, int, int]


def v1(file_data: str) -> tuple[list[Lock], list[Key]]:
    parts = file_data.strip().split("\n\n")
    locks = []
    keys = []
    for part in parts:
        part_lines = part.split("\n")
        if all(c == "#" for c in part_lines[0]) and all(c == "." for c in part_lines[-1]):
            # Lock
            column_heights = []
            columns = len(part_lines[0])
            for col in range(columns):
                for row_num, row in enumerate(part_lines[1:]):
                    if row[col] == ".":
                        column_heights.append(row_num)
                        break
            locks.append(tuple(column_heights))

        elif all(c == "." for c in part_lines[0]) and all(c == "#" for c in part_lines[-1]):
            # Key
            column_heights = []
            columns = len(part_lines[0])
            for col in range(columns):
                for row_num, row in enumerate(part_lines[1:]):
                    if row[col] == "#":
                        column_heights.append(abs(5 - row_num))
                        break
            keys.append(tuple(column_heights))
        else:
            assert False

    fitting_keys = 0
    for lock, key in product(locks, keys):
        for lock_col, key_col in zip(lock, key):
            if lock_col + key_col > 5:
                # Does not fit
                break
        else:
            # Fits!
            fitting_keys += 1
    return fitting_keys


def v2(file_data: str) -> int:
    locks = []
    keys = []
    for part in file_data.strip().split("\n\n"):
        part_lines = part.split("\n")
        is_lock = "#" in part_lines[0]
        positions = set()
        for row_num, row in enumerate(part_lines[1:]):
            for col_num, char in enumerate(row):
                if char == "#":
                    positions.add((row_num, col_num))
        (locks if is_lock else keys).append(positions)

    fitting_keys = 0
    for lock, key in product(locks, keys):
        if lock.isdisjoint(key):
            fitting_keys += 1

    return fitting_keys


def v3(file_data: str) -> int:
    locks, keys = [], []
    for part in file_data.strip().split("\n\n"):
        (locks if part[0] == "#" else keys).append({(row_num, col_num) for row_num, row in enumerate(part.split("\n")) for col_num, char in enumerate(row) if char == "#"})
    return sum(lock.isdisjoint(key) for lock, key in product(locks, keys))


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = file.read()

    print(f"Part 1: {v1(lines)}")
    print(f"Part 1: {v2(lines)}")
    print(f"Part 1: {v3(lines)}")
