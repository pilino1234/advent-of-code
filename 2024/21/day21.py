from collections import defaultdict
from collections.abc import Sequence
from typing import Literal

Point = tuple[int, int]
Keypad = dict[str, Point]
Move = Literal["^", "v", "<", ">", "A"]


def build_numpad() -> Keypad:
    """
    +---+---+---+
    | 7 | 8 | 9 |
    +---+---+---+
    | 4 | 5 | 6 |
    +---+---+---+
    | 1 | 2 | 3 |
    +---+---+---+
        | 0 | A |
        +---+---+
    """
    return {
        "7": (0, 0), "8": (0, 1), "9": (0, 2),
        "4": (1, 0), "5": (1, 1), "6": (1, 2),
        "1": (2, 0), "2": (2, 1), "3": (2, 2),
        ...: (3, 0), "0": (3, 1), "A": (3, 2),
    }


def build_arrow_keys() -> Keypad:
    """
        +---+---+
        | ^ | A |
    +---+---+---+
    | < | v | > |
    +---+---+---+
    """
    return {
        ...: (0, 0), "^": (0, 1), "A": (0, 2),
        "<": (1, 0), "v": (1, 1), ">": (1, 2),
    }


NUMPAD = build_numpad()
ARROWPAD = build_arrow_keys()


def navigate_numpad(start_pos: Point, target_pos: Point) -> str:
    path: list[Move] = []

    vertical_distance = target_pos[0] - start_pos[0]
    vertical_move: Literal["v", "^"] = "^" if vertical_distance < 0 else "v"
    horizontal_distance = target_pos[1] - start_pos[1]
    horizontal_move: Literal["<", ">"] = "<" if horizontal_distance < 0 else ">"

    if (start_pos == NUMPAD["A"] and horizontal_distance == -2) or (
        start_pos == NUMPAD["0"] and horizontal_distance == -1
    ):
        # If moving up and left from bottom row,
        # move vertical first then horizontal to avoid the gap
        path.extend([vertical_move] * abs(vertical_distance))
        path.extend([horizontal_move] * abs(horizontal_distance))
    elif start_pos[1] == 0 and target_pos[0] == 3:
        # moving from leftmost col to bottom row; horizontal first
        path.extend([horizontal_move] * abs(horizontal_distance))
        path.extend([vertical_move] * abs(vertical_distance))
    else:
        # Reduce arrow key pad distance
        # Perform leftwards movements early, then vertical, then rightwards
        if horizontal_distance < 0:
            path.extend([horizontal_move] * abs(horizontal_distance))
        path.extend([vertical_move] * abs(vertical_distance))
        if horizontal_distance > 0:
            path.extend([horizontal_move] * abs(horizontal_distance))

    # Press the button
    path.append("A")
    return "".join(path)


def navigate_arrowpad(start_pos: Point, target_pos: Point) -> str:
    path: list[Move] = []

    vertical_distance = target_pos[0] - start_pos[0]
    vertical_move: Literal["v", "^"] = "^" if vertical_distance < 0 else "v"
    horizontal_distance = target_pos[1] - start_pos[1]
    horizontal_move: Literal["<", ">"] = "<" if horizontal_distance < 0 else ">"

    if start_pos == ARROWPAD["<"] and vertical_distance < 0:
        # If moving up from leftmost column,
        # move horizontal first then vertical to avoid the gap
        path.extend([horizontal_move] * abs(horizontal_distance))
        path.extend([vertical_move] * abs(vertical_distance))
    elif (start_pos == ARROWPAD["^"] and horizontal_distance == -1) or (
        start_pos == ARROWPAD["A"] and horizontal_distance == -2
    ):
        # If moving left from upper row,
        # move vertical first then horizontal to avoid the gap
        path.extend([vertical_move] * abs(vertical_distance))
        path.extend([horizontal_move] * abs(horizontal_distance))
    else:
        # Reduce arrow key pad distance
        # Perform leftwards movements early, then vertical, then rightwards
        if horizontal_distance < 0:
            path.extend([horizontal_move] * abs(horizontal_distance))
        path.extend([vertical_move] * abs(vertical_distance))
        if horizontal_distance > 0:
            path.extend([horizontal_move] * abs(horizontal_distance))

    # Press the button
    path.append("A")
    return "".join(path)


def numpad_sequence(code: str) -> dict[Sequence[Move], int]:
    pos = NUMPAD["A"]
    sequences = defaultdict(int)
    for char in code:
        target = NUMPAD[char]
        sequences[navigate_numpad(pos, target)] += 1
        pos = target
    return sequences


def arrowpad_sequence(sequences: dict[Sequence[Move], int]) -> dict[Sequence[Move], int]:
    return_sequence = defaultdict(int)
    for prev_sequence, prev_count in sequences.items():
        pos = ARROWPAD["A"]
        for char in prev_sequence:
            target = ARROWPAD[char]
            return_sequence[navigate_arrowpad(pos, target)] += prev_count
            pos = target

    return return_sequence


def code_complexity(code: str, my_keypad_sequences: dict[Sequence[Move], int]) -> int:
    return int(code[0:3]) * sum(len(seq) * count for seq, count in my_keypad_sequences.items())


def find_complexity(code: str, robots: int) -> int:
    current_sequences = numpad_sequence(code)
    for robot in range(robots):
        current_sequences = arrowpad_sequence(current_sequences)
    return code_complexity(code, current_sequences)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        data = [line.strip() for line in file]

    print(f"Part 1: {sum(find_complexity(c, 2) for c in data)}")
    print(f"Part 2: {sum(find_complexity(c, 25) for c in data)}")
