import bisect
from collections import defaultdict
from math import inf
from typing import Callable, NamedTuple


class Point(NamedTuple):
    row: int
    col: int

    @property
    def x(self):
        return self.col

    @property
    def y(self):
        return self.row


Grid = set[Point]

MovementFunction = Callable[[Point], Point]
up: MovementFunction = lambda p: Point(p.row - 1, p.col)
down: MovementFunction = lambda p: Point(p.row + 1, p.col)
left: MovementFunction = lambda p: Point(p.row, p.col - 1)
right: MovementFunction = lambda p: Point(p.row, p.col + 1)


def parse(line_data: list[str]) -> list[Point]:
    parsed = []
    for line in line_data:
        x, y = map(int, line.split(","))
        parsed.append(Point(y, x))
    return parsed


def print_grid(set_map: Grid, grid_size: int) -> None:
    for row in range(grid_size):
        for col in range(grid_size):
            pos = (row, col)
            if pos in set_map:
                char = "#"
            else:
                char = "."
            print(char, end="")
        print()


def recover_path(start: Point, end: Point, prev: dict[Point, Point | None]) -> list[Point]:
    path = []
    pos = end
    while (pos is not None) and (pos != start):
        path.append(pos)
        pos = prev[pos]
    return path


def dijkstra(
        obstacles: set[Point],
        start_point: Point,
        end_point: Point,
        grid_size: int=71,
) -> list[Point]:
    # Homemade Dijkstra's
    distance: dict[Point, int | float] = defaultdict(lambda: inf)
    distance[start_point] = 0
    prev: dict[Point, Point | None] = defaultdict(lambda: None)

    to_visit = set()
    for row in range(grid_size):
        for col in range(grid_size):
            p = Point(row, col)
            if p in obstacles:
                continue
            to_visit.add(Point(row, col))

    while to_visit:
        min_dist = inf
        pos_to_process = None
        for pos in to_visit:
            if (dist := distance[pos]) < min_dist:
                min_dist = dist
                pos_to_process = pos

        if pos_to_process is None:
            # raise Exception
            # No more reachable positions
            break

        to_visit.remove(pos_to_process)

        for nb in [up, down, left, right]:
            nb_pos = nb(pos_to_process)
            if not (0 <= nb_pos.row < grid_size and 0 <= nb_pos.col < grid_size):
                continue
            elif nb_pos in obstacles:
                continue
            next_dist = min_dist + 1
            if next_dist < distance[nb_pos]:
                prev[nb_pos] = pos_to_process
                distance[nb_pos] = next_dist

    return recover_path(start_point, end_point, prev)


def p1(
        byte_positions: list[Point],
        start: Point,
        end: Point,
        grid_size: int,
        p1_obstacles: int,
) -> int:
    obstacles = set(byte_positions[:p1_obstacles])
    print_grid(obstacles, grid_size)
    return len(dijkstra(obstacles, start, end, grid_size))


def p2(
        byte_positions: list[Point],
        start: Point,
        end: Point,
        grid_size: int,
        p1_obstacles: int,
) -> str:
    def bisect_key(num_obstacles: int) -> int:
        # print(f"Bisecting for {num_obstacles}")
        obstacles = set(byte_positions[:num_obstacles])
        # assert len(obstacles) == num_obstacles
        path = dijkstra(obstacles, start, end, grid_size)
        return inf if path == [end] else len(path)

    bisect_result = bisect.bisect_left(
        # Binary search over a list of numbers of obstacles to add
        # The first `p1_obstacles` can be skipped since we know from p1 that
        # they are still fine.
        range(p1_obstacles, len(byte_positions)),
        # A big value bigger than any valid path length but smaller than inf
        grid_size**2,
        # This key function maps the number of obstacles -> path length
        # For increasing number of obstacles, path length will increase
        # until it jumps to inf beyond a certain point (the solution)
        key=bisect_key
    )
    last_obstacle = byte_positions[p1_obstacles + bisect_result - 1]
    return f"{last_obstacle.x},{last_obstacle.y}"


if __name__ == "__main__":
    # with open("test1.txt") as file:
    #     grid_dimension = 6
    #     bytes_to_place = 12
    with open("input.txt") as file:
        grid_dimension = 70
        bytes_to_place = 1024
        lines = [line.strip() for line in file]

    data = parse(lines)
    # print(data)
    # print_grid(data)

    start_pos = Point(0, 0)
    end_pos = Point(grid_dimension, grid_dimension)

    print(f"Part 1: {p1(data, start_pos, end_pos, grid_size=grid_dimension+1, p1_obstacles=bytes_to_place)}")
    print(f"Part 2: {p2(data, start_pos, end_pos, grid_size=grid_dimension+1, p1_obstacles=bytes_to_place)}")
