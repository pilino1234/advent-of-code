from typing import Callable

Point = tuple[int, int]
Grid = dict[Point, int]

MovementFunction = Callable[[Point], Point]
up: MovementFunction = lambda p: (p[0] - 1, p[1])
down: MovementFunction = lambda p: (p[0] + 1, p[1])
left: MovementFunction = lambda p: (p[0], p[1] - 1)
right: MovementFunction = lambda p: (p[0], p[1] + 1)



def parse(line_data: list[str]) -> Grid:
    grid = {}
    for row_idx, row in enumerate(line_data):
        for col_idx, elevation in enumerate(row):
            if elevation == ".":
                elevation = -1
            grid[(row_idx, col_idx)] = int(elevation)
    return grid


def print_grid(dict_map: Grid) -> None:
    xs = [key[0] for key in dict_map]
    ys = [key[1] for key in dict_map]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    for row in range(miny, maxy + 1):
        for col in range(minx, maxx + 1):
            pos = (row, col)
            char = dict_map[pos]
            if char == -1:
                char = "."
            print(char, end="")
        print()
    print()


def find_scores(grid: Grid, trailhead: Point) -> tuple[int, int]:
    seen = set()
    to_visit = [trailhead]

    trail_rating = 0

    while to_visit:
        current_point = to_visit.pop(0)
        current_elevation = grid[current_point]

        # Check neighbours
        for nb in [up, down, left, right]:
            next_point = nb(current_point)
            if next_point in seen:
                continue
            elif next_point not in grid:
                continue

            next_elevation = grid[next_point]
            if next_elevation == current_elevation + 1:
                to_visit.append(next_point)

        if current_elevation == 9:
            trail_rating += 1

        seen.add(current_point)

    trail_score = 0
    for pos in seen:
        elevation = grid[pos]
        if elevation == 9:
            trail_score += 1
    return trail_score, trail_rating


def p1_and_p2(grid: Grid) -> tuple[int, int]:
    trailheads = [pos for pos, elevation in grid.items() if elevation == 0]

    total_score = 0
    total_rating = 0
    for trailhead in trailheads:
        score, rating = find_scores(grid, trailhead)
        total_score += score
        total_rating += rating
    return total_score, total_rating


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    # with open("test3.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    print_grid(data)

    p1, p2 = p1_and_p2(data)
    print(f"Part 1: {p1}")
    print(f"Part 2: {p2}")
