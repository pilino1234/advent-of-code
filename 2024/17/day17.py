import re
from typing import Literal

Register = int
ThreeBitValue = Literal[0, 1, 2, 3, 4, 5, 6, 7]
Program = list[ThreeBitValue]


def parse(line_data: str) -> tuple[Register, Register, Register, Program]:
    program_pattern = re.compile(
        r"Register A: (?P<reg_a>\d+)\n"
        r"Register B: (?P<reg_b>\d+)\n"
        r"Register C: (?P<reg_c>\d+)\n"
        r"\n"
        r"Program: (?P<program>[,\d]+)"
    )
    match = program_pattern.match(line_data)
    program: Program = list(map(int, match.group("program").split(",")))

    return (
        int(match.group("reg_a")),
        int(match.group("reg_b")),
        int(match.group("reg_c")),
        program,
    )


def resolve_combo(combo: ThreeBitValue, a, b, c):
    match combo:
        case 0 | 1 | 2 | 3:
            return combo
        case 4:
            return a
        case 5:
            return b
        case 6:
            return c
        case 7:
            raise Exception


def run(program: Program, a: Register, b: Register, c: Register) -> str:
    output_buffer = []

    pc = 0
    while True:
        ins = program[pc]
        op = program[pc + 1]

        match ins:
            case 0 | 6 | 7:  # adv, combo
                numerator = a
                denominator = 2 ** resolve_combo(op, a, b, c)
                result = int(numerator / denominator)
                if ins == 0:
                    a = result
                elif ins == 6:
                    b = result
                elif ins == 7:
                    c = result
            case 1:  # bxl, literal
                b = op ^ b
            case 2:  # bst, combo
                b = resolve_combo(op, a, b, c) % 8
            case 3:  # jnz, literal
                if a == 0:
                    pass
                else:
                    pc = op - 2  # offset for += 2 below
            case 4:  # bxc, operand is ignored
                b = b ^ c
            case 5:  # out, combo
                result = resolve_combo(op, a, b, c) % 8
                output_buffer.append(str(result))

        pc += 2
        if pc >= len(program):
            break

    return ",".join(output_buffer)


def p1(a: Register, b: Register, c: Register, program: Program) -> str:
    return run(program, a, b, c)


def optimized(a: Register, _b: Register, _c: Register) -> str:
    output_buffer = []
    while True:
        b = a % 8
        b ^= 0b010
        c = a >> b
        b ^= c
        b ^= 0b011
        output_buffer.append(str(b % 8))
        a >>= 3
        if a == 0:
            break
    return ",".join(output_buffer)


def p2(program: Program) -> ...:
    ...
    # Approach 1
    # mapping = {}
    # for a_value in range(0b000, 0b111 + 1):
    #     result = int(run(program, a_value, 0, 0))
    #     print(f"{a_value} -> {result}")
    #     if result not in mapping:
    #         mapping[result] = a_value
    #     # mapping[a_value] = result
    #
    # needed_a_value = []
    # for program_number in program:
    #     needed_a_value.append(mapping[program_number])
    #
    # print(needed_a_value)
    # ...

    # print("===============")
    # for a_value1 in range(0b000, 0b111 + 1):
    #     for a_value2 in range(0b000, 0b111 + 1):
    #         a_value = a_value2 << 3 + a_value1
    #         # print(a_value)
    #         result = run(program, a_value, 0, 0)
    #         print(f"{a_value} -> {result}")
    # print("===============")

    # for a_value in range(1000):
    #     # a_value = a_value2 << 3 + a_value1
    #     # print(a_value)
    #     result = run(program, a_value, 0, 0)
    #     if "2" in result:
    #         print(f"{a_value} ({bin(a_value)}) -> {result}")
    #     else:
    #         print(f"{a_value} -> {result}")
    # print("===============")

    # Table for instruction 2
    # for B_value in range(0b000, 0b111 + 1):
    #     result = B_value ^ 0b010
    #     print(f"B={B_value} -> b={result:03b} ({result})")

    # a_value = 0
    required_output = str(",".join(str(p) for p in program))
    # while True:
    #     result = run(program, a_value, 0, 0)
    #     # print(f"{a_value} -> {result}")
    #     if required_output.startswith(result):
    #         print(f"{a_value} -> {result}")
    #     a_value += 1

    # Test that optimized works correctly
    # for a_value in range(1_000_000):
    #     print(a_value)
    #     original = run(program, a_value, 0, 0)
    #     new = optimized(a_value, 0, 0)
    #     assert original == new

    # a_value = (1 << 47)
    # required_output = str(",".join(str(p) for p in program))
    # while True:
    #     result = optimized(a_value, 0, 0)
    #     if a_value % 1_000_000 == 0:
    #         # print(a_value)
    #         print(f"{a_value} -> {result}")
    #     if required_output.startswith(result):
    #         print(f"{a_value} -> {result}")
    #     if result == required_output:
    #         break
    #     a_value += 1

    # a_value = 50135565647348
    # a_value = (1 << 48) - 1
    # a_value = 163543563370496
    # a_value = 163543630479359
    # print(a_value, optimized(a_value, 0, 0))

    # RE notes
    # Program: 2, 4, 1, 2, 7, 5, 4, 5, 1, 3, 5, 5, 0, 3, 3, 0
    # 2, 4,
    # 1, 2,
    # 7, 5,
    # 4, 5,
    # 1, 3,
    # 5, 5,
    # 0, 3,
    # 3, 0

    # bst, A % 8
    # bxl, 2,
    # cdv, B,
    # bxc, 5,
    # bxl, 3,
    # out, B % 8,
    # adv, 3,
    # jnz, 0

    # bst, A % 8   B <- A % 8
    # bxl, 2,      B <- B ^ 2
    # cdv, B,      C <- A / 2**B
    # bxc, 5,      B <- B ^ C
    # bxl, 3,      B <- B ^ 3
    # out, B % 8,  print B % 8
    # adv, 3,      A <- A / 2**3
    # jnz, 0       goto start

    # B <- A % 8        (keep only lowest 3 bits)
    # B <- B ^ 2        (xor with 0b10)
    # C <- A / 2**B     (truncated to integer)
    # B <- B ^ C        (xor with C, operand ignored)
    # B <- B ^ 3        (xor with 0b11)
    # print B % 8       (print lowest 3 bits)
    # A <- A / 8        (a /= 8, truncate to integer)
    # goto start        (repeat until A == 0)

    # B <- low 3 bits of A
    # B <- B ^ 0b10
    # C <- A / 2**B
    # B <- B ^ C
    # B <- B ^ 0b11
    # print low 3 bits of B
    # A <- A >> 3               # bit shift right 3 bits
    # repeat

    # A is input number, processed in chunks of 3 bits
    # Each 3-bit section produces some output number
    # Figure out this function, then reverse to see what A needs to be initially
    # Mapping that seems to be produced for single input --> single output:
    # 0 -> 1
    # 1 -> 0
    # 2 -> 1
    # 3 -> 3
    # 4 -> 5
    # 5 -> 4
    # 6 -> 7
    # 7 -> 6
    # Number 2 is missing (as input, produces 1 like 0, but 0 is smaller for producing lowest initial value?)
    # Reverse the function below; how can 2 be printed?

    # print low 3 bits of B ; must be 0b010
    # b = b ^ 0b11          ; 0b010 = B ^ 0b011 --> B must be 0b001
    # b = b ^ c             ; 0b001 = B ^ C
    #                       ; 0b001 = (B ^ 0b10) ^ (a / 2**(B ^ 0b10))
    #                       ; 0b001 = (B ^ 0b10) ^ (a >> (B ^ 0b10))
    # c = a / 2**b          ; c = a >> b bits  --> get some more bits of A
    #   B=0 -> b=010 (2) -> 2**b = 4    -> c = a >> 4
    #   B=1 -> b=011 (3) -> 2**b = 8    -> c = a >> 8
    #   B=2 -> b=000 (0) -> 2**b = 1    -> c = a >> 1
    #   B=3 -> b=001 (1) -> 2**b = 2    -> c = a >> 2
    #   B=4 -> b=110 (6) -> 2**b = 64   -> c = a >> 64
    #   B=5 -> b=111 (7) -> 2**b = 128  -> c = a >> 128
    #   B=6 -> b=100 (4) -> 2**b = 16   -> c = a >> 16
    #   B=7 -> b=101 (5) -> 2**b = 32   -> c = a >> 32
    # b = B ^ 0b10          ; b = 0bB~BB  --> flip middle bit
    #   B=0 -> b=010 (2)
    #   B=1 -> b=011 (3)
    #   B=2 -> b=000 (0)
    #   B=3 -> b=001 (1)
    #   B=4 -> b=110 (6)
    #   B=5 -> b=111 (7)
    #   B=6 -> b=100 (4)
    #   B=7 -> b=101 (5)
    # B = low 3 bits of A   ; B = ???

    # Required: 2,4,1,2,7,5,4,5,1,3,5,5,0,3,3,0
    # 16 digits
    # 17  (0b0000010001) -> 2,1
    # 529 (0b1000010001) -> 2,1,3,0
    # 1359348 -> 2,4,1,2,7,5,4
    # 9747956 -> 2,4,1,2,7,5,4,5
    #                      0b101001011110111110100  (21 bits = 7 output * 3 bits)
    #                   0b100101001011110111110100  (24 bits = 8 output * 3 bits)
    #                            (a_value must have 16 output * 3 bits = 48 bits)
    # missing ->                ,1,3,5,5,0,3,3,0
    # 0     | 3     | 3     | 0     | 5     | 5     | 3     | 1
    # 0b001 | 0b011 | 0b011 | 0b001 | 0b100 | 0b100 | 0b011 | 0b000
    # 0b001011011001100100011000 | 9747956
    #                   0b100101001011110111110100
    # 0b001011011001100100011000100101001011110111110100
    # = 50135565647348 --> produces:
    # 2,4,1,2,7,5,0,6,7,3,4,6,3,7,7,0
    # 2,4,1,2,7,5,4,5,1,3,5,5,0,3,3,0
    #             ^ ^ ^   ^ ^ ^ ^ ^
    #             `-+---- these two were correct before...
    # bin(9747956)
    # bin(1 << 47)
    # '0b100000000000000000000000000000000000000000000000'
    # '0b100101001011110111110100000000000000000000000000'
    # = 163543563370496 -> 1,1,1,1,1,1,1,1,2,4,1,2,7,5,4,5 (wrong/not useful)

    # 1 << 25 0b10000000000000000000000000
    #           00000000000000000000000000
    # (1 << 26) - 1 0b11111111111111111111111111
    # '0b100101001011110111110100000000000000000000000000'
    # '0b100101001011110111110111111111111111111111111111'
    # = 163543630479359 -> 1,1,1,1,1,1,1,3,1,4,1,2,7,5,4,5 (also wrong/not useful)

    # Test all 2**24 numbers ending in 0b100101001011110111110100
    #                         0b100101001011110111110100
    # 0b111111111111111111111111
    # 0b100000000000000000000000100101001011110111110100 from
    # 0b111111111111111111111111100101001011110111110100 to
    # 0b1000000000000000000000100101001011110111110100 from2 (i.e. first 2 bits are 0)

    a_tail = 0b100101001011110111110100
    iterations = 0
    for a_start in range((1 << 21), (1 << 24) - 1):
        a_value = (a_start << 24) | a_tail
        # assert int.bit_length(a_value) == 48
        # if int.bit_length(a_value) < 48:
        #     continue
        result = optimized(a_value, 0, 0)
        if iterations % 1_000_000 == 0:
            # print(a_value)
            print(f"{a_value} -> {result}")
        if required_output.startswith(result):
            print(f"{a_value} -> {result}")
        if result == required_output:
            print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa")
            print(f"{a_value} -> {result}")
            return a_value
        iterations += 1


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    # with open("test3.txt") as file:
    with open("input.txt") as file:
        lines = file.read().strip()

    data = parse(lines)
    # print(data)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(data[-1])}")
