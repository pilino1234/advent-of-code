from collections import defaultdict
from dataclasses import dataclass, field
from heapq import heapify, heappush, heappop
from math import inf
from typing import Callable

Point = tuple[int, int]
Grid = set[Point]

MovementFunction = Callable[[Point], Point]
up: MovementFunction = lambda p: (p[0] - 1, p[1])
down: MovementFunction = lambda p: (p[0] + 1, p[1])
left: MovementFunction = lambda p: (p[0], p[1] - 1)
right: MovementFunction = lambda p: (p[0], p[1] + 1)

TURN_DIRECTIONS: dict[MovementFunction : tuple[MovementFunction, MovementFunction]] = {
    up: (left, right),
    down: (left, right),
    left: (up, down),
    right: (up, down),
}


def parse(line_data: list[str]) -> tuple[Grid, Point, Point]:
    grid = set()
    start_pos = None
    end_pos = None
    for row_idx, row in enumerate(line_data):
        for col_idx, char in enumerate(row):
            if char == "S":
                start_pos = (row_idx, col_idx)
            elif char == "E":
                end_pos = (row_idx, col_idx)
            elif char == "#":
                continue
            grid.add((row_idx, col_idx))

    return grid, start_pos, end_pos


def print_grid(set_map: Grid, start_pos: Point, end_pos: Point) -> None:
    rows = {pos[0] for pos in set_map}
    cols = {pos[1] for pos in set_map}
    min_row, max_row = min(rows), max(rows)
    min_col, max_col = min(cols), max(cols)

    for row in range(min_row - 1, max_row + 2):
        for col in range(min_col - 1, max_col + 2):
            pos = (row, col)
            if pos == start_pos:
                char = "S"
            elif pos == end_pos:
                char = "E"
            elif pos in set_map:
                char = "."
            else:
                char = "#"
            print(char, end="")
        print()


@dataclass(order=True)
class State:
    score: int
    path: list[Point] = field(compare=False)
    direction: MovementFunction = field(compare=False)

    @property
    def pos(self):
        return self.path[-1]


def p1(
    set_grid: Grid, start_pos: Point, end_pos: Point, start_dir: MovementFunction = right
) -> tuple[int, set[Point]]:
    costs = defaultdict(lambda: inf)
    q: list[State] = [State(0, [start_pos], start_dir)]
    heapify(q)

    best_locations = set()
    best_score = inf

    while q:
        state = heappop(q)

        if state.score > costs[(state.pos, state.direction)]:
            continue
        costs[(state.pos, state.direction)] = state.score

        if state.pos == end_pos and state.score <= best_score:
            best_score = state.score
            best_locations.update(state.path)

        # Keep exploring
        for next_dir, step_cost in (
            (state.direction, 1),  # Same direction
            *((d, 1001) for d in TURN_DIRECTIONS[state.direction]),  # Turns
        ):
            if (next_pos := next_dir(state.pos)) in set_grid:
                heappush(q, State(state.score + step_cost, state.path + [next_pos], next_dir))

    return best_score, best_locations


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    print_grid(*data)
    # print(data[1], data[2])

    p1_result, p2_result = p1(*data)
    print(f"Part 1: {p1_result}")
    print(f"Part 2: {len(p2_result)}")
