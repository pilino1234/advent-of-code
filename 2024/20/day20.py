from collections import Counter
from typing import Callable

Point = tuple[int, int]
Grid = dict[Point, str]

MovementFunction = Callable[[Point], Point]
up: MovementFunction = lambda p: (p[0] - 1, p[1])
down: MovementFunction = lambda p: (p[0] + 1, p[1])
left: MovementFunction = lambda p: (p[0], p[1] - 1)
right: MovementFunction = lambda p: (p[0], p[1] + 1)


def parse(line_data: list[str]) -> tuple[Grid, Point, Point]:
    grid = {}
    start_pos = None
    end_pos = None
    for row_num, row in enumerate(line_data):
        for col_num, char in enumerate(row):
            if char == "S":
                start_pos = (row_num, col_num)
            elif char == "E":
                end_pos = (row_num, col_num)
            elif char == "#":
                continue
            grid[(row_num, col_num)] = char

    return grid, start_pos, end_pos


def find_path(grid: Grid, start: Point, end: Point) -> list[Point]:
    path = [start]
    while path[-1] != end:
        for nb in [up, down, left, right]:
            next_pos = nb(path[-1])
            if next_pos not in grid:
                continue
            elif len(path) > 1 and next_pos == path[-2]:
                continue
            path.append(next_pos)
            break
    return path


def manhattan_distance(p: Point, q: Point) -> int:
    return abs(p[0] - q[0]) + abs(p[1] - q[1])


def p1_and_p2(path: list[Point], max_cheat_length: int, threshold: int = 100) -> int:
    path_length = len(path)
    picoseconds_saved = Counter()
    for start_idx, cheat_start in enumerate(path):
        if start_idx > path_length - threshold:
            # Can't save enough at the end of the path, stop checking
            break

        # Check cheat destinations starting at (start point + threshold) until end of path
        end_idx = start_idx + threshold
        while end_idx < path_length:
            cheat_end = path[end_idx]
            cheat_len = manhattan_distance(cheat_start, cheat_end)

            if cheat_len > max_cheat_length:
                # If current cheat_dest is more than max_cheat_length away from cheat_start,
                # can skip checking the next points because it is impossible to get back into
                # range sooner than that.
                end_idx += cheat_len - max_cheat_length
                continue

            saved = (end_idx - start_idx) - cheat_len
            picoseconds_saved[saved] += 1
            end_idx += 1

    return sum(count for saved, count in picoseconds_saved.items() if saved >= threshold)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = find_path(*parse(lines))

    print(f"Part 1: {p1_and_p2(data, max_cheat_length=2)}")
    print(f"Part 2: {p1_and_p2(data, max_cheat_length=20)}")
