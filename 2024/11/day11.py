import functools
from dataclasses import dataclass
from typing import Self


@dataclass
class Node:
    number: int
    prev: Self
    next: Self


def parse(stones: list[int]) -> Node:
    node = Node(stones[0], None, None)
    head = node
    tail = node
    for stone in stones[1:]:
        node = Node(stone, tail, None)
        tail.next = node
        tail = node

    return head


def print_ll(stones: Node) -> None:
    curr = stones
    while curr:
        print(curr.number, end=" ")
        curr = curr.next
    print()


def len_ll(stones: Node) -> int:
    length = 0
    curr = stones
    while curr:
        length += 1
        curr = curr.next
    return length


def p1(stones_ll: Node, blinks: int = 25) -> int:
    head = stones_ll
    for blink in range(blinks):
        curr = head
        while curr:
            if curr.number == 0:
                # Replace 0 with 1
                curr.number = 1
            elif (len(str(curr.number)) % 2) == 0:
                # Split even length number into 2 stones
                number_str = str(curr.number)
                first, second = number_str[:len(number_str) // 2], number_str[len(number_str) // 2:]
                # print(f"Split {number_str} -> {first} and {second}")

                # Create new list nodes
                new1 = Node(int(first), curr.prev, None)
                new2 = Node(int(second), new1, curr.next)
                new1.next = new2

                # Splice new nodes into list, dropping curr
                if curr.prev is not None:
                    curr.prev.next = new1
                if curr.next is not None:
                    curr.next.prev = new2
                if curr is head:
                    head = new1
            else:
                curr.number *= 2024

            curr = curr.next

    return len_ll(head)


@functools.cache
def stone_splits(stone: int, blinks_left: int) -> int:
    if blinks_left == 0:
        return 1

    if stone == 0:
        return stone_splits(1, blinks_left - 1)

    stone_str = str(stone)
    if len(stone_str) % 2 == 0:
        first, second = stone_str[:len(stone_str) // 2], stone_str[len(stone_str) // 2:]
        return stone_splits(int(first), blinks_left - 1) + stone_splits(int(second), blinks_left - 1)

    return stone_splits(stone * 2024, blinks_left - 1)



def p2(stones: list[int], blinks: int) -> int:
    return sum(stone_splits(stone, blinks) for stone in stones)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        data = list(map(int, file.read().split()))

    data_ll = parse(data)
    # print(data_ll)
    # print_ll(data_ll)

    print(f"Part 1 (LL): {p1(data_ll, blinks=25)}")

    print(f"Part 1: {p2(data, blinks=25)}")
    print(f"Part 2: {p2(data, blinks=75)}")
