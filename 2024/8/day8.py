import itertools
from collections import defaultdict


Point = tuple[int, int]
Frequency = str
Grid = dict[Frequency, set[Point]]


def parse(line_data: list[str]) -> tuple[Grid, int, int]:
    antennas = defaultdict(set)
    for row_num, row in enumerate(line_data):
        for col_num, char in enumerate(row):
            if char == ".":
                continue
            antennas[char].add((row_num, col_num))

    num_rows = row_num
    num_cols = col_num

    return antennas, num_rows, num_cols


def cartesian_distance(a: Point, b: Point) -> Point:
    return (b[0] - a[0]), (b[1] - a[1])


def add_points(a: Point, b: Point) -> Point:
    return a[0] + b[0], a[1] + b[1]


def in_grid(point: Point, grid_height: int, grid_width: int) -> bool:
    return 0 <= point[0] <= grid_height and 0 <= point[1] <= grid_width


def p1(antennas: Grid, height: int, width: int) -> int:
    antinodes = set()
    for positions in antennas.values():
        for pair in itertools.permutations(positions, 2):
            distance = cartesian_distance(pair[0], pair[1])
            antinode = add_points(pair[1], distance)
            if in_grid(antinode, height, width):
                antinodes.add(antinode)
    return len(antinodes)


def p2(grid: Grid, height: int, width: int) -> int:
    antinodes = set()
    for positions in grid.values():
        antinodes.update(positions)
        for pair in itertools.permutations(positions, 2):
            distance = cartesian_distance(pair[0], pair[1])

            pos = pair[1]
            while True:
                antinode = add_points(pos, distance)
                if not in_grid(antinode, height, width):
                    break
                antinodes.add(antinode)
                pos = antinode

    return len(antinodes)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    antenna_map, *grid_dimensions = parse(lines)

    print(f"Part 1: {p1(antenna_map, *grid_dimensions)}")
    print(f"Part 2: {p2(antenna_map, *grid_dimensions)}")
