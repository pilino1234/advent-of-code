import re
from typing import NamedTuple

ClawMachine = NamedTuple("ClawMachine", [("ax", int), ("ay", int), ("bx", int), ("by", int), ("px", int), ("py", int)])


def parse(sections: list[str]) -> list[ClawMachine]:
    section_pattern = re.compile(
        r"Button A: X\+(?P<ax>\d+), Y\+(?P<ay>\d+)\n"
        r"Button B: X\+(?P<bx>\d+), Y\+(?P<by>\d+)\n"
        r"Prize: X=(?P<px>\d+), Y=(?P<py>\d+)"
    )

    parsed = []
    for section in sections:
        match = section_pattern.match(section)
        parsed.append(ClawMachine(**{k: int(v) for k, v in match.groupdict().items()}))
    return parsed


def p1(
        claw_machines: list[ClawMachine],
        a_tokens: int = 3,
        b_tokens: int = 1,
        max_presses_a: int = 100,
        max_presses_b: int = 100,
        prize_offset: int = 0,
) -> int:
    # A*94 + B*22 = 8400
    # A = 8400/94 - B*22/94

    # A*34 + B*67 = 5400

    # (8400/94 - B*22/94)*34 + B*67 = 5400
    # 8400*34/94 - B*22*34/94 + B*67 = 5400
    # -B*22*34/94 + B*67 = 5400 - 8400*34/94
    # B(-22*34/94 + 67) = 5400 - 8400*34/94
    # B = (5400 - 8400*34/94) / (-22*34/94 + 67)
    # A = 8400/94 - [B]*22/94

    # A * ax + B * bx = px
    # A * ay + B * by = py
    # A = (px - (B * bx)) / ax
    # A = px/ax - (B*bx)/ax
    # (px/ax - (B*bx)/ax) * ay + B * by = py
    # px*ay/ax - B*bx*ay/ax + B * by = py
    # - B*bx*ay/ax + B * by = py - px*ay/ax
    # B(-bx*ay/ax + by) = py - px*ay/ax
    # B = (py - px*ay/ax) / (-bx*ay/ax + by)

    total_tokens = 0
    for m in claw_machines:
        if prize_offset:
            m = ClawMachine(m.ax, m.ay, m.bx, m.by, m.px + prize_offset, m.py + prize_offset)

        # B = (PY - PX * AY / AX) / (-BX * AY / AX + BY)
        b_presses = round((m.py - m.px * m.ay / m.ax) / (-m.bx * m.ay / m.ax + m.by))
        # A = PX / AX - [B] * BX/AX
        a_presses = round(m.px / m.ax - b_presses * m.bx / m.ax)

        # Verify
        if not prize_offset:
            if not (0 <= a_presses <= max_presses_a):
                # Too many A presses
                continue
            if not (0 <= b_presses <= max_presses_b):
                # Too many B presses
                continue

        if (a_presses * m.ax + b_presses * m.bx) != m.px:
            # No real match
            continue
        if (a_presses * m.ay + b_presses * m.by) != m.py:
            # No real match
            continue

        total_tokens += (a_presses * a_tokens) + (b_presses * b_tokens)

    return total_tokens

if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = file.read().split("\n\n")

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p1(data, prize_offset=10_000_000_000_000)}")
