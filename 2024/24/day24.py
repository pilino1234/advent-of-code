from collections import defaultdict
from collections.abc import Callable
from pprint import pprint
from typing import NamedTuple

Wire = str
Value = bool
LogicGate = Callable[[Value, Value], Value]
and_: LogicGate = lambda a, b: a and b
or_: LogicGate = lambda a, b: a or b
xor_: LogicGate = lambda a, b: a ^ b


class Connection(NamedTuple):
    in1: Wire
    in2: Wire
    out: Wire
    func: LogicGate


def parse(file_data: str) -> tuple[dict[Wire, Value], list[Connection]]:
    start_values, connections = file_data.strip().split("\n\n")
    start_values = start_values.split("\n")
    connections = connections.split("\n")

    wire_values: dict[Wire, Value] = defaultdict(bool)
    for start_value in start_values:
        wire, value = start_value.split(": ")
        wire_values[wire] = bool(int(value))

    gates = []
    logic_gate_functions = {
        "AND": and_,
        "OR": or_,
        "XOR": xor_,
    }
    for connection in connections:
        in1, func, in2, _, out = connection.split(" ")
        gates.append(Connection(in1, in2, out, logic_gate_functions[func]))

    return wire_values, gates


def simulate(wire_values: dict[Wire, Value], gates: list[Connection]) -> dict[Wire, Value]:
    wire_values = wire_values.copy()
    changed = True
    while changed:
        changed = False
        for gate in gates:
            prev_value = wire_values[gate.out]
            new_value = gate.func(wire_values[gate.in1], wire_values[gate.in2])
            if prev_value != new_value:
                changed = True
                wire_values[gate.out] = new_value
    return wire_values


def get_value(wire_values: dict[Wire, Value], register: str = "z") -> int:
    result_bits = []
    for wire in sorted(wire_values, reverse=True):
        if wire.startswith(register):
            value = wire_values[wire]
            result_bits.append(int(value))
    return int("".join(map(str, result_bits)), base=2)


def p1(wire_values: dict[Wire, Value], connections: list[Connection]) -> int:
    output_values = simulate(wire_values, connections)
    return get_value(output_values)


def p2(wire_values: dict[Wire, Value], gates: list[Connection]) -> str:
    # Use various rules about how the circuit should be structured to identify
    # suspicious parts. Don't actually bother finding which pairs to swap to
    # fix the circuit, just need to find 'bad' wires to get the solution.

    suspicious_gates = set()

    xor_output_to_xor = set()
    and_output_to_or = set()
    # First pass
    for gate in gates:
        if gate.func == xor_:
            # XOR gates must either have x and y bit as input...
            if gate.in1[0] in "xy" and gate.in2[0] in "xy":
                if gate.in1 in ("x00", "y00") and gate.in2 in ("x00", "y00"):
                    # This one is special, don't need to check it
                    continue
                # Ok, but wire must be input to another xor gate, check later
                xor_output_to_xor.add(gate)
            # ...or have z as output
            elif gate.out.startswith("z"):
                # Ok
                ...
            else:
                # Not ok
                print(f"Suspicious (XOR input/output): {gate}")
                suspicious_gates.add(gate)
        elif gate.func == and_:
            # z-bits only come from XOR gates
            if gate.out.startswith("z"):
                # Not ok
                print(f"Suspicious (AND gate outputs to z): {gate}")
                suspicious_gates.add(gate)

            if gate.in1 in ("x00", "y00") and gate.in2 in ("x00", "y00"):
                # This one is special, don't need to check it
                continue
            # Ok, but wire must be input to an OR gate, check later
            and_output_to_or.add(gate)
        elif gate.func == or_:
            # z-bits only come from XOR gates (except z45 which comes from OR)
            if gate.out.startswith("z") and gate.out != "z45":
                # Not ok
                print(f"Suspicious (OR gate outputs to z): {gate}")
                suspicious_gates.add(gate)

    # Second pass, check XOR and AND outputs collected previously
    for gate in gates:
        if gate.func == xor_:
            to_remove = {g for g in xor_output_to_xor if g.out in (gate.in1, gate.in2)}
            xor_output_to_xor.difference_update(to_remove)
        if gate.func == or_:
            to_remove = {g for g in and_output_to_or if g.out in (gate.in1, gate.in2)}
            and_output_to_or.difference_update(to_remove)

    if xor_output_to_xor:
        # If any remaining, not ok
        for gate in xor_output_to_xor:
            print(f"Suspicious (XOR does not output to XOR): {gate}")
            suspicious_gates.add(gate)

    if and_output_to_or:
        # If any remaining, not ok
        for gate in and_output_to_or:
            print(f"Suspicious (AND does not output to OR): {gate}")
            suspicious_gates.add(gate)

    pprint(suspicious_gates)

    bad_wires = [gate.out for gate in suspicious_gates]
    return ",".join(sorted(bad_wires))


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        lines = file.read()

    data = parse(lines)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(*data)}")


# ======== Testing and analysis stuff, unused for actual solution but keeping around ========
def trace_wire(wire_name: Wire, connections: list[Connection]) -> set[Wire]:
    connected = set()
    check_next = [wire_name]
    while check_next:
        new_check_next = []
        for wire in check_next:
            for gate in connections:
                if gate.in1 == wire or gate.in2 == wire:
                    connected.add(gate.out)
                    new_check_next.append(gate.out)
        check_next = new_check_next
    return connected


def set_value(register: str, value: int) -> dict[Wire, Value]:
    assert int.bit_length(value) <= 45

    values = defaultdict(bool)
    for bit in range(int.bit_length(value)):
        wire_value = bool(value & (1 << bit))
        wire_name = f"{register}{bit:02}"
        values[wire_name] = wire_value
    return values


def add(x_value: int, y_value: int, gates: list[Connection]) -> int:
    wire_values = set_value("x", x_value)
    wire_values.update(set_value("y", y_value))
    output_values = simulate(wire_values, gates)
    return get_value(output_values)


def test_addition(x: int, y: int, gates: list[Connection]) -> bool:
    expected = x + y
    result = add(x, y, gates)
    if result != expected:
        print(f"Expected {expected} ({expected:b}) but got {result} ({result:b})")
        return False
    return True


def p2_analysis(wire_values: dict[Wire, Value], connections: list[Connection]) -> ...:
    x_bits = set()
    y_bits = set()
    z_bits = set()

    for gate in connections:
        for wire in (gate.in1, gate.in2, gate.out):
            if wire.startswith("x"):
                x_bits.add(wire)
            elif wire.startswith("y"):
                y_bits.add(wire)
            elif wire.startswith("z"):
                z_bits.add(wire)

    print(sorted(x_bits))
    print(sorted(y_bits))
    print(sorted(z_bits))

    # Static analysis
    # Trace x bits
    for x_bit in sorted(x_bits):
        connected = trace_wire(x_bit, connections)
        x_bit_num = int(x_bit[1:])
        for z_bit in sorted(z_bits):
            z_bit_num = int(z_bit[1:])
            if z_bit_num < x_bit_num:
                continue
            if z_bit not in connected:
                print(f"Missing {z_bit} in {x_bit}")
        # print(x_bit, connected)

        connected_zs = {w for w in connected if w.startswith("z")}
        print(sorted(connected_zs))

    # Missing z06 in x00
    # Missing z06 in x01
    # Missing z06 in x02
    # Missing z06 in x03
    # Missing z06 in x04
    # Missing z06 in x05

    # Trace y bits
    for y_bit in sorted(y_bits):
        connected = trace_wire(y_bit, connections)
        y_bit_num = int(y_bit[1:])
        for z_bit in sorted(z_bits):
            z_bit_num = int(z_bit[1:])
            if z_bit_num < y_bit_num:
                continue
            if z_bit not in connected:
                print(f"Missing {z_bit} in {y_bit}")
    print()
    # Missing z06 in y00
    # Missing z06 in y01
    # Missing z06 in y02
    # Missing z06 in y03
    # Missing z06 in y04
    # Missing z06 in y05

    # --> Something is wrong with z06

    # Test adding stuff
    # print(add(0, 0, connections))
    # print(add(0, 1, connections))
    # print(add(1, 0, connections))
    # print(add(1, 1, connections))

    for x_pos in range(len(x_bits)):
        x_value = 1 << x_pos
        test_addition(x_value, 0, connections)
    print()

    for y_pos in range(len(y_bits)):
        y_value = 1 << y_pos
        test_addition(0, y_value, connections)
    print()

    # 1<<44 = 17592186044416 = 0b100000000000000000000000000000000000000000000
    #                          0b101010101010101010101010101010101010101010101
    x_value = 0b101010101010101010101010101010101010101010101
    y_value = 0b101010101010101010101010101010101010101010101
    test_addition(x_value, y_value, connections)
    print()

    x_value = 0b10101010101010101010101010101010101010101010
    y_value = 0b10101010101010101010101010101010101010101010
    test_addition(x_value, y_value, connections)
    print()

    original_x = get_value(wire_values, register="x")
    original_y = get_value(wire_values, register="y")
    print(f"Original x value: {original_x}")
    print(f"Original y value: {original_y}")
    test_addition(original_x, original_y, connections)
    print()

    for bit_pos in range(len(x_bits)):
        x_value = 1 << bit_pos
        y_value = 1 << bit_pos
        if not test_addition(x_value, y_value, connections):
            print(bit_pos)
    print()

    test_addition((1<<len(x_bits)) - 1, 1, connections)
    print()


