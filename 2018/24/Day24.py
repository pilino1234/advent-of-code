import itertools
import re
from enum import Enum, auto
from typing import List, Dict, Optional, Tuple
from dataclasses import dataclass


class Side(Enum):
    IMMUNE_SYSTEM = auto()
    INFECTION = auto()


@dataclass
class Group:
    id: int
    side: Side
    count: int
    hp: int
    atk_damage: int
    atk_type: str
    initiative: int
    weaknesses: List[str]
    immunities: List[str]
    attacking: Optional["Group"] = None
    attacked_by: Optional["Group"] = None

    @property
    def effective_power(self) -> int:
        return self.count * self.atk_damage

    @property
    def dead(self) -> bool:
        return self.count == 0

#    def __repr__(self):
#        return f"Group({self.id}, {self.side}, {self.count}, {self.hp}, {self.atk_damage}, {self.atk_type}, " \
#               f"{self.initiative}, {self.weaknesses}, {self.immunities})"

    def __str__(self):
        SHORT = True
        if SHORT:
            return self.name
        return f"Group {self.id} ({self.side.name}) with {self.count} units and {self.hp} hp. " \
               f"{self.atk_damage} {self.atk_type} damage, weak to {self.weaknesses}, immune to {self.immunities}. " \
               f"Effective power: {self.effective_power}, Initiative: {self.initiative}"

    @property
    def name(self) -> str:
        return f"{self.side.name.replace('_', ' ').title()} group {self.id}"

    def reset(self):
        if self.attacking:
            self.attacking.attacked_by = None
            self.attacking = None
        self.attacked_by = None

    def calculate_damage(self, defender: "Group", print_ = False) -> int:
        # Apply immunity or weakness
        multiplier = 1
        if self.atk_type in defender.immunities:
            multiplier = 0
        elif self.atk_type in defender.weaknesses:
            multiplier = 2

        dmg = self.effective_power * multiplier
        if print_:
            print(f"{self} would deal {defender} {dmg} damage")
        return dmg

    def attack(self, print_ = False) -> bool:
        """Do an attack on our chosen opponent.

        Returns whether any damage was done.
        """
        if self.attacking is None:
            # print("ATTEMPTING TO ATTACK None UNIT!")
            return

        damage_dealt = self.calculate_damage(self.attacking, print_)

        # Can't kill more units than there are in the opponent group
        units_killed = min(damage_dealt // self.attacking.hp, self.attacking.count)
        if print_:
            print(f"killing {units_killed} units")

        # Kill those units
        self.attacking.count -= units_killed

        self.reset()

        return units_killed > 0


def parse_groups(input_lines: str, immune_system_boost: int = 0) -> Dict[Side, List[Group]]:
    parsed_groups = {s: list() for s in Side}

    regex = re.compile(
        r"(\d+) units each with (\d+) hit points (\([^)]*\) )?with an attack "
        r"that does (\d+) (\w+) damage at initiative (\d+)")

    immune_system, infection = input_lines.split('\n\n')

    for side, team in [(Side.IMMUNE_SYSTEM, immune_system), (Side.INFECTION, infection)]:
        for line in team.splitlines()[1:]:
            if not line:
                continue
            else:
                data = regex.match(line)
                count, hp, extra, damage, atk_type, initiative = data.groups()
                count = int(count)
                hp = int(hp)
                damage = int(damage) + (immune_system_boost if side == Side.IMMUNE_SYSTEM else 0)
                initiative = int(initiative)
                weak = []
                immune = []

                if extra:
                    extra = extra.strip().lstrip('(').rstrip(')')
                    parts = extra.split('; ')
                    for part in parts:  # type: str
                        if part.startswith('weak to'):
                            weak = part[len('weak to '):].split(', ')
                        elif part.startswith('immune to'):
                            immune = part[len('immune to '):].split(', ')

                parsed_groups[side].append(Group(
                    id = len(parsed_groups[side]) + 1,
                    side = side,
                    count = count,
                    hp = hp,
                    atk_damage = damage,
                    atk_type = atk_type,
                    initiative = initiative,
                    weaknesses = weak,
                    immunities = immune
                ))

    return parsed_groups


def select_targets(teams: Dict[Side, List[Group]], print_ = False):
    all_groups = list(itertools.chain.from_iterable(teams.values()))
    all_alive_groups = [g for g in all_groups if not g.dead]
    # Sort all groups by (effective power, initiative) (decr)
    all_groups_by_power = sorted(all_alive_groups, key = lambda g: (g.effective_power, g.initiative), reverse=True)

    # For each group
    for group in all_groups_by_power:
        if print_:
            print(f"{group}")

        # Select target
        other_side = Side.INFECTION if group.side == Side.IMMUNE_SYSTEM else Side.IMMUNE_SYSTEM
        opponents = [opp for opp in teams[other_side] if opp.attacked_by is None and not opp.dead]

        if not opponents:
            # print("No opponents to attack")
            continue

        damages_dealt = [group.calculate_damage(opp, print_) for opp in opponents]
        # print(damages_dealt)

        # Find the opponent where we can deal the most damage
        highest_damage = max(damages_dealt)
        if print_:
            print(f"Highest damage: {highest_damage}")

        # Check if there are several targets with the same potential damage
        if damages_dealt.count(highest_damage) > 1:

            targets = [idx for idx, dmg in enumerate(damages_dealt) if dmg == highest_damage]
            # print(targets)

            # In case of tie, choose opponent with highest effective power
            # In case of tie, choose opponent with highest initiative
            sorted_targets = sorted(targets,
                                    key = lambda i: (opponents[i].effective_power, opponents[i].initiative),
                                    reverse = True)
            target = opponents[sorted_targets[0]]
        elif max(damages_dealt) == 0:
            # If can not attack anything, do nothing.
            if print_:
                print("can not deal damage")
            continue
        else:
            target = opponents[damages_dealt.index(highest_damage)]

        if print_:
            print(f"will attack {target}")
        group.attacking = target
        target.attacked_by = group


def attacking_phase(teams: Dict[Side, List[Group]], print_ = False) -> bool:
    """Execute all attacks.

    Returns whether a stalemate occurred or not.
    """
    all_groups = list(itertools.chain.from_iterable(teams.values()))

    # Sort all groups by initiative (decr)
    all_groups_by_initiative = sorted(all_groups, key = lambda g: g.initiative, reverse = True)

    # Count how many attacks actually lead to damage to detect stalemates
    damage_counter = 0
    for group in all_groups_by_initiative:
        if group.count == 0:
            # print(f"{group} is dead, skipping")
            group.reset()
            continue

        if group.attacking is None:
            continue

        if print_:
            print(f"{group} will now attack {group.attacking}")

        did_damage = group.attack(print_)

        if did_damage:
            damage_counter += 1

    return damage_counter == 0


def fight(teams: Dict[Side, List[Group]], print_ = False) -> Optional[Tuple[Optional[Side], List[Group]]]:
    round_number = 1
    stalemate = False
    while not any(all(g.dead for g in teams[team]) for team in teams):
        if print_:
            print(f"\n\n=== ROUND {round_number} ==============================\n\n")
        assert all(g.attacking is g.attacked_by is None for team in teams for g in teams[team])

        select_targets(teams, print_)
        stalemate = attacking_phase(teams, print_)

        if stalemate:
            if print_:
                print("Stalemate detected, cancelling fight.")
            break

        round_number += 1

    if print_:
        print("\n\n=================================\n\n")

    winner = None
    # print("Battle over")
    for side in teams:
        remaining_groups = [g for g in teams[side] if not g.dead]
        if print_:
            print(f"{side.name.replace('_', ' ').title()}: {len(remaining_groups)} groups remaining")

        if remaining_groups:
            winner = side
            for group in remaining_groups:
                if print_:
                    print(f"{group} has {group.count} units remaining")

            if print_:
                print(f"Total units remaining: {sum(group.count for group in remaining_groups)}")
    if stalemate and print_:
        print("Fight ended due to stalemate.")

    assert (winner is not None) or stalemate
    if stalemate:
        return None, []
    return winner, teams[winner]


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = file.read()

    # Part 1
    groups = parse_groups(lines)
    winning_side, units = fight(groups, print_ = False)
    print(f"Remaining units: {sum(g.count for g in units if not g.dead)}")

    # Part 2
    boost = 1
    search_range = 100
    results = {}

    while True:
        if boost not in results:
            # print(f"Trying boost {boost}")
            groups = parse_groups(lines, boost)
            winner, _ = fight(groups)
            results[boost] = winner
        if boost - 1 not in results:
            # print(f"Trying boost {boost-1}")
            groups = parse_groups(lines, boost-1)
            winner, _ = fight(groups)
            results[boost-1] = winner

        if results[boost] == Side.IMMUNE_SYSTEM:
            if results[boost-1] != Side.IMMUNE_SYSTEM:
                # Solution found, break
                break

            # Try to find lower successful boost
            search_range //= 2
            search_range += 1
            boost = max(1, boost - search_range)
        else:
            boost += search_range

    groups = parse_groups(lines, immune_system_boost = boost)
    winning_side, units = fight(groups, print_ = False)
    print(f"Remaining units: {sum(g.count for g in units if not g.dead)}")
