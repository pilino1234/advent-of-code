from collections import namedtuple
from typing import List, Optional

Point = namedtuple('Point', ['x', 'y', 'z', 'w'])


class DisjointSet:
    # "Merge-find set"
    # See: https://en.wikipedia.org/wiki/Disjoint-set_data_structure

    def __init__(self, items: int):
        self.n: int = items
        self.parents: List[Optional[int]] = [None] * items
        self.ranks: List[int] = [1] * items
        self.num_sets: int = items

    def find(self, i: int) -> int:
        """Find the top-level parent of an element"""
        # Next-level parent
        p = self.parents[i]
        if p is None:
            # If this item does not have a parent, it is a top-level parent
            return i
        # Otherwise, recurse upwards through the tree
        p = self.find(p)
        # Set this item's parent to the highest parent
        self.parents[i] = p
        # Return the parent
        return p

    def in_same_set(self, i: int, j: int) -> bool:
        """Check whether two items are in the same set"""
        # If both items have the same parent, they are in the same set.
        return self.find(i) == self.find(j)

    def merge(self, i: int, j: int) -> None:
        # Find the parents of both sets
        i = self.find(i)
        j = self.find(j)

        # If they already are in the same set, return
        if i == j:
            return

        i_rank = self.ranks[i]
        j_rank = self.ranks[j]

        if i_rank < j_rank:
            # If j has higher rank, merge i into j
            self.parents[i] = j
        elif i_rank > j_rank:
            # If i has higher rank, merge j into i
            self.parents[j] = i
        else:
            # If they have the same rank, merge j into i
            self.parents[j] = i
            # Promote i's rank
            self.ranks[i] += 1
        # Two sets were merged, so the number of sets has decreased
        self.num_sets -= 1


def parse_points(lines: List[str]) -> List[Point]:
    return [Point(*map(int, line.split(','))) for line in lines]


def manhattan(p1: Point, p2: Point) -> int:
    return abs(p1.x-p2.x) + abs(p1.y-p2.y) + abs(p1.z-p2.z) + abs(p1.w-p2.w)


def cluster(points: List[Point]):
    # Create a new merge-find set
    merge_find_set = DisjointSet(len(points))

    to_index = {}
    for idx, point in enumerate(points):
        to_index[point] = idx

        for point2 in to_index:
            if manhattan(point, point2) <= 3:
                merge_find_set.merge(idx, to_index[point2])

    return merge_find_set.num_sets


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file.readlines()]

    points = parse_points(lines)

    print(cluster(points))
