import itertools

import networkx

from Point2D import Point2D as Pt


def generate_cave_terrain(depth: int, target: Pt):
    erosion_levels = {}
    cave_map = {}

    def geologic_index(region: Pt):
        if region == Pt(0, 0) or region == target:
            return 0
        elif region.y == 0:
            return region.x * 16807
        elif region.x == 0:
            return region.y * 48271
        else:
            return erosion_levels[pos.left()] * erosion_levels[pos.above()]

    overscan = 100

    for y in range(target.y + overscan):
        for x in range(target.x + overscan):
            pos = Pt(x, y)
            geologic = geologic_index(pos)
            erosion = (geologic + depth) % 20183
            erosion_levels[pos] = erosion
            cave_map[pos] = erosion % 3

    return cave_map


def calculate_risk_level(cave_map: dict, target: Pt):
    return sum(cave_map[Pt(x, y)] for x in range(target.x+1) for y in range(target.y+1))


def fastest_path(cave_map: dict, target: Pt):
    max_x = max(cave_map, key=lambda p: p.x).x
    max_y = max(cave_map, key=lambda p: p.y).y

    graph = networkx.DiGraph()

    rocky, wet, narrow = 0, 1, 2
    torch, climbing_gear, neither = 0, 1, 2

    available_tools = {
        rocky: (torch, climbing_gear),
        wet: (climbing_gear, neither),
        narrow: (torch, neither)
    }

    for point in cave_map:  # type: Pt
        # Add edges for switching tools from current position
        current_tools = available_tools[cave_map[point]]
        for pair in itertools.permutations(current_tools):
            graph.add_edge((point, pair[0]), (point, pair[1]), weight=7)

        # Add edges for travelling between regions
        for nb in point.nb4():  # type: Pt
            if nb.x < 0 or nb.y < 0 or nb.x > max_x or nb.y > max_y:
                continue
            next_tools = available_tools[cave_map[nb]]

            # Get tools that can be used to travel directly between these regions
            tools = set(current_tools).intersection(set(next_tools))

            for tool in tools:
                graph.add_edge((point, tool), (nb, tool), weight=1)

    shortest_path_length = networkx.dijkstra_path_length(graph, (Pt(0, 0), torch), (target, torch))
    return shortest_path_length


def print_cave(cave_map: dict):
    max_x = max(cave_map, key=lambda p: p.x).x
    max_y = max(cave_map, key=lambda p: p.y).y

    terrain_mapping = {
        0: ".",  # rocky
        1: "=",  # wet
        2: "|"   # narrow
    }

    for y in range(max_y+1):
        for x in range(max_x+1):
            print(terrain_mapping[cave_map[Pt(x, y)]], end="")
        print()
    print()


if __name__ == '__main__':
    with open("input.txt") as file:
        cave_depth = int(file.readline().split()[1])
        target_coords = Pt(*map(int, file.readline().split()[1].split(",")))

    cave = generate_cave_terrain(cave_depth, target_coords)
    # print_cave(cave)

    print("Risk level:", calculate_risk_level(cave, target_coords))
    print("Shortest path length:", fastest_path(cave, target_coords))
