import re
from collections import namedtuple
from typing import List

# z3 code borrowed from: https://github.com/msullivan/advent-of-code/blob/master/2018/23b.py

from z3 import Int, If, Optimize

Nanobot = namedtuple('Nanobot', ['x', 'y', 'z', 'range'])


def parse_nanobots(input_lines: list):
    return [Nanobot(*numbers) for numbers in map(lambda s: map(int, re.findall(r'-?\d+', s)), input_lines)]


def find_max_range(nanobot_list: list) -> Nanobot:
    return max(nanobot_list, key=lambda p: p.range)


def manhattan_dist(p1: Nanobot, p2: Nanobot) -> int:
    return abs(p1.x-p2.x) + abs(p1.y-p2.y) + abs(p1.z-p2.z)


def z3_abs(x):
    return If(x >= 0, x, -x)


def z3_manhattan(x, y):
    return z3_abs(x[0] - y[0]) + z3_abs(x[1] - y[1]) + z3_abs(x[2] - y[2])


def nanobots_in_range(nanobot_list: list, start: Nanobot) -> int:
    count = 0

    for p in nanobot_list:
        if manhattan_dist(start, p) <= start.range:
            count += 1

    return count


def most_nanobots_in_range(nanobot_list: List[Nanobot]) -> int:
    x, y, z = Int('x'), Int('y'), Int('z')

    orig = (x, y, z)
    cost = Int('cost')
    cost_expr = x * 0

    for bot in nanobot_list:
        cost_expr += If(z3_manhattan(orig, (bot.x, bot.y, bot.z)) <= bot.range, 1, 0)

    opt = Optimize()
    print("Starting optimize")
    opt.add(cost == cost_expr)
    opt.maximize(cost)
    opt.minimize(z3_manhattan((0, 0, 0), (x, y, z)))

    opt.check()

    model = opt.model()

    pos = (model[x].as_long(), model[y].as_long(), model[z].as_long())
    print("Coordinates:", pos)
    print("Num in range:", model[cost].as_long())
    distance = manhattan_dist(Nanobot(0, 0, 0, 0), Nanobot(*pos, 0))
    return distance


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file.readlines()]

    nanobots = parse_nanobots(lines)

    max_range = find_max_range(nanobots)
    print("Nanobots in range:", nanobots_in_range(nanobots, max_range))

    max_distance = most_nanobots_in_range(nanobots)
    print(max_distance)
