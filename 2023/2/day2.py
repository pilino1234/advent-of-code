import math
from collections import Counter
from functools import reduce
from operator import or_


def parse(line_data: list[str]) -> list[list[Counter]]:
    game_records = []
    for line in line_data:
        revealed_sets = line.split(":")[1].split(";")

        games = []
        for revealed in revealed_sets:
            cube_amount = Counter()
            for cube in revealed.split(","):
                amount, colour = cube.strip().split(" ")
                cube_amount[colour] = int(amount)
            games.append(cube_amount)
        game_records.append(games)
    return game_records


def p1(main_game: Counter, played_games: list[list[Counter]]) -> int:
    sum_of_valid_games = 0
    for game_idx, game_records in enumerate(played_games, start=1):
        if all(record <= main_game for record in game_records):
            sum_of_valid_games += game_idx

    return sum_of_valid_games


def p2(played_games: list[list[Counter]]) -> int:
    return sum(math.prod(reduce(or_, game_records).values()) for game_records in played_games)


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    game = Counter(red=12, green=13, blue=14)

    print(f"Part 1: {p1(game, data)}")
    print(f"Part 2: {p2(data)}")
