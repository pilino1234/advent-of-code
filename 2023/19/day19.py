import operator
import re
from typing import NamedTuple

ACCEPT_WORKFLOW = "A"
REJECT_WORKFLOW = "R"

MAX_RATING = 4000


class Part(NamedTuple):
    x: int
    m: int
    a: int
    s: int


class Rule(NamedTuple):
    field: str
    op: str
    value: int
    destination: str

    def matches(self, part: Part) -> bool:
        if self.op == ">":
            comp = operator.gt
        elif self.op == "<":
            comp = operator.lt
        else:
            raise ValueError(f"Unknown operator: {self.op}")

        return comp(getattr(part, self.field), self.value)


class Workflow(NamedTuple):
    name: str
    rules: list[Rule]


def parse_workflows(line_data: list[str]) -> dict[str, Workflow]:
    parsed_workflows = {}
    for line in line_data:
        name = line[: line.index("{")]
        w = Workflow(name=name, rules=[])
        rules = line[line.index("{") + 1 : -1].split(",")
        for rule in rules:
            if ":" in rule:
                # 'Regular' rule
                condition, dest = rule.split(":")
                rule = Rule(
                    field=condition[0], op=condition[1], value=int(condition[2:]), destination=dest
                )
            else:
                # Final rule has no condition, use something that always becomes true.
                # The rule only consists of the destination.
                # A bit of a kludge, could use some other kind of sentinel object to signal this case for p2 solution.
                rule = Rule("x", ">", 0, destination=rule)
            w.rules.append(rule)
        parsed_workflows[w.name] = w
    return parsed_workflows


def parse_parts(line_data: list[str]) -> list[Part]:
    return [Part(*(int(n) for n in re.findall(r"\d+", line))) for line in line_data]


def p1(parts: list[Part], workflows: dict[str, Workflow]) -> int:
    accepted = []
    for part in parts:
        wf = workflows["in"]
        done = False
        while not done:
            for rule in wf.rules:
                if not rule.matches(part):
                    continue

                # If the rule matched
                if rule.destination == ACCEPT_WORKFLOW:
                    accepted.append(part)
                    done = True
                    break
                elif rule.destination == REJECT_WORKFLOW:
                    done = True
                    break
                # Otherwise, proceed to the next workflow (destination)
                wf = workflows[rule.destination]
                break  # Need to break so that for-loop iterates over new wf.rules instead
    return sum(map(sum, accepted))


class Region(NamedTuple):
    x: range
    m: range
    a: range
    s: range
    # Keep track of which workflow & rule to use for splitting this region further
    wf: str
    rule_idx: int

    @property
    def volume(self):
        return len(self.x) * len(self.m) * len(self.a) * len(self.s)


def p2(workflows: dict[str, Workflow]) -> int:
    """Build a 4D decision tree."""
    stack = [
        Region(
            range(1, MAX_RATING + 1),
            range(1, MAX_RATING + 1),
            range(1, MAX_RATING + 1),
            range(1, MAX_RATING + 1),
            "in",
            0,
        )
    ]

    accept_regions = []

    while stack:
        region = stack.pop()

        if region.wf == ACCEPT_WORKFLOW:
            # Accepted region, no more processing
            accept_regions.append(region)
            continue
        elif region.wf == REJECT_WORKFLOW:
            # Rejected region, just discard?
            # rejected_regions.append(r)
            continue

        rule = workflows[region.wf].rules[region.rule_idx]
        # Detect final rule of a chain from the special condition added during parsing
        if rule.field == "x" and rule.op == ">" and rule.value == 0:
            # Final rule in chain, don't branch, just go to next workflow
            stack.append(region._replace(**{"wf": rule.destination, "rule_idx": 0}))
            continue

        split_dimension = rule.field
        split_value = rule.value
        curr_range = getattr(region, split_dimension)

        if rule.op == ">":
            # Left region does not match, continue following this workflow
            left_dest_wf = region.wf
            left_dest_rule_idx = region.rule_idx + 1
            # Right region matches rule, follow destination
            right_dest_wf = rule.destination
            right_dest_rule_idx = 0
            # Shift split value upwards by 1 to satisfy > condition
            split_value += 1
        elif rule.op == "<":
            # Right region does not match, continue following this workflow
            right_dest_wf = region.wf
            right_dest_rule_idx = region.rule_idx + 1
            # Left region matches rule, follow destination
            left_dest_wf = rule.destination
            left_dest_rule_idx = 0
        else:
            raise ValueError

        left_range = range(curr_range.start, split_value)
        right_range = range(split_value, curr_range.stop)

        left = region._replace(
            **{split_dimension: left_range, "wf": left_dest_wf, "rule_idx": left_dest_rule_idx}
        )
        right = region._replace(
            **{split_dimension: right_range, "wf": right_dest_wf, "rule_idx": right_dest_rule_idx}
        )

        stack.extend([left, right])

    # Sanity check
    # assert sum(reg.volume for reg in accept_regions) + sum(reg.volume for reg in rejected_regions) == MAX_RATING ** 4
    return sum(reg.volume for reg in accept_regions)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        wfs, ps = file.read().split("\n\n")

    wfs = parse_workflows(wfs.splitlines())
    ps = parse_parts(ps.splitlines())

    assert Region(range(1, 11), range(1, 2), range(1, 2), range(1, 2), "", 0).volume == 10
    assert Region(range(1, 11), range(1, 11), range(1, 2), range(1, 2), "", 0).volume == 100
    assert Region(range(1, 11), range(1, 11), range(1, 11), range(1, 2), "", 0).volume == 1000
    assert Region(range(1, 11), range(1, 11), range(1, 11), range(1, 11), "", 0).volume == 10000
    start = Region(
        range(1, MAX_RATING + 1),
        range(1, MAX_RATING + 1),
        range(1, MAX_RATING + 1),
        range(1, MAX_RATING + 1),
        "in",
        0,
    )
    assert start.volume == 4000**4

    print(f"Part 1: {p1(ps, wfs)}")
    print(f"Part 2: {p2(wfs)}")
