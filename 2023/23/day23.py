from collections import defaultdict
from heapq import heapify, heappop, heappush
from typing import NamedTuple

Point = tuple[int, int]


def parse(line_data: list[str]) -> tuple[dict[Point, str], Point, Point]:
    grid = {}
    for row_idx, row in enumerate(line_data):
        for col_idx, char in enumerate(row):
            grid[(row_idx, col_idx)] = char

    start = (0, line_data[0].index("."))
    goal = (len(line_data) - 1, line_data[-1].index("."))

    return grid, start, goal


def draw_map(grid: dict[Point, str]):
    rows = [p[0] for p in grid]
    cols = [p[1] for p in grid]
    min_row = min(rows)
    max_row = max(rows)
    min_col = min(cols)
    max_col = max(cols)

    for row in range(min_row, max_row + 1):
        for col in range(min_col, max_col + 1):
            print(grid[(row, col)], end="")
        print()
    print()


def above(p: Point) -> Point:
    return p[0] - 1, p[1]


def below(p: Point) -> Point:
    return p[0] + 1, p[1]


def left(p: Point) -> Point:
    return p[0], p[1] - 1


def right(p: Point) -> Point:
    return p[0], p[1] + 1


def nb4(p: Point) -> tuple[Point, Point, Point, Point]:
    return above(p), below(p), left(p), right(p)


ALLOWED_SLOPES = {
    right: ">",
    left: "<",
    above: "^",
    below: "v",
}


def p1(grid: dict[Point, str], start: Point, finish: Point) -> int:
    class State(NamedTuple):
        steps: int  # Negated!
        # path: list[Point]
        path: set[Point]
        current_pos: Point
    q: list[State] = [State(0, {start}, start)]
    heapify(q)

    possible_paths = []
    while q:
        neg_distance, path, pos = heappop(q)

        if pos == finish:
            # possible_paths.append(path)
            possible_paths.append(len(path) - 1)
            continue

        for next_dir in (above, below, left, right):
            next_pos = next_dir(pos)
            if next_pos not in grid:
                continue
            elif next_pos in path:
                # Cannot visit same tile twice
                continue
            elif grid[next_pos] == "#":
                # Walls
                continue
            elif grid[next_pos] == ".":
                heappush(q, State(neg_distance - 1, path | {next_pos}, next_pos))
            elif grid[next_pos] == ALLOWED_SLOPES[next_dir]:
                heappush(q, State(neg_distance - 1, path | {next_pos}, next_pos))

    return max(possible_paths)


def p2(grid: dict[Point, str], start: Point, finish: Point) -> int:
    new_grid = {pos: "." if char in "<>v^" else char for pos, char in grid.items()}

    # Find all intersections/forks
    crossings: set[Point] = {start, finish}
    for pos, char in new_grid.items():
        if char == "#":
            continue
        path_neighbours = [c for c in map(new_grid.get, nb4(pos)) if c != "#"]
        if len(path_neighbours) > 2:
            # Tile is an intersection
            crossings.add(pos)

    # Find distances between directly connected crossings
    new_graph: dict[Point, list[tuple[Point, int]]] = defaultdict(list)
    for intersection_pos in crossings:
        # All positions the same distance from the starting intersection position
        to_explore = [intersection_pos]
        seen = {intersection_pos}
        next_distance = 0
        while to_explore:
            next_distance += 1
            next_positions = []
            for pos in to_explore:
                for nb in nb4(pos):
                    if nb in seen:
                        continue
                    elif nb not in new_grid:
                        continue
                    elif new_grid[nb] == "#":
                        continue

                    seen.add(nb)

                    if nb in crossings:
                        # Reached another crossing, store neighbour and distance
                        new_graph[intersection_pos].append((nb, next_distance))
                    else:
                        # Keep exploring this path
                        next_positions.append(nb)
            to_explore = next_positions

    # Search all paths in the new graph to find the longest one
    class State(NamedTuple):
        pos: Point
        path: list[Point]
        total_distance: int
    q: list[State] = [State(start, [start], 0)]
    longest_path_len = 0
    while q:
        state = q.pop()

        # Reached goal
        if state.pos == finish:
            longest_path_len = max(longest_path_len, state.total_distance)
            continue

        # Explore adjacent crossings
        for next_node, next_distance in new_graph[state.pos]:
            if next_node in state.path:
                # Do not revisit paths
                continue
            q.append(State(next_node, state.path + [next_node], state.total_distance + next_distance))

    return longest_path_len


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(*data)}")
