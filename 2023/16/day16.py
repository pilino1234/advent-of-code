from typing import Callable

Point = tuple[int, int]


def parse(line_data: list[str]) -> tuple[dict[Point, str], int, int]:
    grid = {}
    for row_idx, row in enumerate(line_data):
        for col_idx, char in enumerate(row):
            grid[(row_idx, col_idx)] = char

    grid_height = len(line_data)
    grid_width = len(line_data[0])
    return grid, grid_height, grid_width


def above(p: Point) -> Point:
    return p[0] - 1, p[1]


def below(p: Point) -> Point:
    return p[0] + 1, p[1]


def left(p: Point) -> Point:
    return p[0], p[1] - 1


def right(p: Point) -> Point:
    return p[0], p[1] + 1


def energize(
    grid: dict[Point, str],
    start_pos: Point = (0, 0),
    start_dir: Callable[[Point], Point] = right,
) -> int:
    energized = set()

    seen = set()
    beams = [(start_pos, start_dir)]

    mirrors = {
        "\\": {above: left, below: right, left: above, right: below},
        "/": {above: right, below: left, left: below, right: above},
    }

    while beams:
        beam_pos, direction = beams.pop(0)
        while True:
            if beam_pos not in grid or (beam_pos, direction) in seen:
                break

            energized.add(beam_pos)
            seen.add((beam_pos, direction))

            symbol = grid[beam_pos]
            if direction in (left, right) and symbol == "|":
                beams.append((above(beam_pos), above))
                beams.append((below(beam_pos), below))
                break
            elif direction in (above, below) and symbol == "-":
                beams.append((left(beam_pos), left))
                beams.append((right(beam_pos), right))
                break
            elif symbol == "\\" or symbol == "/":
                direction = mirrors[symbol][direction]

            # Move to next position
            beam_pos = direction(beam_pos)

    return len(energized)


def p2(grid: dict[Point, str], grid_height: int, grid_width: int) -> int:
    starts = []
    for start_row in range(grid_height):
        starts.append(((start_row, 0), right))
        starts.append(((start_row, grid_width - 1), left))
    for start_col in range(grid_width):
        starts.append(((0, start_col), below))
        starts.append(((grid_height - 1, start_col), above))

    return max(energize(grid, pos, d) for pos, d in starts)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    g, width, height = parse(lines)

    print(f"Part 1: {energize(g)}")
    print(f"Part 2: {p2(g, width, height)}")
