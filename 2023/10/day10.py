Maze = list[str]
Point = tuple[int, int]


def find_start(line_data: Maze) -> Point:
    for row_idx, row in enumerate(line_data):
        for col_idx, col in enumerate(row):
            if col == "S":
                return row_idx, col_idx


def above(p: Point) -> Point:
    return p[0] - 1, p[1]


def below(p: Point) -> Point:
    return p[0] + 1, p[1]


def left(p: Point) -> Point:
    return p[0], p[1] - 1


def right(p: Point) -> Point:
    return p[0], p[1] + 1


def find_first_adj_path(pos: Point, maze: Maze) -> Point:
    for direction in (above, below, left, right):
        neighbour = direction(pos)

        try:
            character = maze[neighbour[0]][neighbour[1]]
        except IndexError:  # Out of bounds of maze
            continue

        if direction is above and character in "|7F":
            return neighbour
        elif direction is below and character in "|LJ":
            return neighbour
        elif direction is left and character in "-LF":
            return neighbour
        elif direction is right and character in "-J7":
            return neighbour
        else:
            continue
    else:
        m = "\n".join(maze)
        raise ValueError(f"Failed to find connected path component @{pos}:\n{m}")



def find_path(maze: Maze) -> list[Point]:
    start = find_start(maze)
    path = [start]

    current_pos = find_first_adj_path(path[-1], maze)
    prev = start
    path.append(current_pos)

    directions = {
        "|": (above, below),
        "-": (left, right),
        "L": (above, right),
        "J": (above, left),
        "7": (below, left),
        "F": (below, right),
    }

    # Follow the path until the beginning
    while current_pos != start:
        char = maze[current_pos[0]][current_pos[1]]

        for direction in directions[char]:
            next_pos = direction(current_pos)
            if next_pos != prev:
                # Don't go backwards
                # Could also do `if next_pos in path:`
                break
        else:
            m = "\n".join(maze)
            raise ValueError(f"Failed to find connected path component @{current_pos} (prev: {prev}):\n{m}")

        prev = current_pos
        current_pos = next_pos
        path.append(current_pos)

    return path


def p1(maze: Maze) -> int:
    path = find_path(maze)
    return len(path) // 2


def p2(maze: Maze) -> int:
    inside_count = 0
    path = set(find_path(maze))

    for row_idx, row in enumerate(maze):
        inside = False
        for col_idx, char in enumerate(row):
            if (row_idx, col_idx) in path:
                if char in "|LJ":
                    inside = not inside
            else:
                inside_count += inside

    return inside_count


if __name__ == '__main__':
    # with open("test7.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    print(f"Part 1: {p1(lines)}")
    print(f"Part 2: {p2(lines)}")
