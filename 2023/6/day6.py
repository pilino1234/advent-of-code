import math
import re
from typing import NamedTuple


class Race(NamedTuple):
    duration: int
    record_distance: int


def parse(line_data: list[str]) -> tuple[list[Race], Race]:
    times = map(int, re.findall(r"\d+", line_data[0]))
    distances = map(int, re.findall(r"\d+", line_data[1]))

    time_p2 = int(line_data[0].replace(" ", "")[5:])
    distance_p2 = int(line_data[1].replace(" ", "")[9:])

    return [Race(t, d) for t, d in zip(times, distances)], Race(time_p2, distance_p2)


def p1(races: list[Race]) -> int:
    """
    Initial brute force solution.

    - Could search from bottom and top instead of scanning the whole range, but may be worse if only a small set of
        winning speeds compared to race duration.
    - Symmetrical -- actually just need to find first (or last) winning speed
    - Could binary search for first winning speed.
    """
    ways_to_win = []

    for (race_duration, record_distance) in races:
        distances = []
        for hold_duration in range(race_duration):
            moving_time = race_duration - hold_duration
            moving_speed = hold_duration
            moved_distance = moving_time * moving_speed
            distances.append(moved_distance)
        ways_to_win.append(sum(d > record_distance for d in distances))

    return math.prod(ways_to_win)


def ways_to_win(race: Race) -> int:
    """Analytical solution"""
    # TODO
    ...


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    race_data, p2_race = parse(lines)

    print(f"Part 1: {p1(race_data)}")
    print(f"Part 2: {p1([p2_race])}")
