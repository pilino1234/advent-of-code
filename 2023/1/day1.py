import re


def p1_oneline(input_lines: list[str]) -> int:
    return sum(int(re.findall(r'\d', line)[0] + re.findall(r'\d', line)[-1]) for line in input_lines)


def parse_calibration_value(line: str, include_spelled_out: bool = False) -> int:
    digits = []
    for idx, character in enumerate(line):
        if character.isdigit():
            digits.append(character)
            continue
        elif include_spelled_out:
            for digit, word in enumerate(("one", "two", "three", "four", "five", "six", "seven", "eight", "nine"), start=1):
                if line[idx:].startswith(word):
                    digits.append(str(digit))
                    break
    return int(digits[0] + digits[-1])


if __name__ == '__main__':
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        calibration_document = [line.strip() for line in file]

    print(f"Part 1: {sum(parse_calibration_value(line) for line in calibration_document)}")
    print(f"Part 1 (one line): {p1_oneline(calibration_document)}")
    print(f"Part 2: {sum(parse_calibration_value(line, include_spelled_out=True) for line in calibration_document)}")
