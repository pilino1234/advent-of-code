import collections


Scratchcard = tuple[int, set[int], set[int]]


def parse(line_data: list[str]) -> list[Scratchcard]:
    parsed_cards = []

    for card_number, line in enumerate(line_data, start=1):
        winning, have = line.split(": ")[1].strip().split("|")
        parsed_cards.append(
            (
                card_number,
                set(map(int, winning.split())),
                set(map(int, have.split()))
            )
        )
    return parsed_cards


def score_card(scratchcard: Scratchcard) -> int:
    return int(2**(len(scratchcard[1] & scratchcard[2]) - 1))


def p1(scratchcards: list[Scratchcard]) -> int:
    return sum(score_card(card) for card in scratchcards)


def p2(scratchcards: list[Scratchcard]) -> int:
    matches_per_card = {card[0]: len(card[1] & card[2]) for card in scratchcards}
    number_of_cards = collections.Counter(matches_per_card.keys())

    for card_number, matches in matches_per_card.items():
        for copied_card in range(card_number + 1, card_number + matches + 1):
            number_of_cards[copied_card] += number_of_cards[card_number]
        # for offset in range(matches):
        #     number_of_cards[card_number + offset + 1] += number_of_cards[card_number]

    return sum(number_of_cards.values())


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
