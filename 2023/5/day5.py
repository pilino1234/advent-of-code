import re


def parse(section_lines: list[str]) -> tuple[list[int], list[range], list[list[tuple[range, int]]]]:
    parsed_seeds = list(map(int, re.findall(r"\d+", sections.pop(0))))

    parsed_seed_ranges = []
    for range_start, range_len in zip(parsed_seeds[::2], parsed_seeds[1::2]):
        parsed_seed_ranges.append(range(range_start, range_start + range_len))

    parsed_maps = []
    for lines in section_lines:
        lines = lines.splitlines()
        _title = lines.pop(0)

        ranges = []
        for line in lines:
            dest_start, src_start, range_len = map(int, line.split())
            src_range = range(src_start, src_start + range_len)
            offset: int = src_start - dest_start
            ranges.append((src_range, offset))
        parsed_maps.append(ranges)

    return parsed_seeds, parsed_seed_ranges, parsed_maps


def transform(seed: int, category: list[tuple[range, int]]) -> int:
    for src_range, offset in category:
        if seed in src_range:
            return seed - offset
    else:
        return seed


def p1(seeds_list: list[int], category_maps: list[list[tuple[range, int]]]) -> int:
    seed_locations = []
    for seed in seeds_list:
        number = seed
        for category in category_maps:
            number = transform(number, category)
        seed_locations.append(number)
    return min(seed_locations)


def range_intersection(range_a: range, range_b: range) -> range | None:
    intersection = range(max(range_a.start, range_b.start), min(range_a.stop, range_b.stop))
    return intersection or None


def transform_ranges(ranges: list[range], category: list[tuple[range, int]]) -> list[range]:
    transformed = []
    to_transform = ranges[:]

    while to_transform:
        range_ = to_transform.pop(0)
        for src_range, offset in category:
            if intersection := range_intersection(range_, src_range):
                if range_.start < intersection.start:
                    to_transform.append(range(range_.start, intersection.start))  # untransformed (for now)

                if range_.stop > intersection.stop:
                    to_transform.append(range(intersection.stop, range_.stop))  # untransformed (for now)

                # Transform the overlapping bit
                transformed.append(range(intersection.start - offset, intersection.stop - offset))
                break
        else:
            # Intersected no transformation ranges for this category
            transformed.append(range_)
    return transformed


# Unused (not actually needed), but keeping around.
# def merge_overlapping_ranges(ranges: list[range]) -> list[range]:
#     to_process = list(sorted(ranges, key=lambda r: r.start))
#     merged = [to_process.pop(0)]
#
#     while to_process:
#         current = to_process.pop(0)
#         for existing in merged:
#             if current.start - 1 in existing or current.stop + 1 in existing:
#                 merged.remove(existing)
#                 current = range(min(current.start, existing.start), max(current.stop, existing.stop))
#                 merged.append(current)
#
#         if current not in merged:
#             merged.append(current)
#
#     return merged


def p2(seed_ranges: list[range], category_maps: list[list[tuple[range, int]]]) -> int:
    ranges = seed_ranges[:]
    for category in category_maps:
        # ranges = merge_overlapping_ranges(ranges)
        ranges = transform_ranges(ranges, category)
    return min(r.start for r in ranges)


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        sections = file.read().split("\n\n")

    seeds, seed_ranges, maps = parse(sections)

    print(f"Part 1: {p1(seeds, maps)}")
    print(f"Part 2: {p2(seed_ranges, maps)}")
