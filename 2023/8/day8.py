import itertools
import math
import re
from collections.abc import Iterator, Sequence, Callable
from typing import Literal

Network = dict[str, dict[str, str]]


def parse(line_data: list[str]) -> Network:
    parsed_network = {}
    for line in line_data:
        source, left, right = re.fullmatch(r"(\w+) = \((\w+), (\w+)\)", line).groups()
        parsed_network[source] = {"L": left, "R": right}
    return parsed_network


def steps_until(
        start: str,
        arrived: Callable[[str], bool],
        network: Network,
        directions: Iterator[Literal["R", "L"]]
) -> int:
    current = start

    for step_count in itertools.count():
        if arrived(current):
            return step_count
        current = network[current][next(directions)]


def p1(network: Network, instructions: Sequence[Literal["R", "L"]]) -> int:
    directions_gen = itertools.cycle(instructions)
    return steps_until("AAA", lambda n: n == "ZZZ", network, directions_gen)


def p2(network: Network, instructions: Sequence[Literal["R", "L"]]) -> int:
    start_nodes = {n for n in network if n.endswith("A")}
    cycle_lengths = set()
    # Turns out that each A-node will cycle to the same Z-node over and over
    # again, AND the A-nodes are all immediately following their respective
    # Z-node. So we only need to find the length of this cycle for each A-node
    # and find the LCM of these lengths.
    for start in start_nodes:
        directions_generator = itertools.cycle(instructions)
        cycle_steps = steps_until(start, lambda n: n.endswith("Z"), network, directions_generator)
        cycle_lengths.add(cycle_steps)

    return math.lcm(*cycle_lengths)


if __name__ == '__main__':
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    # with open("test3.txt") as file:
    with open("input.txt") as file:
        lines = file.read().split("\n\n")

    leftright: Sequence = lines[0]
    data = parse(lines[1].splitlines())

    print(f"Part 1: {p1(data, leftright)}")
    print(f"Part 2: {p2(data, leftright)}")
