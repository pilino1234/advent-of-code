import itertools

History = list[int]


def differentiate(history: History) -> History:
    return [b - a for (a, b) in itertools.pairwise(history)]


def extrapolate(histories: list[History]):
    for i in range(len(histories) - 2, -1, -1):
        above = histories[i + 1][-1]
        own_last = histories[i][-1]
        own_next = own_last + above
        histories[i].append(own_next)


def p1(sensor_data: list[History]) -> int:
    extrapolated = []
    for history in sensor_data:
        diffs = [history]
        current = history
        while any(current):
            diffs.append(current := differentiate(current))

        extrapolate(diffs)

        extrapolated.append(diffs[0][-1])

    return sum(extrapolated)


def extrapolate_recursive(history: History) -> int:
    """Extrapolate next value for a history of sensor values and return it."""
    if not any(history):
        # Base case; if history contains all 0's, return a 0.
        return history[-1]

    # Last value of this history + extrapolated next value of sequence of differences of history elements
    return history[-1] + extrapolate_recursive([b - a for (a, b) in itertools.pairwise(history)])


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = [[int(d) for d in line.split()] for line in lines]
    reversed_data = [[*reversed(d)] for d in data]

    print(f"Part 1 (recursive): {sum(extrapolate_recursive(h) for h in data)}")
    print(f"Part 2 (recursive): {sum(extrapolate_recursive(h) for h in reversed_data)}")
    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p1(reversed_data)}")
