from collections import Counter
from enum import auto, Enum
from functools import total_ordering
from typing import Self

PART = 1


@total_ordering
class HandType(Enum):
    HIGH_CARD = auto()
    ONE_PAIR = auto()
    TWO_PAIR = auto()
    THREE_OF_A_KIND = auto()
    FULL_HOUSE = auto()
    FOUR_OF_A_KIND = auto()
    FIVE_OF_A_KIND = auto()

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented

    @staticmethod
    def determine_hand_type(cards: str) -> "HandType":
        card_counts = Counter(cards)

        if PART == 2 and 1 <= card_counts["J"] < 5:
            # If jokers present, add them to the biggest existing card type for best result
            j_count = card_counts.pop("J")
            [(most_common_card_type, _)] = card_counts.most_common(1)
            card_counts[most_common_card_type] += j_count

        match tuple(sorted(card_counts.values())):
            case [5]:
                return HandType.FIVE_OF_A_KIND
            case [1, 4]:
                return HandType.FOUR_OF_A_KIND
            case [2, 3]:
                return HandType.FULL_HOUSE
            case [1, 1, 3]:
                return HandType.THREE_OF_A_KIND
            case [1, 2, 2]:
                return HandType.TWO_PAIR
            case [1, 1, 1, 2]:
                return HandType.ONE_PAIR
            case [1, 1, 1, 1, 1]:
                return HandType.HIGH_CARD
            case _unknown:
                raise ValueError(f"Unknown hand type for hand '{cards}', {card_counts}")


@total_ordering
class Hand:
    def __init__(self, cards: str, bid: int):
        self.cards: str = cards
        self.bid: int = bid

    @property
    def hand_type(self):
        return HandType.determine_hand_type(self.cards)

    def __repr__(self):
        return f"Hand({self.cards}, {self.bid})"

    def __lt__(self, other) -> bool:
        if not isinstance(other, Hand):
            return NotImplemented

        # Compare hand type first
        if self.hand_type != other.hand_type:
            return self.hand_type < other.hand_type

        # If same hand type, compare card strengths
        for own_card, other_card in zip(self.cards, other.cards):
            if own_card != other_card:
                return card_strength(own_card) < card_strength(other_card)

    @classmethod
    def from_line(cls, line: str) -> Self:
        hand, bid = line.split()
        return cls(hand, int(bid))


def card_strength(card: str) -> int:
    if PART == 1:
        return -"AKQJT98765432".index(card)
    else:
        return -"AKQT98765432J".index(card)


def calculate_total_winnings(hands: list[Hand]) -> int:
    return sum(rank * hand.bid for rank, hand in enumerate(sorted(hands), start=1))


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = [Hand.from_line(line) for line in lines]

    # for h in data:
    #     print(h, HandType.determine_hand_type(h.cards), HandType.determine_hand_type2(h.cards))

    print(f"Part 1: {calculate_total_winnings(data)}")
    PART = 2
    print(f"Part 2: {calculate_total_winnings(data)}")
