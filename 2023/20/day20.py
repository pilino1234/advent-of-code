import math
from abc import ABC, abstractmethod
from collections import defaultdict, Counter
from typing import Type

Signal = bool
LOW_PULSE = False
HIGH_PULSE = True


class Module(ABC):
    def __init__(self, name: str):
        self.name = name

        self._outputs = []
        self._queue = []

        self._sent = Counter()

    @property
    def low_sent(self) -> int:
        return self._sent[LOW_PULSE]

    @property
    def high_sent(self) -> int:
        return self._sent[HIGH_PULSE]

    @property
    def finished(self):
        return len(self._queue) == 0

    def register_output(self, output: "Module"):
        self._outputs.append(output)

    def _broadcast(self, value: Signal):
        for module in self._outputs:
            # print(f"{self.name} -{'high' if value else 'low'}-> {module.name}")
            self._sent[value] += 1
            module.receive(value, self.name)

    def receive(self, value: Signal, from_: str):
        self._queue.append((value, from_))

    def process(self):
        if not self._queue:
            return  # empty queue, nothing to do

        value, sender = self._queue.pop(0)
        self._process(value, sender)

    @abstractmethod
    def _process(self, value: Signal, sender: str):
        raise NotImplementedError

    def reset(self):
        self._sent.clear()
        self._queue.clear()
        self._reset()

    @abstractmethod
    def _reset(self):
        raise NotImplementedError


class PlainModule(Module):
    """Dummy module that does nothing."""

    def _process(self, value: Signal, sender: str):
        pass

    def _reset(self):
        pass


class FlipFlopModule(Module):
    def __init__(self, name: str):
        super().__init__(name)

        self._state: Signal = LOW_PULSE

    def _process(self, value: Signal, sender: str):
        if not value:
            # Low value toggles state
            self._state = not self._state
            self._broadcast(self._state)
        else:
            # High value does nothing
            pass

    def _reset(self):
        self._state = LOW_PULSE


class NANDModule(Module):
    def __init__(self, name: str, inputs: list["str"]):
        super().__init__(name)

        self._state: dict[str, Signal] = {i: LOW_PULSE for i in inputs}

    def _process(self, value: Signal, sender: str):
        self._state[sender] = value
        if all(self._state.values()):
            # If all inputs true, send low pulse
            self._broadcast(LOW_PULSE)
        else:
            self._broadcast(HIGH_PULSE)

    def _reset(self):
        for input_signal in self._state:
            self._state[input_signal] = LOW_PULSE


class BroadcastModule(Module):
    def _process(self, value: Signal, sender: str):
        self._broadcast(value)

    def _reset(self):
        pass


class ButtonModule(Module):
    def __init__(self, name: str):
        super().__init__(name)

        self._presses: int = 0

    @property
    def times_pressed(self):
        return self._presses

    def _process(self, value: Signal, sender: str):
        pass

    def press(self):
        # Button press sends a low pulse
        self._presses += 1
        self._broadcast(LOW_PULSE)

    def _reset(self):
        self._presses = 0


def parse(line_data: list[str]) -> tuple[dict[str, Module], list[Module]]:
    inputs = defaultdict(list)
    outputs = defaultdict(list)
    types: dict[str, Type[Module]] = {}

    for line in line_data:
        sender, receivers = line.split(" -> ")

        if sender.startswith("%"):
            type_ = FlipFlopModule
            name = sender[1:]
        elif sender.startswith("&"):
            type_ = NANDModule
            name = sender[1:]
        elif sender == "broadcaster":
            type_ = BroadcastModule
            name = sender
        else:
            # plain module
            type_ = PlainModule
            name = sender
        types[name] = type_
        outputs[name] = receivers.split(", ")

    # Add button
    types["button"] = ButtonModule
    outputs["button"].append("broadcaster")

    # Generate inputs mapping (inverse of outputs)
    for src, dests in outputs.items():
        for dest in dests:
            inputs[dest].append(src)

    # Generate plain modules for any names that do not have a specified type
    for module_name in inputs:
        if module_name not in types:
            types[module_name] = PlainModule

    # Construct the module objects
    parsed_modules: dict[str, Module] = {}
    for name, type_ in types.items():
        type_: Type[Module] | Type[NANDModule]
        if type_ is NANDModule:
            parsed_modules[name] = type_(name, inputs=inputs[name])
        else:
            parsed_modules[name] = type_(name)

    # Wire up the outputs
    for src, dests in outputs.items():
        for dest in dests:
            parsed_modules[src].register_output(parsed_modules[dest])

    # For part 2, trace back to the NAND-gates preceding rx, which takes input from exactly one NAND-gate.
    # In turn, this one takes input from 4 NAND-gates
    rx_parent = inputs["rx"]
    assert len(rx_parent) == 1, "rx should have only 1 parent"
    rx_parent_inputs = inputs[rx_parent[0]]

    return parsed_modules, [parsed_modules[name] for name in rx_parent_inputs]


def p1(modules: dict[str, Module]) -> int:
    # Button press sends a low pulse to broadcaster
    # noinspection PyTypeChecker
    button: ButtonModule = modules["button"]
    for _ in range(1000):
        button.press()
        finished = False
        while not finished:
            for m in modules.values():
                m.process()
            finished = all(m.finished for m in modules.values())

    low_sent = sum(m.low_sent for m in modules.values())
    high_sent = sum(m.high_sent for m in modules.values())
    return low_sent * high_sent


def p2(modules: dict[str, Module], rx_parents: list[Module]) -> int:
    # Reset all modules
    for module in modules.values():
        module.reset()

    # Button press sends a low pulse to broadcaster
    # noinspection PyTypeChecker
    button: ButtonModule = modules["button"]

    presses_until_high: dict[str, int] = {}
    done = False
    while not done:
        button.press()
        finished = False
        while not finished:
            for m in modules.values():
                m.process()
            finished = all(m.finished for m in modules.values())

        # Check if any of the chains produced a high pulse
        for m in rx_parents:
            if m.name not in presses_until_high and m.high_sent:
                # print(f"Module {m.name} sent high output after {button.times_pressed} presses")
                presses_until_high[m.name] = button.times_pressed
        done = all(m.name in presses_until_high for m in rx_parents)

    return math.lcm(*presses_until_high.values())


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    mods, p2_final_4 = parse(lines)

    print(f"Part 1: {p1(mods)}")
    print(f"Part 2: {p2(mods, p2_final_4)}")
