from collections.abc import Callable
from typing import NamedTuple, Optional

Point = tuple[int, int]


def above(p: Point, distance: int = 1) -> Point:
    return p[0] - distance, p[1]


def below(p: Point, distance: int = 1) -> Point:
    return p[0] + distance, p[1]


def left(p: Point, distance: int = 1) -> Point:
    return p[0], p[1] - distance


def right(p: Point, distance: int = 1) -> Point:
    return p[0], p[1] + distance


def nb4(p: Point) -> tuple[Point, Point, Point, Point]:
    return above(p), below(p), left(p), right(p)


class Dig(NamedTuple):
    direction: Callable[[Point, Optional[int]], Point]
    distance: int
    colour: str


def parse(line_data: list[str]):
    parsed = []
    for line in line_data:
        direction, distance, colour = line.split(" ")
        direction = {
            "R": right,
            "L": left,
            "U": above,
            "D": below,
        }[direction]
        distance = int(distance)
        colour = colour.strip("()")
        parsed.append(Dig(direction, distance, colour))

    return parsed


def parse_p2(digs: list[Dig]) -> list[Dig]:
    new_digs = []
    for dig in digs:
        c = dig.colour[1:]
        distance = int(c[:5], 16)
        direction = {
            "0": right,
            "1": below,
            "2": left,
            "3": above,
        }[c[5:]]
        new_digs.append(Dig(direction, distance, c))
    return new_digs


def dig_middle(edge_digs: set[Point]) -> set[Point]:
    """Scuffed flood fill. Keeping it because it took a long time to write."""
    rows = [p[0] for p in edge_digs]
    cols = [p[1] for p in edge_digs]
    min_row = min(rows)
    max_row = max(rows)
    min_col = min(cols)
    max_col = max(cols)

    def is_inside(p: Point) -> bool:
        return min_row <= p[0] <= max_row and min_col <= p[1] <= max_col

    tiles_to_process = {(row, col) for row in range(min_row, max_row + 1) for col in range(min_col, max_col + 1)}
    tiles_to_process -= edge_digs

    inside = set()
    outside = set()

    while tiles_to_process:
        current = tiles_to_process.pop()
        working = {current}
        candidates = {p for p in nb4(current) if p not in edge_digs}
        reaches_edge = False

        while candidates:
            current = candidates.pop()
            working.add(current)
            nbs = nb4(current)
            for nb in nbs:
                if nb in working:
                    continue
                elif not is_inside(nb):
                    reaches_edge = True
                    continue
                elif nb not in tiles_to_process:
                    continue
                candidates.add(nb)

        if reaches_edge:
            outside |= working
        else:
            inside |= working
        tiles_to_process -= working
        working.clear()

    return inside


def p1(dig_plan: list[Dig]) -> int:
    current: Point = (0, 0)
    dug = {current}

    for instruction in dig_plan:
        for _ in range(instruction.distance):
            current = instruction.direction(current)
            dug.add(current)

    middle = dig_middle(dug)
    # draw_dig(dug | middle)

    return len(dug) + len(middle)


def draw_dig(dug_tiles: set[Point], inside: set[Point] | None = None, outside: set[Point] | None = None):
    rows = [p[0] for p in dug_tiles]
    cols = [p[1] for p in dug_tiles]
    min_row = min(rows)
    max_row = max(rows)
    min_col = min(cols)
    max_col = max(cols)

    for row in range(min_row, max_row + 1):
        for col in range(min_col, max_col + 1):
            if (row, col) in dug_tiles:
                char = "#"
            elif inside and (row, col) in inside:
                char = "I"
            elif outside and (row, col) in outside:
                char = "O"
            else:
                char = "."
            print(char, end="")
        print()
    print()


def polygon_grid_cells(polygon_points: list[Point], boundary_length: int) -> int:
    """
    Calculate the number of grid cells covered by the polygon given by the points in `polygon_points`.

    Grid cells covered = boundary + contained points
                       = boundary_length + interior_points

    where the number of interior points is calculated by a combination of the Trapezoid Formula (Shoelace formula)
    and Pick's theorem.
    """
    # Use Trapezoid Formula variant of shoelace formula to calculate the area of the polygon
    partial_result = 0
    for p_a, p_b in zip(polygon_points, polygon_points[1:] + polygon_points[:1]):
        partial_result += (p_a[1] + p_b[1]) * (p_a[0] - p_b[0])
    area = abs(partial_result // 2)

    # Now, use Pick's theorem to calculate the number of contained grid cells in the polygon
    # The theorem states that area = interior points + (boundary length / 2) - 1
    # To calculate interior points, use area - (boundary / 2) + 1
    interior_points = area - (boundary_length // 2) + 1

    # Total contained grid cells is boundary + interior points
    # This effectively returns area + (boundary_length // 2) + 1
    return boundary_length + interior_points


def p2(dig_plan: list[Dig]) -> int:
    current: Point = (0, 0)
    corner_points = [current]

    for instruction in dig_plan:
        current = instruction.direction(current, instruction.distance)
        corner_points.append(current)

    return polygon_grid_cells(corner_points, boundary_length=sum(d.distance for d in dig_plan))


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1 (original): {p1(data)}")
    print(f"Part 1 (faster): {p2(data)}")

    data2 = parse_p2(data)

    assert p2([
        Dig(right, 1, ""),
        Dig(below, 1, ""),
        Dig(left, 1, ""),
        Dig(above, 1, ""),
    ]) == 4

    assert p2([
        Dig(right, 2, ""),
        Dig(below, 2, ""),
        Dig(left, 2, ""),
        Dig(above, 2, ""),
    ]) == 9

    print(f"Part 2: {p2(data2)}")
