import itertools

Point = tuple[int, int]
Universe = set[Point]


def cosmic_expansion(line_data: list[str], expansion_factor: int) -> Universe:
    universe = set()

    for row_idx, row in enumerate(line_data):
        for column_idx, char in enumerate(row):
            if char == "#":
                universe.add((row_idx, column_idx))

    # draw_universe(universe)

    # Go through all rows in reverse
    for row_idx in range(len(line_data) - 1, -1, -1):
        if all(c == "." for c in line_data[row_idx]):
            for pos in [p for p in universe if p[0] > row_idx]:
                universe.remove(pos)
                universe.add((pos[0] + expansion_factor - 1, pos[1]))

    # Then all columns in reverse
    for col_idx in range(len(line_data[0]) - 1, -1, -1):
        if all(c == "." for c in (l[col_idx] for l in line_data)):
            for pos in [p for p in universe if p[1] > col_idx]:
                universe.remove(pos)
                universe.add((pos[0], pos[1] + expansion_factor - 1))

    # draw_universe(universe)

    return universe


def draw_universe(universe: Universe):
    rows = [p[0] for p in universe]
    cols = [p[1] for p in universe]
    min_row = min(rows)
    max_row = max(rows)
    min_col = min(cols)
    max_col = max(cols)

    for row in range(min_row, max_row + 1):
        for col in range(min_col, max_col + 1):
            if (row, col) in universe:
                char = "#"
            else:
                char = "."
            print(char, end="")
        print()
    print()


def manhattan_distance(p: Point, q: Point) -> int:
    return abs(p[0] - q[0]) + abs(p[1] - q[1])


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = cosmic_expansion(lines, expansion_factor=2)
    print(f"Part 1: {sum(manhattan_distance(g1, g2) for g1, g2 in itertools.combinations(data, 2))}")

    data = cosmic_expansion(lines, expansion_factor=1_000_000)
    print(f"Part 2: {sum(manhattan_distance(g1, g2) for g1, g2 in itertools.combinations(data, 2))}")
