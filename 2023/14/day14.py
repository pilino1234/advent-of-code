import itertools
from collections.abc import Callable
from typing import Literal

Rock = tuple[int, int]


def parse(line_data: list[str]) -> tuple[set[Rock], set[Rock], int, int]:
    cubes = set()
    round_rocks = set()

    for row_idx, row in enumerate(line_data):
        for col_idx, char in enumerate(row):
            if char == ".":
                continue
            elif char == "#":
                cubes.add((row_idx, col_idx))
            elif char == "O":
                round_rocks.add((row_idx, col_idx))
            else:
                raise ValueError(f"Unknown character {char}")

    grid_height = len(line_data)
    grid_width = len(line_data[0])

    return cubes, round_rocks, grid_height, grid_width


SORTING = {
    "up": lambda p: p[0],
    "down": lambda p: -p[0],
    "left": lambda p: p[1],
    "right": lambda p: -p[1],
}

DIRECTIONS = {
    "up": lambda p: (p[0] - 1, p[1]),
    "down": lambda p: (p[0] + 1, p[1]),
    "left": lambda p: (p[0], p[1] - 1),
    "right": lambda p: (p[0], p[1] + 1),
}


def tilt(
        cube_rocks: set[Rock],
        round_rocks: set[Rock],
        direction: Literal["up", "down", "left", "right"],
        width: int,
        height: int
) -> set[Rock]:
    new_round_rocks = set()
    move: Callable[[Rock], Rock] = DIRECTIONS[direction]
    for round_rock in sorted(round_rocks, key=SORTING[direction]):
        curr_pos = round_rock
        while True:
            new_pos = move(curr_pos)
            if new_pos in cube_rocks or new_pos in new_round_rocks:
                # Would hit a rock, stay at current pos
                new_round_rocks.add(curr_pos)
                break
            elif not (0 <= new_pos[0] < width and 0 <= new_pos[1] < height):
                # Would hit edge, stay at current pos
                new_round_rocks.add(curr_pos)
                break
            curr_pos = new_pos

    return new_round_rocks


def p1(cubes: set[Rock], round_rocks: set[Rock], num_rows: int, num_cols: int) -> int:
    round_rocks_north = tilt(cubes, round_rocks, "up", num_rows, num_cols)
    return sum(num_rows - rock[0] for rock in round_rocks_north)


def spin(cubes: set[Rock], round_rocks: set[Rock], dish_height: int, dish_width: int) -> set[Rock]:
    new_round = tilt(cubes, round_rocks, "up", dish_height, dish_width)
    new_round = tilt(cubes, new_round, "left", dish_height, dish_width)
    new_round = tilt(cubes, new_round, "down", dish_height, dish_width)
    new_round = tilt(cubes, new_round, "right", dish_height, dish_width)
    return new_round


def p2(cubes: set[Rock], round_rocks: set[Rock], num_rows: int, num_cols: int, repetitions: int) -> int:
    loop_start = None
    loop_length = None

    seen = {}
    prev_round_rocks = round_rocks
    for idx in itertools.count(start=1):
        new_round = spin(cubes, prev_round_rocks, num_rows, num_cols)

        if tuple(new_round) in seen:
            # Loop started the first time this arrangement was seen
            first_seen = seen[tuple(new_round)]
            loop_start = first_seen
            loop_length = idx - first_seen
            break
        seen[tuple(new_round)] = idx
        prev_round_rocks = new_round

    # Calculate the index that we would reach at the end of all repetitions:
    # loop_start iterations to get into the loop, then mod with the length of the loop.
    # Finally, need index *into the loop* (not just index from start)
    final_idx = (repetitions - loop_start) % loop_length + loop_start
    # Get the final arrangement of round rocks at this repetition
    final_round_rocks = next((round_positions for round_positions, idx in seen.items() if idx == final_idx), None)

    return sum(num_rows - rock[0] for rock in final_round_rocks)


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(*data, repetitions=1_000_000_000)}")
