import itertools

Pattern = list[str]


def find_reflection(pattern: Pattern, ignore_line: int = None) -> int | None:
    """Get number of lines before the reflection line."""
    for count_a, line_a, line_b, count_b in zip(itertools.count(1), pattern, pattern[1:], itertools.count(2)):
        if line_a != line_b or count_a == ignore_line:
            # If lines are not equal or this reflection should be ignored (part 2), skip this
            continue

        if count_a == 1:
            # If reflection is directly at the edge there is nothing more to check
            return count_a

        # Possible reflection, check neighbours outwards
        for l1, l2 in zip(pattern[count_a-2::-1], pattern[count_b:]):
            if l1 != l2:
                break
        else:
            # Valid reflection, return number of lines before
            return count_a
    # No reflection found
    return None


def transpose(pattern: Pattern) -> Pattern:
    return ["".join(v) for v in zip(*pattern)]


def p1(patterns: list[Pattern]) -> int:
    result = 0

    for pattern in patterns:
        if lines_before := find_reflection(pattern):
            result += lines_before * 100
            continue

        transposed_pattern = transpose(pattern)

        if lines_before := find_reflection(transposed_pattern):
            result += lines_before

    return result


def flip(pos_row, pos_col, pattern: Pattern) -> Pattern:
    out = []
    for row_idx, row in enumerate(pattern):
        if row_idx != pos_row:
            out.append(row)
            continue

        line = []
        for col_idx, char in enumerate(row):
            if col_idx != pos_col:
                line.append(char)
                continue

            line.append("." if char == "#" else "#")
        out.append("".join(line))
    return out


def try_all_smudges(pattern: Pattern, previous_line: int | None) -> int | None:
    for row in range(len(pattern)):
        for col in range(len(pattern[row])):
            p = flip(row, col, pattern)

            if reflection_line := find_reflection(p, ignore_line=previous_line):
                # This is the smudge location
                return reflection_line
    return None


def p2(patterns: list[Pattern]) -> int:
    result = 0

    for pattern in patterns:
        transposed_pattern = transpose(pattern)

        if previous_line := find_reflection(pattern):
            if new_line := try_all_smudges(pattern, previous_line):
                result += new_line * 100
                continue
            if new_line := try_all_smudges(transposed_pattern, None):
                result += new_line
                continue

        if previous_line := find_reflection(transposed_pattern):
            if new_line := try_all_smudges(transposed_pattern, previous_line):
                result += new_line
                continue
            if new_line := try_all_smudges(pattern, None):
                result += new_line * 100
                continue

    return result


if __name__ == '__main__':
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        data = [p.splitlines() for p in file.read().split("\n\n")]

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
