import math
import re


def p1(engine_schematic: list[str]) -> int:
    sum_of_part_numbers = 0

    symbol_positions: dict[tuple[int, int], str] = {
        (row, col): character
        for row, line in enumerate(engine_schematic)
        for col, character in enumerate(line)
        if not (character.isdigit() or character == ".")
    }

    for row, line in enumerate(engine_schematic):
        for found_number in re.finditer(r"\d+", line):
            box = {
                (y, x)
                for y in range(row - 1, row + 2)
                for x in range(found_number.start() - 1, found_number.end() + 1)
            }

            if box & symbol_positions.keys():
                # If box contains a symbol, this is a valid part number
                sum_of_part_numbers += int(found_number.group(0))

    return sum_of_part_numbers


def p2(engine_schematic: list[str]) -> int:
    gear_positions: dict[tuple[int, int], list[int]] = {
        (row, col): []
        for row, line in enumerate(engine_schematic)
        for col, character in enumerate(line)
        if character == "*"
    }

    for row, line in enumerate(engine_schematic):
        for found_number in re.finditer(r"\d+", line):
            box = {
                (y, x)
                for y in range(row - 1, row + 2)
                for x in range(found_number.start() - 1, found_number.end() + 1)
            }

            for gear_pos in box & gear_positions.keys():
                # If box contains a gear, store this valid part number at that gear
                gear_positions[gear_pos].append(int(found_number.group(0)))

    return sum(
        math.prod(adjacent_part_numbers)
        for adjacent_part_numbers in gear_positions.values()
        if len(adjacent_part_numbers) == 2
    )


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    print(f"Part 1: {p1(lines)}")
    print(f"Part 2: {p2(lines)}")
