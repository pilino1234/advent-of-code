def scuffed_hash(in_data: str) -> int:
    value = 0
    for c in in_data:
        value += ord(c)
        value *= 17
        value %= 256
    return value


def p1(sequence: list[str]) -> int:
    return sum(scuffed_hash(s) for s in sequence)


def p2(sequence: list[str]) -> int:
    boxes = [dict() for _ in range(256)]

    for step in sequence:
        if step.endswith("-"):
            label = step.removesuffix("-")
            box = scuffed_hash(label)
            # Remove the lens with this label
            boxes[box].pop(label, None)
        else:
            label, _, focal_length = step.partition("=")
            focal_length = int(focal_length)
            box = scuffed_hash(label)
            boxes[box][label] = focal_length

    focusing_powers = []
    for box, lenses in enumerate(boxes):
        for slot, (label, focal_length) in enumerate(lenses.items(), start=1):
            focusing_powers.append((1 + box) * slot * focal_length)
    return sum(focusing_powers)


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = file.read().strip()

    data = lines.split(",")

    assert scuffed_hash("HASH") == 52

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
