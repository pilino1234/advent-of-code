from functools import cache


def parse(line_data: list[str], expand: bool = False) -> list[tuple[str, tuple[int, ...]]]:
    parsed_lines = []

    for line in line_data:
        pattern, numbers = line.split(" ")
        numbers = tuple(int(n) for n in numbers.split(","))

        if expand:
            pattern = "?".join([pattern] * 4)
            numbers = numbers * 4

        parsed_lines.append((pattern, numbers))

    return parsed_lines


def is_valid(line: str, groups: tuple[int, ...], check_all: bool = False) -> bool:
    contained_groups = [g for g in line.split(".") if g]
    # print(line, groups, contained_groups)

    last_group_in_progress = False
    if check_all:
        if len(contained_groups) != len(groups):
            # print("groups")
            return False
    else:
        if line.endswith("#"):
            # contained_groups = contained_groups[:-1]
            # print("last in progress")
            last_group_in_progress = True
        ...

    group_lengths = {len(g) for g in (contained_groups[:-1] if last_group_in_progress else contained_groups)}
    expected_lengths = set(groups)
    if group_lengths - expected_lengths:
        # print("group lengths")
        return False

    for idx, (g, length) in enumerate(zip(contained_groups, groups)):
        #  or (idx == len(contained_groups) -1 and len(g) > length)
        if not check_all and line.endswith("#"):
            if idx == len(contained_groups) - 1:
                if len(g) > length:
                    # print("asdf", line, groups, contained_groups, idx, g, length, len(g) > length)
                    return False
        else:
            if len(g) != length:
                # print("len", line, groups, contained_groups, g, length, False)
                return False

    # print(line, groups, contained_groups, True)
    return True


# @functools.cache
def find_arrangements(placed: str, remaining: tuple[str, ...], groups: tuple[int, ...]) -> int:
    arrangements = 0

    if not remaining:
        arr = "".join(placed)
        if is_valid(arr, groups, check_all=True):
            # print(arr)
            arrangements += 1
        return arrangements

    valid_chars = [".", "#"]

    needed_springs = sum(groups)

    next_char, remaining = remaining[0], remaining[1:]

    if next_char in valid_chars:
        # arrangements.extend(find_arrangements(placed + next_char, remaining, groups))
        arrangements += find_arrangements(placed + next_char, remaining, groups)
    else:
        for char in valid_chars:
            if not is_valid(placed + char, groups):
                # print(f"!{char} ", end="")
                continue

            max_possible_springs = placed.count("#") + char.count("#") + remaining.count("#") + remaining.count("?")
            if max_possible_springs < needed_springs:
                # print(f"Pruning, {needed_springs=} but {max_possible_springs=}")
                # print(placed, remaining)
                continue
                # return arrangements

            # arrangements.extend(find_arrangements(placed + char, remaining, groups))
            arrangements += find_arrangements(placed + char, remaining, groups)

    return arrangements


def p1(spring_rows: list[tuple[str, tuple[int]]]) -> int:
    arrangements = 0

    for idx, (springs, groups) in enumerate(spring_rows, start=1):
        # print(f"{idx}/{len(spring_rows)} {springs} {groups}")

        arrs = find_arrangements("", tuple(springs), groups)
        # print(arrs)
        # print(len(arrs), arrs)
        arrangements += arrs
        # break

    return arrangements


def valid_arrangements(remaining_parts: str, groups: tuple[int, ...]) -> int:
    if not groups:
        if "#" not in remaining_parts:
            return 1
        return 0
    elif not remaining_parts:
        return 0
    else:
        max_possible_broken = remaining_parts.count("#") + remaining_parts.count("?")

        # alternative_placements = None
        if remaining_parts.startswith("#"):
            alternative_placements = 0
        else:
            alternative_placements = valid_arrangements(remaining_parts[1:], groups)

        child_placements = valid_arrangements(remaining_parts[groups[0]:], groups[1:])

        if max_possible_broken < sum(groups):
            return 0
        elif can_fit(groups[0], remaining_parts):
            return child_placements + alternative_placements
        else:
            return alternative_placements


def can_fit(brokensprings: int, record: str) -> bool:
    if brokensprings == 0 and not record:
        return True
    elif not record:
        return False
    elif brokensprings == 0:
        return not record.startswith("#")
    else:
        return (not record.startswith(".")) and can_fit(brokensprings - 1, record[1:])


@cache
def possible_arrangements(remaining: str, groups: tuple[int, ...]) -> int:
    if not groups:
        return "#" not in remaining  # False if broken springs in remainder

    current_group_length, *other_groups = groups
    min_tail = len(other_groups) + sum(other_groups)

    s = 0
    for prefix_len in range(len(remaining) - current_group_length - min_tail + 1):
        current = "." * prefix_len + "#" * current_group_length + "."
        if all(p == c or p == "?" for p, c in zip(remaining, current)):
            s += possible_arrangements(remaining[len(current):], tuple(other_groups))

    return s


def p2(spring_rows: list[tuple[str, tuple[int, ...]]]) -> int:
    arrangements = 0

    for idx, (springs, groups) in enumerate(spring_rows, start=1):
        print(f"{idx}/{len(spring_rows)} {springs} {groups} {'?'.join([springs] * 2)} {groups * 2}")

        # base_arrangements = find_arrangements("", tuple(springs), groups)
        # unfolded_arrangements = find_arrangements("", tuple("?".join([springs] * 2)), groups * 2)
        # arrangements_in_unfolded_part = unfolded_arrangements // base_arrangements
        # arrangements += base_arrangements * arrangements_in_unfolded_part ** 4

        base_arrangements = possible_arrangements(springs, groups)
        unfolded_arrangements = possible_arrangements("?".join([springs] * 2), groups * 2)

        full_unfold = possible_arrangements("?".join([springs] * 5), groups * 5)

        # base_arrangements = valid_arrangements(springs, groups)
        # unfolded_arrangements = valid_arrangements("?".join([springs] * 2), groups * 2)
        arrangements_in_unfolded_part = unfolded_arrangements // base_arrangements
        predicted = base_arrangements * arrangements_in_unfolded_part ** 4
        arrangements += predicted
        print(f"{base_arrangements} {unfolded_arrangements} {unfolded_arrangements // base_arrangements} {predicted} {full_unfold}")
        if predicted != full_unfold:
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

        # if unfolded_arrangements % base_arrangements != 0:
        #     print("\n!!!!!!!!!!!!!!!!!\n")

    return arrangements


def p2_full_unfold(spring_rows: list[tuple[str, tuple[int, ...]]]) -> int:
    arrangements = 0

    for idx, (springs, groups) in enumerate(spring_rows, start=1):
        # print(f"{idx}/{len(spring_rows)} {springs} {groups} {'?'.join([springs] * 2)} {groups * 2}")
        # base_arrangements = possible_arrangements(springs, groups)
        arrangements += possible_arrangements("?".join([springs] * 5), groups * 5)

        # base_arrangements = valid_arrangements(springs, groups)
        # unfolded_arrangements = valid_arrangements("?".join([springs] * 2), groups * 2)
        # arrangements_in_unfolded_part = unfolded_arrangements // base_arrangements
        # arrangements += base_arrangements * arrangements_in_unfolded_part ** 4
        # print(f"{base_arrangements} {unfolded_arrangements} {unfolded_arrangements / base_arrangements} {arrangements_in_unfolded_part} {arrangements}")
        # if unfolded_arrangements % base_arrangements != 0:
        #     print("\n!!!!!!!!!!!!!!!!!\n")

    return arrangements


if __name__ == '__main__':
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    # print(data)

    # print(f"Part 1: {sum(len(find_arrangements(*l)) for l in data)}")
    print(f"Part 1: {p1(data)}")

    # data = parse(lines, expand=True)
    # print(data)
    # print(f"Part 2: {p2(data)}")
    print(f"Part 2: {p2_full_unfold(data)}")
