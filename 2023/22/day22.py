from collections import defaultdict
from typing import Final

Point = tuple[int, int, int]
Brick = tuple[Point, ...]
Point2D = tuple[int, int]
Environment = dict[int, set[Point2D]]
X: Final[int] = 0
Y: Final[int] = 1
Z: Final[int] = 2


def brick_factory(start: Point, end: Point) -> Brick:
    cubes = [start]
    for dim in range(len(start)):
        if start[dim] != end[dim]:
            break
    else:
        # Brick contains just a single cube
        return tuple(cubes)

    working_cube = [*start]
    for val in range(start[dim] + 1, end[dim] + 1):
        working_cube[dim] = val
        cubes.append(tuple(working_cube))
    return tuple(cubes)


def parse(line_data: list[str]) -> list[Brick]:
    parsed_bricks = []
    for line in line_data:
        start, end = line.split("~")
        start = tuple(int(num) for num in start.split(","))
        end = tuple(int(num) for num in end.split(","))
        # noinspection PyTypeChecker
        parsed_bricks.append(brick_factory(start, end))
    return parsed_bricks


def min_z(brick: Brick) -> int:
    """Find lowest z-coordinate for given brick."""
    return min(brick, key=lambda b: b[Z])[Z]


def below(brick: Brick) -> Brick:
    """Shift brick downwards by one step."""
    return tuple((cube[X], cube[Y], cube[Z] - 1) for cube in brick)


def add_brick_to_environment(brick: Brick, environment: Environment):
    for cube in brick:
        assert (cube[X], cube[Y]) not in environment[cube[Z]]
        environment[cube[Z]].add((cube[X], cube[Y]))


def disintegrate_brick(brick: Brick, environment: Environment):
    for cube in brick:
        assert (cube[X], cube[Y]) in environment[cube[Z]]
        environment[cube[Z]].remove((cube[X], cube[Y]))


def is_supported(brick: Brick, environment: Environment) -> bool:
    for cube in brick:
        pos_below = (cube[X], cube[Y], cube[Z] - 1)

        if pos_below in brick:
            # Vertical bricks can't be supported by their own cubes
            continue
        elif pos_below[Z] == 0:
            # Rests on ground, brick is supported
            return True
        elif (pos_below[X], pos_below[Y]) in environment[pos_below[Z]]:
            # Rests on other brick
            return True
    return False


def settle_bricks(bricks: list[Brick]) -> tuple[Environment, list[Brick]]:
    # Place bricks into environment and settle any loose bricks
    # Process bricks sorted by their minimum z-coordinates
    settled_bricks: list[Brick] = []
    settled_environment: dict[int, set[Point2D]] = defaultdict(lambda: set())
    for brick in sorted(bricks, key=min_z):
        # Check if brick is supported
        supported = is_supported(brick, settled_environment)

        # If supported, add to settled env
        if supported:
            add_brick_to_environment(brick, settled_environment)
            settled_bricks.append(brick)
        else:
            # Move brick downwards until supported
            while not is_supported(brick, settled_environment):
                brick = below(brick)
            settled_bricks.append(brick)
            add_brick_to_environment(brick, settled_environment)

    assert len(settled_bricks) == len(bricks)
    return settled_environment, settled_bricks


def p1(settled_environment: Environment, settled_bricks: list[Brick]) -> int:
    # Find bricks that can be disintegrated
    disintegrated_bricks = set()
    for brick in settled_bricks:
        disintegrate_brick(brick, settled_environment)

        # For all bricks above this one, check if they are still supported
        min_z_for_brick = min_z(brick)
        bricks_above = (brick for brick in settled_bricks if min_z(brick) >= min_z_for_brick)
        for b2 in bricks_above:
            if not is_supported(b2, settled_environment):
                # If any brick becomes unsupported, stop checking more
                break
        else:
            # No bricks became unsupported, this one is safe to disintegrate
            disintegrated_bricks.add(brick)

        # Put the brick back
        add_brick_to_environment(brick, settled_environment)

    return len(disintegrated_bricks)


def p2(settled_environment: Environment, settled_bricks: list[Brick]) -> int:
    # Determine lengths of chain reactions
    chain_reaction_lengths: dict[Brick, int] = {}
    for brick in settled_bricks:
        disintegrate_brick(brick, settled_environment)
        chain_reaction = set()  # First brick is not included in chain reaction length
        remaining_bricks = {*settled_bricks}
        remaining_bricks.remove(brick)

        any_more_disintegrated = True
        while any_more_disintegrated:
            any_more_disintegrated = False
            for b2 in remaining_bricks.copy():
                if not is_supported(b2, settled_environment):
                    # If not supported, disintegrate the brick
                    remaining_bricks.remove(b2)
                    chain_reaction.add(b2)
                    disintegrate_brick(b2, settled_environment)
                    any_more_disintegrated = True

        # Don't include the first brick in the count
        chain_reaction_lengths[brick] = len(chain_reaction)

        # Put all the bricks back
        for disintegrated_brick in chain_reaction | {brick}:
            add_brick_to_environment(disintegrated_brick, settled_environment)

    return sum(chain_reaction_lengths.values())


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    data = settle_bricks(data)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(*data)}")
