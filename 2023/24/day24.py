import itertools
import re
from typing import NamedTuple, Self

Point = tuple[int, int]


class Hailstone(NamedTuple):
    px: int
    py: int
    pz: int
    vx: int
    vy: int
    vz: int

    @classmethod
    def from_str(cls, s: str) -> Self:
        return cls(*(int(n) for n in re.findall(r"-?\d+", s)))

    @property
    def slope(self) -> float:
        return self.vy / self.vx

    @property
    def y_intercept(self) -> float:
        return -self.px * self.slope + self.py

    def intersection(self, other: Self) -> tuple[float, float] | None:
        if self.slope == other.slope:
            # Parallel trajectories
            return None

        x_coord = (other.y_intercept - self.y_intercept) / (self.slope - other.slope)
        y_coord = self.slope * x_coord + self.y_intercept

        return x_coord, y_coord


def parse(line_data: list[str]) -> list[Hailstone]:
    return [Hailstone.from_str(line) for line in line_data]


def p1(hailstones: list[Hailstone], test_area: range) -> int:
    intersect_inside = 0
    for h1, h2 in itertools.combinations(hailstones, 2):
        intersection = h1.intersection(h2)

        # No intersection if trajectories are parallel
        if not intersection:
            continue

        # Check if intersection is within test area
        if not all(test_area.start <= coord <= test_area.stop for coord in intersection):
            continue

        # Check if intersection lies in past
        if h1.vx < 0 and intersection[0] > h1.px:
            continue
        elif h1.vx > 0 and intersection[0] < h1.px:
            continue
        elif h2.vx < 0 and intersection[0] > h2.px:
            continue
        elif h2.vx > 0 and intersection[0] < h2.px:
            continue

        intersect_inside += 1

    return intersect_inside


def p2(hailstones: list[Hailstone]) -> int:
    import z3

    rock = z3.RealVector("rock", 6)
    time = z3.RealVector("time", 3)

    s = z3.Solver()
    for axis in range(3):
        for t, hail in zip(time, hailstones):
            # h_l = [hail.px, hail.py, hail.pz, hail.vx, hail.vy, hail.vz]
            s.add(rock[axis] + rock[axis+3] * t == hail[axis] + hail[axis+3] * t)
    s.check()

    return s.model().eval(sum(rock[:3]))


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)
    # print(data)

    # print(f"Part 1: {p1(data, range(7, 27 + 1))}")
    print(f"Part 1: {p1(data, range(200_000_000_000_000, 400_000_000_000_000))}")
    print(f"Part 2: {p2(data)}")
