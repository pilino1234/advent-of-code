from heapq import heappop, heapify, heappush
from typing import NamedTuple, Literal

Point = tuple[int, int]


def parse(line_data: list[str]) -> dict[Point, int]:
    grid = {}
    for row_idx, row in enumerate(line_data):
        for col_idx, digit in enumerate(row):
            grid[(row_idx, col_idx)] = int(digit)

    return grid


def above(p: Point) -> Point:
    return p[0] - 1, p[1]


def below(p: Point) -> Point:
    return p[0] + 1, p[1]


def left(p: Point) -> Point:
    return p[0], p[1] - 1


def right(p: Point) -> Point:
    return p[0], p[1] + 1


DIRECTIONS = {
    "above": above,
    "below": below,
    "left": left,
    "right": right,
}


TURN_90_DIRS = {
    "above": ("left", "right"),
    "below": ("left", "right"),
    "left": ("above", "below"),
    "right": ("above", "below"),
}


class State(NamedTuple):
    loss: int
    position: Point
    direction: Literal["above", "below", "left", "right"]


def move_crucible(
    grid: dict[Point, int], minimum_straight: int, maximum_straight: int
) -> int:
    seen = set()
    q: list[State] = [State(0, (0, 0), "right"), State(0, (0, 0), "below")]
    heapify(q)

    # Goal in bottom right corner has max coordinate values
    goal: Point = max(grid)

    while q:
        loss, pos, direction = heappop(q)

        if (pos, direction) in seen:
            continue
        seen.add((pos, direction))

        if pos == goal:
            # Reached goal, return
            return loss

        for next_dir in TURN_90_DIRS[direction]:
            next_pos = pos
            next_loss = loss
            for distance in range(1, maximum_straight + 1):
                next_pos = DIRECTIONS[next_dir](next_pos)
                if next_pos not in grid:
                    break
                next_loss += grid[next_pos]

                if distance >= minimum_straight:
                    heappush(q, State(next_loss, next_pos, next_dir))


if __name__ == "__main__":
    # with open("test1.txt") as file:
    # with open("test2.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {move_crucible(data, 1, 3)}")
    print(f"Part 2: {move_crucible(data, 4, 10)}")
