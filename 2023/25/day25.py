from collections import defaultdict, Counter


def parse(line_data: list[str]) -> ...:
    graph = defaultdict(set)
    for line in line_data:
        n1, other = line.split(": ")
        for n2 in other.split():
            graph[n1].add(n2)
            graph[n2].add(n1)

    return graph


def p1(graph: dict[str, set[str]]) -> int:
    not_connected = Counter()
    for node in graph:
        not_connected[node] = 0

    while not_connected.total() != 3:
        next_nb, _ = not_connected.most_common(1)[0]
        not_connected.pop(next_nb)
        nbs = graph[next_nb]
        for nb in nbs:
            if nb in not_connected:
                not_connected[nb] += 1

    group1 = not_connected.keys()
    group2 = graph.keys() - group1
    return len(group1) * len(group2)


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
