from typing import NamedTuple

Point = tuple[int, int]


def parse(line_data: list[str]) -> tuple[dict[Point, str], Point]:
    grid = {}
    start_pos = None

    for row_idx, row in enumerate(line_data):
        for col_idx, char in enumerate(row):
            grid[(row_idx, col_idx)] = char
            if char == "S":
                start_pos = (row_idx, col_idx)

    return grid, start_pos


def above(p: Point) -> Point:
    return p[0] - 1, p[1]


def below(p: Point) -> Point:
    return p[0] + 1, p[1]


def left(p: Point) -> Point:
    return p[0], p[1] - 1


def right(p: Point) -> Point:
    return p[0], p[1] + 1


def nb4(p: Point) -> tuple[Point, Point, Point, Point]:
    return above(p), below(p), left(p), right(p)


def draw_gardens(grid: dict[Point, str], destinations: set[Point]):
    rows = [p[0] for p in grid]
    cols = [p[1] for p in grid]
    min_row = min(rows)
    max_row = max(rows)
    min_col = min(cols)
    max_col = max(cols)

    for row in range(min_row, max_row + 1):
        for col in range(min_col, max_col + 1):
            if (row, col) in destinations:
                char = "O"
            else:
                char = grid[(row, col)]
            print(char, end="")
        print()
    print()


class State(NamedTuple):
    steps: int
    pos: Point


def p1(grid: dict[Point, str], start: Point, max_steps: int) -> int:
    q: list[State] = [State(0, start)]

    rows = [p[0] for p in grid]
    edge_length = max(rows) - min(rows) + 1

    destinations = set()

    while q:
        current = q.pop(0)

        if current.pos in destinations:
            continue

        # if current.steps % 2 == 0:
        if current.steps % 2 == max_steps % 2:
            # Anything reachable with an even number of steps is reachable in 64 steps
            destinations.add(current.pos)

        if current.steps == max_steps:
            destinations.add(current.pos)
            continue

        for nb in nb4(current.pos):
            # Handle infinite grid by wrapping around on the base grid when checking for walls
            grid_pos = (nb[0] % edge_length, nb[1] % edge_length)
            if grid[grid_pos] == "#":
                continue
            q.append(State(current.steps + 1, nb))

    # draw_gardens(grid, destinations)

    return len(destinations)


def p2(grid: dict[Point, str], start: Point, max_steps: int) -> int:
    """
    Based partially on https://github.com/thomasjevskij/advent_of_code/blob/master/2023/aoc21/day21.py
    """
    rows = [p[0] for p in grid]
    edge_length = max(rows) - min(rows) + 1
    half = edge_length // 2

    reachable = []  # becomes = [3884, 34564, 95816]
    # Add one more "layer" of gardens around the first one twice, then extrapolate the trend
    for steps in (half, half + edge_length, half + 2 * edge_length):
        reachable.append(p1(grid, start, steps))

    # Number of reachable tiles grows quadratically in the number of layers added.
    # Calculate the coefficients of the quadratic equation describing this growth.
    # f(0) = reachable[0] = 3884 = reachable in 65 steps
    #      = a * 0^2 + b * 0 + c = c  -->  c = f0
    # f(1) = reachable[1] = 34564 = reachable in 196 steps
    #      = a * 1^2 + b * 1 + c = a + b + c  -->  a + b = f1 - f0
    # f(2) = reachable[2] = 95816 = reachable in 327 steps
    #      = a * 2^2 + b * 2 + c = 4a + 2b + c  -->  4a + 2b = f2 - f0
    #                                                2a      = f2 - f0 - 2(f1 - f0) = f2 - 2f1 + f0
    # and finally:
    #                                                      b = f1 - f0 - a
    # f(N) = ??? = reachable in 26501365 steps  <--  what we want to calculate
    [f0, f1, f2] = reachable
    c = f0
    a = (f2 - 2 * f1 + f0) // 2
    b = f1 - f0 - a

    # The number of layers to add to accommodate `max_steps` steps
    n = (max_steps - half) // edge_length
    return a * n**2 + b * n + c


if __name__ == "__main__":
    # with open("test1.txt") as file:
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    g, s = parse(lines)

    # For test data:
    # assert p1(g, s, 6) == 16
    # assert p1(g, s, 10) == 50
    # assert p1(g, s, 50) == 1_594
    # assert p1(g, s, 100) == 6_536
    # assert p1(g, s, 500) == 167_004  # slow

    print(f"Part 1: {p1(g, s, 64)}")
    print(f"Part 2: {p2(g, s, 26501365)}")
