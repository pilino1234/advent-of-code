from typing import List


def decode_image(pixel_data: str, width: int, height: int) -> List[str]:
    pixel_count = width * height

    layers: List[str] = []
    for i in range(0, len(pixel_data), pixel_count):
        layers.append(pixel_data[i:i + pixel_count])

    return layers


def corruption_detection(image_pixels: List[str]):
    layer_with_fewest_zeros = sorted(image_pixels, key = lambda s: s.count('0'))[0]

    return layer_with_fewest_zeros.count('1') * layer_with_fewest_zeros.count('2')


def render(image_layers: List[str], width: int, height: int) -> None:
    visible = ''
    # For each pixel
    for i in range(len(image_layers[0])):
        # Get pixel colours for this pixel from each layer
        layer_data_for_pixel = [l[i] for l in image_layers]
        # Find the first colour that is not transparent (!= 2)
        first_non_transparent = next((p for p in layer_data_for_pixel if int(p) < 2))
        visible += first_non_transparent

    # Output pixels
    for i in range(height):
        for j in range(width):
            pixel = visible[i * width + j]
            print('█' if pixel == '1' else ' ', end = '')
        print()  # Go to next line


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    image = decode_image(lines[0], 25, 6)
    print(corruption_detection(image))
    render(image, 25, 6)
