from typing import List

from IntCode import IntCode
from util import extract_all_numbers


def execute(instructions: List[int]) -> List[int]:
    pc = 0

    while True:
        opcode = instructions[pc]
        if opcode == 1:
            a = instructions[pc + 1]
            b = instructions[pc + 2]
            target = instructions[pc + 3]
            instructions[target] = instructions[a] + instructions[b]
        elif opcode == 2:
            a = instructions[pc + 1]
            b = instructions[pc + 2]
            target = instructions[pc + 3]
            instructions[target] = instructions[a] * instructions[b]
        elif opcode == 99:
            break
        else:
            break

        pc += 4

    return instructions


def brute_force(target: int, original_program: str) -> IntCode:
    for input_a in range(100):
        for input_b in range(100):
            numbers = extract_all_numbers(original_program)
            numbers[1] = input_a
            numbers[2] = input_b
            program = ','.join(map(str, numbers))

            comp = IntCode(program)
            comp.run()

            if comp.memory[0] == target:
                return comp


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    # Prepare program
    numbers = extract_all_numbers(lines[0])
    numbers[1] = 12
    numbers[2] = 2
    program = ','.join(map(str, numbers))

    computer = IntCode(program)
    computer.run()
    print(computer.memory[0])

    # Part 2
    result_computer = brute_force(target = 19690720, original_program = lines[0])
    print(100 * result_computer.memory[1] + result_computer.memory[2])
