from functools import reduce
from typing import List
from itertools import combinations
from math import gcd

import util


class Moon:
    counter = 0

    def __init__(self, x: int, y: int, z: int):
        self.x = x
        self.y = y
        self.z = z
        self.dx = 0
        self.dy = 0
        self.dz = 0

        self.name = Moon.counter
        Moon.counter += 1

    def move(self):
        self.x += self.dx
        self.y += self.dy
        self.z += self.dz

    @property
    def potential_energy(self) -> int:
        return sum(map(abs, [self.x, self.y, self.z]))

    @property
    def kinetic_energy(self) -> int:
        return sum(map(abs, [self.dx, self.dy, self.dz]))

    @property
    def energy(self) -> int:
        return self.potential_energy * self.kinetic_energy

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"Moon {self.name} @ ({self.x}, {self.y}, {self.z}) velocity=({self.dx}, {self.dy}, {self.dz})"


def parse_moons(in_data: List[str]) -> List[Moon]:
    parsed_moons = []
    for line in in_data:
        parsed_moons.append(Moon(*util.extract_all_numbers(line)))
    return parsed_moons


def simulate_step(moon_list: List[Moon]) -> List[Moon]:
    # Update velocity due to gravity
    # Moons are simple, so shallow copy works for them
    new_moons: List[Moon] = moon_list[:]
    for moon1, moon2 in combinations(moon_list, r = 2):  # type: Moon, Moon
        delta_x = moon2.x - moon1.x
        delta_y = moon2.y - moon1.y
        delta_z = moon2.z - moon1.z

        new_moon1 = next((moon for moon in new_moons if moon.name == moon1.name))
        new_moon2 = next((moon for moon in new_moons if moon.name == moon2.name))

        if delta_x:
            new_moon1.dx += 1 if delta_x > 0 else -1
            new_moon2.dx += -1 if delta_x > 0 else 1

        if delta_y:
            new_moon1.dy += 1 if delta_y > 0 else -1
            new_moon2.dy += -1 if delta_y > 0 else 1

        if delta_z:
            new_moon1.dz += 1 if delta_z > 0 else -1
            new_moon2.dz += -1 if delta_z > 0 else 1

    # Update position by applying velocity
    for moon in new_moons:
        moon.move()

    return new_moons


def simulate(moon_list: List[Moon], steps: int) -> List[Moon]:
    for step in range(1, steps + 1):
        moon_list = simulate_step(moon_list)

    return moon_list


def lcm(numbers):
    return reduce(lambda a, b: a * b // gcd(a, b), numbers)


def nature_of_the_universe(moon_list: List[Moon]):
    initial_state = {'x': [(moon.x, moon.dx) for moon in moon_list],
                     'y': [(moon.y, moon.dy) for moon in moon_list],
                     'z': [(moon.z, moon.dz) for moon in moon_list],
                     }
    phases = {'x': 0, 'y': 0, 'z': 0}
    step = 1

    while not all(phase for phase in phases.values()):
        moon_list = simulate(moon_list, 1)
        state = {'x': [], 'y': [], 'z': []}

        for moon in moon_list:
            state['x'].append((moon.x, moon.dx))
            state['y'].append((moon.y, moon.dy))
            state['z'].append((moon.z, moon.dz))

        for axis in state:
            if not phases[axis]:
                if state[axis] == initial_state[axis]:
                    phases[axis] = step

        step += 1

    return lcm(phases.values())


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    moons = parse_moons(lines)

    print(sum(m.energy for m in simulate(moons, 1000)))

    print(nature_of_the_universe(parse_moons(lines)))
