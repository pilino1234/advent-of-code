import collections
from itertools import zip_longest
from typing import List, Dict

import util
from IntCode import IntCode, State


def grouper(iterable, n, fillvalue = None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue = fillvalue)


def network(nic_code: List[int], nat_enabled: bool = False):
    computer_count = 50
    computers: Dict[int, IntCode] = {addr: IntCode(nic_code, [addr]) for addr in range(computer_count)}
    recv_queues: Dict[int, collections.deque] = {addr: collections.deque() for addr in range(computer_count)}

    nat_packet = None
    previous_nat_y = None

    while True:
        for address, comp in computers.items():
            if comp.state == State.WAITING:
                if recv_queues[address]:
                    packet = recv_queues[address].popleft()
                    comp.submit_input(packet[0])
                    comp.submit_input(packet[1])
                else:
                    comp.submit_input(-1)
            comp.run()
            output = comp.get_output(clear = True)
            assert len(output) % 3 == 0
            for destination, x, y in grouper(output, 3):
                if destination == 255 and not nat_enabled:
                    return y
                elif destination == 255 and nat_enabled:
                    nat_packet = (x, y)
                else:
                    recv_queues[destination].append((x, y))

        if all(comp.state == State.WAITING for comp in computers.values()) \
                and not any(queue for queue in recv_queues.values()) \
                and nat_enabled and nat_packet is not None:
            # Network is idle
            recv_queues[0].append(nat_packet)
            if nat_packet[1] == previous_nat_y:
                # Same y-value was sent twice in a row, return it
                return nat_packet[1]
            previous_nat_y = nat_packet[1]


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = util.extract_all_numbers(lines[0])

    print(network(program))
    print(network(program, nat_enabled = True))
