from collections import defaultdict
from typing import List, Tuple, Dict, Optional
from queue import Queue

import util
from IntCode import IntCode

directions = {
    1: (0, 1),  # Up
    2: (0, -1),  # Down
    3: (-1, 0),  # Left
    4: (1, 0),  # Right
}

opposite_directions = {1: 2, 2: 1, 3: 4, 4: 3}


def add_coord(pa: Tuple[int, int], pb: Tuple[int, int]) -> Tuple[int, int]:
    return pa[0] + pb[0], pa[1] + pb[1]


def repair_droid(droid_program: List[int]):
    area = defaultdict(lambda: -1)
    position: Tuple[int, int] = (0, 0)
    area[position] = 1

    goal: Optional[Tuple[int, int]] = None

    droid: IntCode = IntCode(droid_program, [])

    def dfs():
        nonlocal area, position, goal
        for direction_code, dir_ in directions.items():
            new_pos = add_coord(position, dir_)
            if new_pos not in area:
                original_pos = position
                droid.submit_input(direction_code)
                droid.run()
                [output] = droid.get_output(clear = True)
                area[new_pos] = output
                if output != 0:
                    position = new_pos
                    if output == 2:
                        goal = position
                    dfs()
                    # Once we return, go back one step
                    droid.submit_input(opposite_directions[direction_code])
                    droid.run()
                    assert droid.get_output(clear = True) != 0
                    position = add_coord(position, directions[opposite_directions[direction_code]])
                assert position == original_pos  # Check that we ended up back where we started

    dfs()
    print_map(area, position, goal)

    q = Queue()
    q.put(goal)
    seen = {goal: 0}

    while q.qsize():
        pos = q.get()
        for direction in directions.values():
            next_pos = add_coord(pos, direction)
            if area[next_pos] > 0 and next_pos not in seen:
                seen[next_pos] = seen[pos] + 1
                q.put(next_pos)

    print(seen[position])
    print(max(seen.values()))


def print_map(dict_map: Dict[Tuple[int, int], int], droid_pos: Tuple[int, int], goal_pos: Tuple[int, int]):
    xs = [key[0] for key in dict_map] + [droid_pos[0]]
    ys = [key[1] for key in dict_map] + [droid_pos[1]]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    for row in range(maxy, miny - 1, -1):
        for col in range(minx, maxx + 1):
            if (col, row) == droid_pos:
                print('▲', end = '')
                continue
            if (col, row) == goal_pos:
                print('⚑', end = '')
                continue
            data = dict_map[(col, row)]
            char = ''
            if data == 0:  # Wall
                char = '█'
            elif data == 1:  # Free
                char = ' '
            elif data == -1:  # Unknown
                char = '░'
            print(char, end = '')
        print()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    repair_droid(util.extract_all_numbers(lines[0]))
