import queue
from typing import List, Tuple

import networkx as nx


def add_coord(pa: Tuple[int, int], pb: Tuple[int, int]) -> Tuple[int, int]:
    """Helper to add coordinate tuples"""
    return pa[0] + pb[0], pa[1] + pb[1]


def adjacent_cells(position: Tuple[int, int]) -> List[Tuple[int, int]]:
    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    return [add_coord(position, d) for d in directions]


def create_graph(input_lines: List[str]):
    inner_portals = {}
    outer_portals = {}
    width = len(input_lines[0]) - 1
    height = len(input_lines) - 1

    maze_map: List[List[str]] = [[c for c in line] for line in input_lines]

    graph = nx.Graph()
    for y, line in enumerate(maze_map):
        for x, char in enumerate(line):
            neighbour_positions = adjacent_cells((y, x))

            # If the character is a passageway
            if char == '.':
                for nb in neighbour_positions:
                    nb_char = maze_map[nb[0]][nb[1]]
                    if nb_char == '.':
                        graph.add_edge((y, x), nb, weight = 1)

            # If the character is a portal
            if char.isalpha():
                neighbour_data = {}
                for nb in neighbour_positions:
                    if not (0 <= nb[0] <= height) or not (0 <= nb[1] <= width):
                        # Out of bounds, continue
                        continue
                    neighbour_data[nb] = maze_map[nb[0]][nb[1]]

                # If this portal is not directly next to the maze, continue
                if '.' not in neighbour_data.values():
                    continue
                assert any(val.isalpha() for val in neighbour_data.values())

                second_letter = next(char for char in neighbour_data.values() if char.isalpha())
                portal_name = ''.join(sorted((char, second_letter)))

                closest_path = next(key for key, val in neighbour_data.items() if val == '.')
                portal_dir = 1 if (y not in (1, height - 1) and x not in (1, width - 1)) else -1

                if portal_dir == -1:
                    outer_portals[portal_name] = closest_path
                elif portal_dir == 1:
                    inner_portals[portal_name] = closest_path

                if portal_name in inner_portals and portal_name in outer_portals:
                    inner_target = inner_portals[portal_name]
                    outer_target = outer_portals[portal_name]
                    graph.add_edge(inner_target, outer_target, weight = 1)
    return graph, inner_portals, outer_portals


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip('\n') for line in file]

    maze, inner, outer = create_graph(lines)

    print(len(nx.shortest_path(maze, source = outer['AA'],
                               target = outer['ZZ'])) - 1)

    bfs = queue.Queue()
    bfs.put((outer['AA'], 0, 0))
    seen = {}
    while bfs.not_empty:
        pos, dist, depth = bfs.get()
        if (pos, depth) in seen:
            continue
        seen[(pos, depth)] = dist

        if pos == outer['ZZ'] and depth == 0:
            print(dist)
            break

        for nb in maze[pos]:
            # Ignore any warps to targets on the inner edge when on the outermost level
            if depth == 0 and pos in outer.values() and nb in inner.values():
                continue

            # Travelling from the inner to the outer edge: depth increases by 1
            if pos in inner.values() and nb in outer.values():
                bfs.put((nb, dist + 1, depth + 1))
            # Travelling from the outer to the inner edge: depth decreases by 1
            elif pos in outer.values() and nb in inner.values():
                bfs.put((nb, dist + 1, depth - 1))
            else:
                bfs.put((nb, dist + 1, depth))
