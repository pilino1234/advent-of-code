import math
from typing import List, Dict
from collections import namedtuple


Chemical = namedtuple('Chemical', ['name', 'amount'])
Reaction = namedtuple('Reaction', ['reactants', 'product'])


def parse_reactions(input_reactions: List[str]) -> Dict[str, Reaction]:
    parsed_reactions = {}
    for line in input_reactions:
        raw_reactants, raw_product = line.split(' => ')

        reactants = []
        for reactant in raw_reactants.split(', '):
            amount, name = reactant.split(' ')
            reactants.append(Chemical(name, int(amount)))

        amount, name = raw_product.split(' ')
        product = Chemical(name, int(amount))

        parsed_reactions[product.name] = Reaction(reactants, product)

    return parsed_reactions


def requirements_for_fuel(possible_reactions: Dict[str, Reaction], fuel_amount: int = 1) -> Chemical:
    needed = {'FUEL': fuel_amount}
    while any(needed[chem] > 0 and chem != 'ORE' for chem in needed):
        # Choose the next chemical to produce
        target = [chem for chem in needed if chem != 'ORE' and needed[chem] > 0][0]
        # The needed amount of that chemical
        amount = needed[target]
        # Select a reaction that produces this chemical
        reaction = possible_reactions[target]
        # The amount of said chemical that is produced by the reaction
        product_quantity = reaction.product.amount
        # Find how many times the reaction needs to be done to produce the needed amount
        factor = math.ceil(amount/product_quantity)
        # Deduce the created amount of product from the needed amount
        needed[target] -= product_quantity * factor

        # Add the new reactions' reactants to the needed chemicals
        for reactant in reaction.reactants:  # type: Chemical
            if reactant.name in needed:
                needed[reactant.name] += reactant.amount * factor
            else:
                needed[reactant.name] = reactant.amount * factor
    return needed['ORE']


def binary_search_fuel(reactions_: Dict[str, Reaction]) -> int:
    a = 1
    b = 2
    # Find a starting range around the target value
    while requirements_for_fuel(reactions_, b) < 10 ** 12:
        a, b = b, b * 2
    while b - a >= 2:
        fuel = (a + b) // 2
        required_ore = requirements_for_fuel(reactions_, fuel)
        if required_ore >= 10 ** 12:
            b = fuel
        else:
            a = fuel
    return a


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    reactions = parse_reactions(lines)

    print(requirements_for_fuel(reactions))
    print(binary_search_fuel(reactions))
