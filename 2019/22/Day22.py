from typing import List
from collections import deque

import util


def part1(shuffles: List[str], length: int):
    deck = deque(range(length))
    direction = 1

    for shuffle in shuffles:
        if shuffle.startswith("deal into new stack"):
            direction *= -1
        elif shuffle.startswith("cut"):
            [number] = util.extract_all_numbers(shuffle)
            deck.rotate(-number * direction)
        elif shuffle.startswith("deal with increment"):
            [number] = util.extract_all_numbers(shuffle)
            new = deque(range(length))

            while deck:
                new[0] = deck.popleft() if direction == 1 else deck.pop()
                new.rotate(-number)

            deck = new
            direction = 1

    final_deck = list(deck) if direction == 1 else list(reversed(deck))

    return final_deck


# Borrowed from r/adventofcode btw, especially the explanation by mcpower.
def part2(shuffles: List[str], length: int, iterations: int, position: int):
    increment_multiplier = 1
    offset_diff = 0

    for shuffle in shuffles:
        if shuffle.startswith("deal into new stack"):
            increment_multiplier *= -1
            offset_diff += increment_multiplier
        elif shuffle.startswith("cut"):
            [number] = util.extract_all_numbers(shuffle)
            offset_diff += number * increment_multiplier
        elif shuffle.startswith("deal with increment"):
            [number] = util.extract_all_numbers(shuffle)
            increment_multiplier *= pow(number, length - 2, length)

        increment_multiplier %= length
        offset_diff %= length

    increment = pow(increment_multiplier, iterations, length)
    offset = offset_diff * (1 - increment) * pow((1 - increment_multiplier) % length, length - 2, length)
    offset %= length

    card = (offset + position * increment) % length
    print(f"Card at position {position} after {iterations} iterations: {card}")


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    shuffled = part1(lines, length = 10007)
    print(f"Index of card 2019 in deck after shuffling: {shuffled.index(2019)}")

    part2(lines, length = 119315717514047, iterations = 101741582076661, position = 2020)
