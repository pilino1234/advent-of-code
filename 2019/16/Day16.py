from typing import List, Union, Iterator
from itertools import repeat, chain, cycle


def fft_phase(input_list: Union[List[int], Iterator[int]], pattern: List[int]) -> List[int]:
    output_list = []
    for idx in range(1, len(input_list) + 1):
        stretched_pattern = cycle(chain.from_iterable(repeat(num, idx) for num in pattern))
        # Skip first element
        next(stretched_pattern)

        new_number = sum(num * patt for num, patt in zip(input_list, stretched_pattern))
        new_digit = abs(new_number) % 10

        output_list.append(new_digit)
    return output_list


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    original_numbers = list(map(int, lines[0]))

    pattern = [0, 1, 0, -1]

    numbers = original_numbers[:]
    for _ in range(100):
        numbers = fft_phase(numbers, pattern)
    print(''.join(map(str, numbers[:8])))

    message_offset = int(''.join(map(str, original_numbers[:7])))
    numbers: List = list(original_numbers * 10000)
    # This is a requirement for the following shortcut to work.
    # The message has to be in the second half of the signal.
    assert message_offset > (len(numbers) / 2)

    for _ in range(100):
        # The last number is always the same for all phases, so we can always
        # fill it in first.
        prev_number = numbers[-1]
        # Go through the signal in reverse order for each number from the end
        # until just before the message begins.
        for i in range(len(numbers) - 1, message_offset - 1, -1):
            # The number in a specific location can easily be calculated as a
            # cumulative sum of all numbers in the previous phase output from
            # the end of the signal to its position ASSUMING it is in the
            # second half of the input signal.
            # This is because the base pattern is stretched so far that the
            # most of the input will be multiplied by 0 (i.e. disregarded
            # completely) and parts of the second half will be multiplied by
            # 1s, which are then added together => a cumulative sum.
            numbers[i] = prev_number
            prev_number = abs(numbers[i] + numbers[i - 1]) % 10
    output_message = ''.join(map(str, numbers[message_offset:message_offset + 8]))
    print(output_message)
