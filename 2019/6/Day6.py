from typing import List, Dict


def parse_orbits(input_lines: List[str]) -> Dict[str, str]:
    return {b: a for a, b in (line.split(')') for line in input_lines)}


def count_total_orbits(orbits: Dict[str, str], orbit_name: str) -> int:
    # This object orbits all objects around it on the path to COM, except itself
    return len(get_path_to_com(orbits, orbit_name)) - 1


def get_path_to_com(orbits: Dict[str, str], src: str) -> List[str]:
    return [src] + (get_path_to_com(orbits, orbits[src]) if src in orbits else [])


def minimum_distance(orbits: Dict[str, str], src: str, dest: str) -> int:
    src_path = get_path_to_com(orbits, src)
    dest_path = get_path_to_com(orbits, dest)

    # Find the first common node between the paths
    first_common = next((a for a in src_path if a in set(dest_path)))
    # The number of orbits between src and dest without their respective starting orbits
    steps = -2 + src_path.index(first_common) + dest_path.index(first_common)

    return steps


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    orbit_map = parse_orbits(lines)
    print(sum(count_total_orbits(orbit_map, o) for o in orbit_map))
    print(minimum_distance(orbit_map, 'YOU', 'SAN'))
