from collections import defaultdict

import util
from typing import List, Tuple, Dict, Set

from IntCode import IntCode
from State import State

DIRECTIONS = {
    'U': ('L', 'R', (0, 1)),
    'D': ('R', 'L', (0, -1)),
    'L': ('D', 'U', (-1, 0)),
    'R': ('U', 'D', (1, 0))
}


def hull_painting_robot(code: List[int], start_white: bool = False) -> Tuple[Set[Tuple[int, int]],
                                                                             Dict[Tuple[int, int], int]]:
    hull = defaultdict(int)
    painted = set()
    position = (0, 0)
    direction = DIRECTIONS['U']

    bot = IntCode(code, [])

    if start_white:
        hull[position] = 1

    while not bot.state == State.HALTED:
        bot.submit_input(hull[position])
        bot.run()
        output = bot.get_output(clear = True)
        color_to_paint = output[0]
        new_direction = output[1]

        if color_to_paint != hull[position]:
            painted.add(position)
        hull[position] = color_to_paint

        direction = DIRECTIONS[direction[new_direction]]
        position = (position[0] + direction[2][0], position[1] + direction[2][1])

    return painted, hull


def print_pattern(painting: Dict[Tuple[int, int], int]):
    xs = set(x for x, _ in painting)
    ys = set(y for _, y in painting)

    for row in reversed(sorted(ys)):
        #print(''.join('█' if painting[(col, row)] else ' ' for col in sorted(xs)))
        for col in sorted(xs):
            print('█' if painting[(col, row)] else ' ', end = '')
        print()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = util.extract_all_numbers(lines[0])

    painted_tiles, _ = hull_painting_robot(program)
    print(len(painted_tiles))

    _, pattern = hull_painting_robot(program, start_white = True)
    print_pattern(pattern)
