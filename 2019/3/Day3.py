from collections import namedtuple, defaultdict
from typing import List, Dict, Tuple

Step = namedtuple('Step', ['direction', 'length'])
Wire = namedtuple('Wire', ['name', 'path'])


DIRECTIONS = {
    'U': (0, 1),
    'D': (0, -1),
    'L': (-1, 0),
    'R': (1, 0)
}


def parse_wire(text: str) -> List[Step]:
    steps = []
    for part in text.split(','):
        steps.append(Step(part[0], int(part[1:])))
    return steps


def place_wires(wire_list: List[Wire]) -> Dict[Tuple, Dict[str, int]]:
    panel: Dict[Tuple, Dict[str, int]] = defaultdict(dict)

    for wire in wire_list:
        position = (0, 0)
        step_counter = 0
        for part in wire.path:
            for _ in range(part.length):
                step_counter += 1
                direction = DIRECTIONS[part.direction]
                position = (position[0] + direction[0], position[1] + direction[1])
                if wire.name not in panel[position]:
                    panel[position][wire.name] = step_counter

    return panel


def find_intersections(panel: Dict[Tuple, Dict[str, int]]):
    intersections = []
    for pos, ws in panel.items():
        if len(ws) > 1:
            intersections.append(pos)

    return intersections


def manhattan_from_origin(position: Tuple[int, int]) -> int:
    return abs(position[0]) + abs(position[1])


def steps_needed(position: Tuple[int, int], panel) -> int:
    return sum(panel[position].values())


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    # w1 = "R8,U5,L5,D3"
    # w2 = "U7,R6,D4,L4"
    # w1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72"
    # w2 = "U62,R66,U55,R34,D71,R55,D58,R83"
    # w1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
    # w2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
    # lines = f"{w1}\n{w2}".split()
    #lines = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83".split()

    wires = [Wire(f"Wire-{idx}", parse_wire(l)) for idx, l in enumerate(lines)]

    jumble = place_wires(wires)

    wire_intersections = find_intersections(jumble)

    sorted_by_distance = sorted(wire_intersections, key = lambda p: manhattan_from_origin(p))
    sorted_by_steps = sorted(wire_intersections, key = lambda p: steps_needed(p, jumble))
    print(manhattan_from_origin(sorted_by_distance[0]))
    print(steps_needed(sorted_by_steps[0], jumble))
