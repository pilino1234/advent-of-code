from collections import defaultdict
from typing import List, Tuple, Dict
from math import gcd, atan2
from operator import itemgetter


def create_map(input_lines: List[str]) -> Dict[Tuple[int, int], bool]:
    asteroid_locations = defaultdict(bool)
    for y_pos, line in enumerate(input_lines):
        for x_pos, char in enumerate(line):
            if char == '#':
                asteroid_locations[(y_pos, x_pos)] = True
            elif char == '.':
                pass
            else:
                raise ValueError(f"Invalid character encountered while parsing asteroid map: '{char}'")
    return asteroid_locations


def can_detect(source: Tuple[int, int], dest: Tuple[int, int],
               locations: Dict[Tuple[int, int], bool]):
    if source == dest:
        return False
    dy = dest[0] - source[0]
    dx = dest[1] - source[1]
    if dy == 0:
        ystep = 0
        xstep = dx / abs(dx)
        upper = abs(dx)
    elif dx == 0:
        xstep = 0
        ystep = dy / abs(dy)
        upper = abs(dy)
    else:
        factor = abs(gcd(dy, dx))
        ystep = dy / factor
        xstep = dx / factor
        upper = factor

    for i in range(1, upper):
        target = (source[0] + ystep*i, source[1] + xstep*i)
        if locations[target]:
            return False
    return True


def find_best_asteroid(locations: Dict[Tuple[int, int], bool]) -> Tuple[Tuple[int, int], int]:
    asteroids = [pos for pos in locations if locations[pos]]
    counts = {pos: len([asteroid for asteroid in asteroids
                        if asteroid != pos
                        and can_detect(pos, asteroid, locations)])
              for pos in asteroids}
    return max(counts.items(), key = itemgetter(1))


def vaporize_asteroids(locations: Dict[Tuple[int, int], bool],
                       position: Tuple[int, int]) -> Tuple[int, int]:
    vaporized = []
    while any(locations[pos] for pos in locations if pos != position):
        asteroids = [pos for pos in locations if locations[pos]]
        angles = []
        for dest in asteroids:
            if dest == position:
                continue
            if can_detect(position, dest, locations):
                dy = dest[0] - position[0]
                dx = dest[1] - position[1]
                angles.append((atan2(dx, dy), dest))

        clockwise_angles = list(map(lambda v: v[1], sorted(angles)[::-1]))
        vaporized.extend(clockwise_angles)
        for dest in clockwise_angles:
            locations[dest] = False

    return vaporized[199]


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    asteroid_map = create_map(lines)

    best_location, count = find_best_asteroid(asteroid_map)
    print(count)

    winning_bet = vaporize_asteroids(asteroid_map, best_location)
    print(winning_bet[1] * 100 + winning_bet[0])
