from typing import List
from itertools import permutations

import util
from IntCode import IntCode


#class Amplifier:
#    def __init__(self, input_lines: List[str], phase_setting: int):
#        self.input_lines = input_lines
#        self.phase_setting = phase_setting
#        self.finished = False
#
#    def _get_program(self):
#        return util.extract_all_numbers(self.input_lines[0])
#
#    def run(self, input_levels: List[int]) -> Optional[List[int]]:
#        if self.finished:
#            return
#        program = self._get_program()
#        try:
#            program, out = util.execute(program, [self.phase_setting] + input_levels)
#        except util.MoreInput as e:
#            return e.args[0]
#        self.finished = True
#        return out
#
#    def __repr__(self):
#        return f"Amplifier(phase={self.phase_setting})"
from State import State


def test_all(input_lines: List[str], low: int, high: int):
    final_out = 0

    program = util.extract_all_numbers(input_lines[0])

    for perm in permutations(range(low, high+1)):
        amps = [IntCode(program, [num]) for num in perm]
        carry_value = 0

        while not all(a.state == State.HALTED for a in amps):
            for idx, amp in enumerate(amps):
                amp.submit_input(carry_value)
                amp.run()
                carry_value = amp.get_output()[-1]

        final_out = max(carry_value, final_out)

    return final_out


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    print(test_all(lines, low = 0, high = 4))
    print(test_all(lines, low = 5, high = 9))
