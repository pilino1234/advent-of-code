from typing import List, Tuple, Dict, Generator


def print_area(area_map: Dict[Tuple[int, int], bool]):
    for y in range(5):
        for x in range(5):
            print('#' if area_map[(y, x)] else ' ', end = '')
        print()


def print_layered_area(area_layers: Dict[int, Dict[Tuple[int, int], bool]]):
    for layer in sorted(area_layers.keys()):
        print(f"Layer {layer}")
        print_area(area_layers[layer])


def parse_grid(input_lines: List[str]) -> Dict[Tuple[int, int], bool]:
    grid = {}
    for y, line in enumerate(input_lines):
        for x, char in enumerate(line):
            grid[(y, x)] = char == '#'
    return grid


def parse_grid_plutonian(input_lines: List[str]) -> Dict[int, Dict[Tuple[int, int], bool]]:
    return {0: parse_grid(input_lines)}


def add_coord(pa: Tuple[int, int], pb: Tuple[int, int]) -> Tuple[int, int]:
    """Helper to add coordinate tuples"""
    return pa[0] + pb[0], pa[1] + pb[1]


def adjacent_cells(position: Tuple[int, int]) -> List[Tuple[int, int]]:
    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    return [add_coord(position, d) for d in directions]


def biodiversity_rating(grid: Dict[Tuple[int, int], bool]) -> int:
    result = 0
    power = 0
    for y in range(5):
        for x in range(5):
            if grid[(y, x)]:
                result += 2 ** power
            power += 1
    return result


def simulate(grid: Dict[Tuple[int, int], bool]):
    while True:
        new_grid = {}
        for pos, infested in grid.items():
            neighbour_data = []
            for nb in adjacent_cells(pos):
                if 0 <= nb[0] < 5 and 0 <= nb[1] < 5:
                    neighbour_data.append(grid[nb])
            if infested:
                # If exactly one infested neighbour, stay infested
                new_grid[pos] = (neighbour_data.count(True) == 1)
            else:
                # Not infested, become infested if 1 or 2 neighbours are
                new_grid[pos] = (neighbour_data.count(True) in (1, 2))
        # print_area(new_grid)
        grid = new_grid
        # yield biodiversity_rating(new_grid)
        yield grid


def simulate_plutonian(input_grid: Dict[int, Dict[Tuple[int, int], bool]]) -> Generator[Dict[int, Dict[Tuple[int, int], bool]], None, None]:
    levels = input_grid

    sides = {
        (1, 2): [(0, x) for x in range(5)],
        (2, 1): [(y, 0) for y in range(5)],
        (2, 3): [(y, 4) for y in range(5)],
        (3, 2): [(4, x) for x in range(5)]
    }

    while True:
        new_levels = {}

        # If the top or bottom layers are not empty, add new empty layers
        level_numbers = list(sorted(levels.keys()))
        if any(levels[min(level_numbers)].values()):
            levels[min(level_numbers) - 1] = {(y, x): False for x in range(5) for y in range(5)}
        if any(levels[max(level_numbers)].values()):
            levels[max(level_numbers) + 1] = {(y, x): False for x in range(5) for y in range(5)}

        for level, grid in levels.items():
            new_level = {(y, x): False for x in range(5) for y in range(5)}
            for pos, infested in grid.items():
                if pos == (2, 2):
                    continue
                neighbour_data = []
                for nb in adjacent_cells(pos):
                    # Centre tile: one level up
                    if nb == (2, 2):
                        if level + 1 in levels:
                            next_level = levels[level + 1]
                            for c in sides[pos]:
                                neighbour_data.append(next_level[c])
                        else:
                            neighbour_data.extend([False] * 5)
                    # Regular neighbours
                    elif 0 <= nb[0] < 5 and 0 <= nb[1] < 5:
                        neighbour_data.append(grid[nb])
                    # Outside neighbours: one level down
                    else:
                        if level - 1 in levels:
                            prev_level = levels[level - 1]
                            if nb[0] < 0:
                                neighbour_data.append(prev_level[(1, 2)])
                            elif nb[0] >= 5:
                                neighbour_data.append(prev_level[(3, 2)])

                            if nb[1] < 0:
                                neighbour_data.append(prev_level[(2, 1)])
                            elif nb[1] >= 5:
                                neighbour_data.append(prev_level[(2, 3)])
                        else:
                            neighbour_data.append(False)
                if infested:
                    # If exactly one infested neighbour, stay infested
                    new_level[pos] = (neighbour_data.count(True) == 1)
                else:
                    # Not infested, become infested if 1 or 2 neighbours are
                    new_level[pos] = (neighbour_data.count(True) in (1, 2))
            new_levels[level] = new_level

        levels = new_levels
        yield levels


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    eris = parse_grid(lines)

    simulation = simulate(eris)
    ratings = {biodiversity_rating(eris)}
    while True:
        eris = next(simulation)
        rating = biodiversity_rating(eris)
        if rating in ratings:
            break
        ratings.add(rating)
    print(f"First biodiversity rating to be reached twice: {rating}")

    # Part 2
    eris = parse_grid_plutonian(lines)
    simulation = simulate_plutonian(eris)
    for minute in range(200):
        eris = next(simulation)

    total = sum(infested for layer in eris.values() for infested in layer.values())
    print(f"Total infested after {minute+1} minutes: {total}")
