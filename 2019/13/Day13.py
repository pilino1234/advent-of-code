from collections import defaultdict
from typing import List, Set, Tuple, Dict

import util
from IntCode import IntCode
from State import State


def paint_output(arcade_output: List[int], show: bool = False) -> Dict[Tuple[int, int], int]:
    output = defaultdict(int)
    score = 0

    for x, y, data in zip(*[iter(arcade_output)] * 3):
        if x == -1 and y == 0:
            score = data
        else:
            output[(x, y)] = data

    if show:
        minx = min(arcade_output[::3])
        maxx = max(arcade_output[::3])
        miny = min(arcade_output[1::3])
        maxy = max(arcade_output[1::3])

        for row in range(maxy+1, miny-1, -1):
            for col in range(minx-1, maxx+1):
                data = output[(col, row)]
                char = ''
                if data == 0:  # Empty
                    char = ' '
                elif data == 1:  # Wall
                    char = '█'
                elif data == 2:  # Block
                    char = '⬜'
                elif data == 3:  # Paddle
                    char = '▁'
                elif data == 4:  # Ball
                    char = '●'
                print(char, end='')
            print()
        print(f"Score: {score}")

    return output


def play_game(code: List[int]):
    game = IntCode(code, [0])
    output = defaultdict(int)
    score = 0
    paddle_x = 0
    ball_x = 0
    while not game.state == State.HALTED:
        game.run()
        arcade_output = game.get_output(clear = True)

        for x, y, data in zip(*[iter(arcade_output)] * 3):
            if x == -1 and y == 0:
                score = data
                continue
            if data == 3:
                paddle_x = x
            elif data == 4:
                ball_x = x
            output[(x, y)] = data

        if ball_x > paddle_x:
            game.submit_input(1)
        elif ball_x < paddle_x:
            game.submit_input(-1)
        else:
            game.submit_input(0)

    return score


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = util.extract_all_numbers(lines[0])
    arcade = IntCode(program, [])
    arcade.run()
    screen = paint_output(arcade.get_output())
    print(sum(tile_id == 2 for tile_id in screen.values()))

    # Play for free
    program[0] = 2
    print(play_game(program))

