from typing import List

import util
from IntCode import IntCode
from State import State


def ascii_print(ascii_data: List[int]):
    for d in ascii_data:
        print(chr(d), end = '')


def ascii_to_int(input_text: str) -> List[int]:
    return list(map(ord, input_text + '\n'))


def play(code: List[int]):
    """Play the game interactively"""
    droid = IntCode(code, [])
    while True:
        droid.run()
        output = droid.get_output(clear = True)
        ascii_print(output)

        if droid.state == State.HALTED:
            break

        inp = input(">>> ")
        for char in ascii_to_int(inp):
            droid.submit_input(char)


def auto_solve(code: List[int]):
    # Pick up first item
    first_item = [
        'east',
        'east',
        'south',
        'take monolith'
    ]

    # Go to and pick up second item
    second_item = [
        'north',
        'west',
        'north',
        'north',
        'take planetoid'
    ]

    # Third item
    third_item = [
        'west',
        'south',
        'south',
        'take fuel cell'
    ]

    # Fourth item
    fourth_item = [
        'north',
        'north',
        'east',
        'east',
        'south',
        'west',
        'north',
        'take astrolabe'
    ]

    # Walk to santa
    walk_to_gate = [
        'west'
    ]

    droid = IntCode(code, [])
    for step in first_item + second_item + third_item + fourth_item + walk_to_gate:
        command = ascii_to_int(step)
        for num in command:
            droid.submit_input(num)

    droid.run()
    # Clear the output
    droid.get_output(clear = True)

    step_through_gate = [
        'north'
    ]

    for step in step_through_gate:
        command = ascii_to_int(step)
        for num in command:
            droid.submit_input(num)

    droid.run()
    output = droid.get_output(clear = True)
    str_output = ''.join(map(chr, output))
    [password] = util.extract_all_numbers(str_output)
    print(f"Password for the main airlock: {password}")


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = util.extract_all_numbers(lines[0])

    # Play interactively
    # play(program)

    # Just get the password
    auto_solve(program)
