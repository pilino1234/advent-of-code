from util import to_ints


def fuel_required(weight: int) -> int:
    return max((weight // 3) - 2, 0)


def total_fuel_required(weight: int) -> int:
    if weight <= 0:
        return 0
    needed_fuel = fuel_required(weight)
    return needed_fuel + total_fuel_required(needed_fuel)


if __name__ == '__main__':
    with open("input.txt") as file:
        weights = to_ints([line.strip() for line in file])

    assert fuel_required(12) == 2
    assert fuel_required(14) == 2
    assert fuel_required(1969) == 654
    assert fuel_required(100756) == 33583

    assert total_fuel_required(14) == 2
    assert total_fuel_required(1969) == 966
    assert total_fuel_required(100756) == 50346

    print(sum(fuel_required(w) for w in weights))
    print(sum(total_fuel_required(w) for w in weights))
