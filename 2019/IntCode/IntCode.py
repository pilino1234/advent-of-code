from typing import List, Dict, Tuple
from collections import namedtuple, defaultdict

import util
from Mode import Mode
from State import State

Instruction = namedtuple('Instruction', ['opcode', 'name', 'length', 'src_params', 'dest_params', 'func'])


class Memory(defaultdict):
    def __init__(self, data: List[int]):
        super(Memory, self).__init__(int)

        for idx, value in enumerate(data):
            self[idx] = value

    def __getitem__(self, item: int) -> int:
        if not isinstance(item, int):
            raise TypeError(f"Memory address needs to be a positive integer, not '{item}'")
        elif item < 0:
            raise IndexError(f"Trying to read from invalid memory address '{item}'")
        return super(Memory, self).__getitem__(item)

    def __setitem__(self, key: int, value: int):
        if not isinstance(key, int):
            raise TypeError(f"Memory address needs to be a positive integer, not '{key}'")
        elif key < 0:
            raise IndexError(f"Trying to write to invalid memory address '{key}'")

        if not isinstance(value, int):
            raise ValueError(f"IntCode memory can only store integers, not '{value}'")
        super(Memory, self).__setitem__(key, value)

    def __str__(self):
        return f"Memory: {list(super().values())}"


class IntCode:
    def __init__(self, code: List[int], inputs: List[int]):
        self.instructions = [
            Instruction(opcode = 1, name = 'add', length = 4, src_params = 2, dest_params = 1, func = self.__add),
            Instruction(opcode = 2, name = 'mul', length = 4, src_params = 2, dest_params = 1, func = self.__mul),
            Instruction(opcode = 3, name = 'in', length = 2, src_params = 0, dest_params = 1, func = self.__in),
            Instruction(opcode = 4, name = 'out', length = 2, src_params = 1, dest_params = 0, func = self.__out),
            Instruction(opcode = 5, name = 'jit', length = 3, src_params = 2, dest_params = 0, func = self.__jit),
            Instruction(opcode = 6, name = 'jif', length = 3, src_params = 2, dest_params = 0, func = self.__jif),
            Instruction(opcode = 7, name = 'lt', length = 4, src_params = 2, dest_params = 1, func = self.__lt),
            Instruction(opcode = 8, name = 'eq', length = 4, src_params = 2, dest_params = 1, func = self.__eq),
            Instruction(opcode = 9, name = 'rel', length = 2, src_params = 1, dest_params = 0, func = self.__rel),
            Instruction(opcode = 99, name = 'hlt', length = 0, src_params = 0, dest_params = 0, func = self.__halt)
        ]

        self.memory: Memory = Memory(code)
        self._opcodes: Dict[int, Instruction] = self._load_instructions()
        self._stdin: List[int] = []
        for input_ in inputs:
            self.submit_input(input_)
        self._stdout: List[int] = []
        self.pc: int = 0
        self.relative_base: int = 0
        self.state: State = State.NOT_RUNNING

    def _load_instructions(self) -> Dict[int, Instruction]:
        return {ins.opcode: ins for ins in self.instructions}

    def submit_input(self, data: int):
        if not isinstance(data, int):
            raise TypeError(f"Attempted to submit non-integer input, was type '{type(data)}'")
        self._stdin.append(data)

    def get_output(self, clear: bool = False) -> List[int]:
        data = self._stdout[:]
        if clear:
            self._stdout = []
        return data

    def _fetch(self) -> Tuple[Instruction, List[Mode]]:
        op_data = str(self.memory[self.pc]).zfill(5)
        opcode = int(op_data[3:])
        try:
            operation: Instruction = self._opcodes[opcode]
        except KeyError:
            print(f"Could not decode instruction with opcode '{opcode}'")
            raise
        modes = []
        for i in range(operation.src_params + operation.dest_params):
            raw_mode = int(op_data[2 - i])
            modes.append(Mode(raw_mode))

        return operation, modes

    def _execute(self):
        op, modes = self._fetch()
        self._execute_op(op, modes)

    def _get_parameter(self, mode: Mode, parameter_number: int, address: bool = False) -> int:
        parameter_value = self.memory[self.pc + parameter_number]
        if mode is Mode.POSITION:
            return parameter_value if address else self.memory[parameter_value]
        elif mode is Mode.IMMEDIATE:
            if address:
                raise RuntimeError("Attempting to use parameter address in immediate mode")
            return parameter_value
        elif mode is Mode.RELATIVE:
            relative_value = self.relative_base + parameter_value
            return relative_value if address else self.memory[relative_value]
        else:
            raise ValueError(f"Invalid parameter mode '{mode}'")

    def _execute_op(self, op: Instruction, modes: List[Mode]):
        parameter_values = []
        for num, mode in enumerate(modes, start = 1):
            parameter_values.append(self._get_parameter(mode, num, address = len(parameter_values) >= op.src_params))

        op.func(op, *parameter_values)

    def run(self):
        if self.state == State.NOT_RUNNING:
            self.state = State.RUNNING
        elif self.state == State.WAITING:
            if self._stdin:
                self.state = State.RUNNING

        while self.state == State.RUNNING:
            self._execute()

    # Operator implementations
    def __add(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        self.memory[args[2]] = args[0] + args[1]
        self.pc += op.length

    def __mul(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        self.memory[args[2]] = args[0] * args[1]
        self.pc += op.length

    def __in(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        if self._stdin:
            read_data = self._stdin.pop(0)
        else:
            self.state = State.WAITING
            return
        self.memory[args[0]] = read_data
        self.pc += op.length

    def __out(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        self._stdout.append(args[0])
        self.pc += op.length

    def __jit(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        if args[0] != 0:
            self.pc = args[1]
        else:
            self.pc += op.length

    def __jif(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        if args[0] == 0:
            self.pc = args[1]
        else:
            self.pc += op.length

    def __lt(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        self.memory[args[2]] = 1 if args[0] < args[1] else 0
        self.pc += op.length

    def __eq(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        self.memory[args[2]] = 1 if args[0] == args[1] else 0
        self.pc += op.length

    def __rel(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        self.relative_base += args[0]
        self.pc += op.length

    def __halt(self, op: Instruction, *args):
        assert len(args) == op.src_params + op.dest_params
        self.state = State.HALTED


if __name__ == '__main__':
    computer = IntCode('1,1,1,1')
    print(computer.memory)
    computer.run()
