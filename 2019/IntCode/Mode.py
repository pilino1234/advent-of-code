from enum import Enum, auto


class Mode(Enum):
    POSITION = 0
    IMMEDIATE = 1
    RELATIVE = 2
