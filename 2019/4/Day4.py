from collections import Counter


def valid(pwd: int, strict_adjacent: bool = False) -> bool:
    if not (1e5 < pwd < 1e6):  # Check length
        return False

    # We can do this because the digits have to be increasing
    counts = Counter(str(pwd))
    if strict_adjacent:
        adjacent = any(count == 2 for count in counts.values())
    else:
        adjacent = any(count > 1 for count in counts.values())

    increasing = str(pwd) == "".join(sorted(str(pwd)))
    if not adjacent or not increasing:
        return False

    return True


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    a, b = map(int, lines[0].split('-'))

    assert valid(111111) is True
    assert valid(223450) is False
    assert valid(123789) is False
    assert valid(112233, True) is True
    assert valid(123444, True) is False
    assert valid(111122, True) is True

    print(sum(valid(pwd) for pwd in range(a, b)))
    print(sum(valid(pwd, True) for pwd in range(a, b)))

