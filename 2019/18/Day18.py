import string
from queue import Queue
from typing import List, Tuple, Dict


def add_coord(pa: Tuple[int, int], pb: Tuple[int, int]) -> Tuple[int, int]:
    """Helper to add coordinate tuples"""
    return pa[0] + pb[0], pa[1] + pb[1]


def parse_area(input_lines: List[str]) -> Tuple[Dict[Tuple[int, int], str], List[Tuple[int, int]]]:
    area_map = {}
    start = []
    for y, row in enumerate(input_lines):
        for x, col in enumerate(row):
            area_map[(y, x)] = col
            if col == '@':
                start.append((y, x))
    assert start is not None, "Input map must contain starting position marked with '@'"
    return area_map, start


def print_area(area_map: Dict[Tuple[int, int], str]):
    ys = [key[0] for key in area_map]
    xs = [key[1] for key in area_map]
    miny = min(ys)
    maxy = max(ys)
    minx = min(xs)
    maxx = max(xs)

    for y in range(miny, maxy+1):
        for x in range(minx, maxx+1):
            print(area[(y, x)], end = '')
        print()


def modify_area(area_map: Dict[Tuple[int, int], str]):
    # For part 2, replace starting area of map to unleash 4 robots at once:
    #     #   #      #@#@#
    #     # @ #  ->  #####
    #     #   #      #@#@#
    start = next(pos for pos, data in area_map.items() if data == '@')
    new_starts = []
    area_map[start] = '#'
    for d in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
        area_map[add_coord(start, d)] = '#'
    for d in [(1, 1), (-1, 1), (1, -1), (-1, -1)]:
        pos = add_coord(start, d)
        area_map[pos] = '@'
        new_starts.append(pos)
    return new_starts


def reachable_keys(maze: Dict[Tuple[int, int], str], start: Tuple[int, int],
                   found_keys: str) -> Dict[str, Tuple[int, Tuple[int, int]]]:
    # Use BFS to find which keys are reachable from the current location,
    # including their location and shortest distance to them.
    q = Queue()
    q.put(start)
    seen = {start: 0}
    keys = {}

    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]

    while q.qsize():
        starting_pos = q.get()

        for delta in directions:
            next_pos = add_coord(starting_pos, delta)

            if next_pos not in maze:
                continue
            elif next_pos in seen:
                continue

            char = maze[next_pos]
            if char == '#':
                continue

            seen[next_pos] = seen[starting_pos] + 1
            # Encountering a gate that we do not have a key for
            if char in string.ascii_uppercase and char.lower() not in found_keys:
                continue
            # Encountering a new key
            elif char in string.ascii_lowercase and char not in found_keys:
                keys[char] = (seen[next_pos], next_pos)
            else:
                q.put(next_pos)
    #print(f"BFS returned: {keys}")
    return keys


# Memoization for pairs of robot positions and currently found keys.
seen_states = {}


def walk(maze: Dict[Tuple[int, int], str], starting_points: Tuple[Tuple[int, int]],
         found_keys: str) -> int:
    sorted_keys = ''.join(sorted(found_keys))
    # Memoization; if we have seen this state before, return the cached result immediately.
    if (starting_points, sorted_keys) in seen_states:
        return seen_states[starting_points, sorted_keys]

#    if len(seen_states) % 1 == 0:
#        print(sorted_keys)

    # Check which keys we can reach from our current location
    keys = {}
    # For each robot,
    for robot, robot_pos in enumerate(starting_points):
        # For each key that this robot can reach,
        for key_name, (dist, position) in reachable_keys(maze, robot_pos, found_keys).items():
            # Store the distance to it, its location, and which robot reached it
            keys[key_name] = (dist, position, robot)

    # If no more keys can be found, the recursion is complete
    if len(keys) == 0:
        answer = 0
    else:
        # Otherwise, explore the possible routes recursively
        possible = []
        # For each possible route
        for key_name, (dist, pos, robot) in keys.items():
            # Create new tuple of next starting positions
            next_starts = tuple(pos if robot == robot_id else position
                                for robot_id, position in enumerate(starting_points))
            # Calculate the distance that it will take to find all remaining keys
            possible.append(dist + walk(maze, next_starts, found_keys + key_name))
        # The shortest possible path is best (i.e. the answer)
        answer = min(possible)
    seen_states[starting_points, sorted_keys] = answer
    return answer


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    area, starting_points = parse_area(lines)
    print_area(area)

    if len(starting_points) == 1:
        print(walk(area, tuple(starting_points), ''))

    # Part 2:
        starting_points = modify_area(area)
        print(starting_points)

    print(walk(area, tuple(starting_points), ''))



