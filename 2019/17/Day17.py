from collections import defaultdict
from typing import List, Tuple, Dict

import util
from IntCode import IntCode


def create_grid(output_data: List[int]) -> Dict[Tuple[int, int], int]:
    grid = defaultdict(int)
    row, col = 0, 0

    for d in output_data:
        grid[(col, row)] = d
        if d == 10:
            row += 1
            col = 0
        else:
            col += 1
    return grid


def draw_output_plain(output_data: List[int]):
    for d in output_data:
        print(chr(d), end='')


def find_intersections(grid: Dict[Tuple[int, int], int]):
    found_intersections = []
    # To make sure we don't change the dimensions of the original grid
    grid_copy = grid.copy()
    for col, row in grid:
        data = grid_copy[(col, row)]
        if data != 35:
            continue
        neighbouring_scaffolding = 0
        for dy in (-1, 1):
            if grid_copy[(col + dy, row)] == 35:
                neighbouring_scaffolding += 1
        for dx in (-1, 1):
            if grid_copy[(col, row + dx)] == 35:
                neighbouring_scaffolding += 1
        if neighbouring_scaffolding == 4:
            found_intersections.append((col, row))
    return found_intersections


def calculate_alignment_parameter(positions: List[Tuple[int, int]]):
    return sum(x * y for y, x in positions)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    code = util.extract_all_numbers(lines[0])

    computer = IntCode(code, [])
    computer.run()
    output = computer.get_output()
    scaffolding = create_grid(output)
    # draw_output_plain(output)

    intersections = find_intersections(scaffolding)

    print(calculate_alignment_parameter(intersections))

    # Part 2
    code[0] = 2
    computer = IntCode(code, [])

    # Movement code
    main_movement = "A,B,B,C,B,C,B,C,A,A\n"
    movement_a = "L,6,R,8,L,4,R,8,L,12\n"
    movement_b = "L,12,R,10,L,4\n"
    movement_c = "L,12,L,6,L,4,L,4\n"
    visual_feed = "n\n"
    for char in map(ord, main_movement + movement_a + movement_b + movement_c + visual_feed):
        computer.submit_input(char)

    computer.run()

    *graphics, space_dust = computer.get_output()

    # draw_output_plain(graphics)
    print(space_dust)
