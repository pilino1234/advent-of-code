from typing import List

from IntCode import IntCode
from util import extract_all_numbers


def execute(instructions: List[int], inputs: List[int]) -> List[int]:
    comp = IntCode(instructions, inputs)
    comp.run()
    return comp.get_output()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = extract_all_numbers(lines[0])

    assert execute([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], [8])[-1] == 1
    assert execute([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], [0])[-1] == 0
    assert execute([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], [7])[-1] == 1
    assert execute([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], [9])[-1] == 0
    assert execute([3, 3, 1108, -1, 8, 3, 4, 3, 99], [8])[-1] == 1
    assert execute([3, 3, 1108, -1, 8, 3, 4, 3, 99], [0])[-1] == 0
    assert execute([3, 3, 1107, -1, 8, 3, 4, 3, 99], [5])[-1] == 1
    assert execute([3, 3, 1107, -1, 8, 3, 4, 3, 99], [9])[-1] == 0
    assert execute([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], [5])[-1] == 1
    assert execute([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], [0])[-1] == 0
    assert execute([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], [5])[-1] == 1
    assert execute([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], [0])[-1] == 0

    test = extract_all_numbers(
        "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,"
        "1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,"
        "104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
    )
    assert execute(test, [5])[-1] == 999
    assert execute(test, [8])[-1] == 1000
    assert execute(test, [10])[-1] == 1001

    output = execute(program, [1])
    print(output[-1])

    output = execute(program, [5])
    print(output[-1])
