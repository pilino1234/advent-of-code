import util
from IntCode import IntCode


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = util.extract_all_numbers(lines[0])

    computer = IntCode(program, [1])
    computer.run()
    print(computer.get_output()[0])

    computer = IntCode(program, [2])
    computer.run()
    print(computer.get_output()[0])



