from typing import List, Optional

import util
from IntCode import IntCode


def ascii_print(ascii_data: List[int]):
    for d in ascii_data:
        print(chr(d), end = '')


def preprocess_script(script: List[str], run: bool = False) -> Optional[List[int]]:
    if len(script) > 14:
        raise ValueError("Springscript program length may not exceed 15 commands "
                         f"including WALK or RUN, got {len(script) + 1}.")
    processed = []
    for line in script:
        line = line.strip() + '\n'
        for char in map(ord, line):
            processed.append(char)
    end = "RUN\n" if run else "WALK\n"
    for char in map(ord, end):
        processed.append(char)
    return processed


def spring(intcode: List[int], script: List[int]):
    droid = IntCode(intcode, [])
    droid.run()
    for data in script:
        droid.submit_input(data)
    droid.run()
    return droid.get_output()


def run_springdroid(intcode: List[int], script: List[str], run: bool = False):
    output = spring(intcode, preprocess_script(script, run = run))

    if all(0 <= d <= 0x10ffff for d in output):
        ascii_print(output)
    else:
        # ascii_print(output[:-1])
        print(output[-1])


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]
    program = util.extract_all_numbers(lines[0])

    # Jump if any of the next three are a hole but D is land
    # (!A | !B | !C) & D
    part1 = [
        "NOT A J",
        "NOT B T",
        "OR T J",
        "NOT C T",
        "OR T J",
        "AND D J"
    ]

    # Jump if Part 1 but also if we can keep walking or jumping
    # immediately after landing.
    # Part1 & (E | H)
    part2 = part1 + [
        "NOT E T",
        "NOT T T",
        "OR H T",
        "AND T J"
    ]

    run_springdroid(program, part1)
    run_springdroid(program, part2, run = True)
