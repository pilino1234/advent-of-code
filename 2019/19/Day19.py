import time
from collections import defaultdict
from typing import Tuple

import util
from IntCode import IntCode


def add_coord(pa: Tuple[int, int], pb: Tuple[int, int]) -> Tuple[int, int]:
    """Helper to add coordinate tuples"""
    return pa[0] + pb[0], pa[1] + pb[1]


def print_beam(area_map):
    ys = [key[0] for key in area_map]
    xs = [key[1] for key in area_map]
    miny = min(ys)
    maxy = max(ys)
    minx = min(xs)
    maxx = max(xs)

    for y in range(miny, maxy+1):
        for x in range(minx, maxx+1):
            print('#' if area_map[(y, x)] else '.', end = '')
        print()


def is_in_beam(x_pos: int, y_pos: int):
    comp = IntCode(program, [x_pos, y_pos])
    comp.run()
    return comp.get_output(clear = True) == [1]


def trace_left_edge():
    _y = 0
    _x = 0
    limit_counter = 0
    while True:
        if is_in_beam(_x, _y):
            yield _x, _y
            _y += 1
        else:
            if _y < 5:
                limit_counter += 1
                if limit_counter > 5:
                    _y += 1
                    _x = 0
                    limit_counter = 0
                    continue
            _x += 1


def beam_locations(distance: int):
    beam = defaultdict(bool)
    for x_pos in range(distance):
        for y_pos in range(distance):
            beam[(y_pos, x_pos)] = is_in_beam(x_pos, y_pos)
    # print_beam(beam)
    return beam


def fit_square(square_size: int):
    y = 0
    left_edge = trace_left_edge()
    while y < square_size:  # Trace until we actually see the beam
        _, y = next(left_edge)
    while True:
        x, y = next(left_edge)
        opposite_corner = add_coord((x, y), (square_size - 1, -(square_size - 1)))
        if is_in_beam(*opposite_corner):
            break
    # Return coordinates of the top-left corner of the square
    return x, y - (square_size - 1)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    program = util.extract_all_numbers(lines[0])

    # Part 1
    start1 = time.time()
    beam_range = beam_locations(50)
    print(f"Part 1: {sum(i for i in beam_range.values())}")
    end1 = time.time()

    # Part 2
    start2 = time.time()
    result_x, result_y = fit_square(100)
    print(f"Part 2: {result_x * 10000 + result_y}")
    end2 = time.time()

    print(f"Time (part 1): {end1 - start1:.3f} seconds")
    print(f"Time (part 2): {end2 - start2:.3f} seconds")
