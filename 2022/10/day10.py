from collections import defaultdict


def run(program):
    pc = 0
    cycle_counter = 1
    register_x = 1
    started = False
    target_counter = 1

    part1_value = 0

    frame_buffer = defaultdict(bool)

    while True:
        if cycle_counter == target_counter:
            if started:
                # Finish instruction
                ins = program[pc]

                if ins == "noop":
                    pass
                elif ins.startswith("addx"):
                    arg = int(ins.split()[1])
                    register_x += arg
                pc += 1

            if pc >= len(program):
                break

            # Start next instruction
            ins = program[pc]

            if ins == "noop":
                target_counter = cycle_counter + 1
            elif ins.startswith("addx"):
                target_counter = cycle_counter + 2
            started = True

        if cycle_counter % 40 == 20:
            part1_value += cycle_counter * register_x

        # Draw to crt
        # This part is 0-indexed though
        row, col = divmod(cycle_counter-1, 40)
        sprite_visible = abs(col - register_x) <= 1
        frame_buffer[(row, col)] = sprite_visible

        cycle_counter += 1

    return part1_value, frame_buffer


def draw_crt(frame_buffer: dict[tuple[int, int], bool]):
    ys = [p[0] for p in frame_buffer]
    xs = [p[1] for p in frame_buffer]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    for row in range(miny, maxy + 1):
        for col in range(minx, maxx + 1):
            print("█" if frame_buffer[(row, col)] else " ", end='')
        print()
    print()


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    p1, p2 = run(lines)

    print(f"Part 1: {p1}")
    print("Part 2:")
    draw_crt(p2)

