import re


def parse(line_data: list[str]) -> dict:
    pattern = re.compile(
        r"Sensor at x=(?P<sx>-?[0-9]+), y=(?P<sy>-?[0-9]+): closest beacon is at x=(?P<bx>-?[0-9]+), y=(?P<by>-?[0-9]+)"
    )

    closest = {}

    for line in line_data:
        sx, sy, bx, by = map(int, pattern.match(line).groups())
        closest[(sx, sy)] = (bx, by)

    return closest


def manhattan_distance(p: tuple[int, int], q: tuple[int, int]) -> int:
    return abs(p[0] - q[0]) + abs(p[1] - q[1])


def p1(closest: dict[tuple[int, int], tuple[int, int]], y_line: int) -> int:
    occupied_xs = set()

    for sensor_pos, beacon_pos in closest.items():
        sensor_range = manhattan_distance(sensor_pos, beacon_pos)
        sensor_x = sensor_pos[0]
        sensor_y = sensor_pos[1]
        beacon_x = beacon_pos[0]
        beacon_y = beacon_pos[1]
        # if abs(sensor_y - y_line):

        distance_from_y_line = abs(sensor_y - y_line)
        if distance_from_y_line > sensor_range:
            continue

        # If radius of sensor reaches target line, calculate width of range at this distance
        remaining_radius_at_yline = sensor_range - distance_from_y_line

        occupied_xs.update(set(range(sensor_x - remaining_radius_at_yline, sensor_x + remaining_radius_at_yline + 1)))

        # Discard beacons on target line
        if beacon_y == y_line:
            occupied_xs.discard(beacon_x)

    return len(occupied_xs)


def p2(closest: dict[tuple[int, int], tuple[int, int]], area_size: int) -> int:
    radius = {sensor_pos: manhattan_distance(sensor_pos, beacon_pos) for sensor_pos, beacon_pos in closest.items()}

    pos_intercepts = set()
    neg_intercepts = set()
    for (sensor_x, sensor_y), r in radius.items():
        # Axis intercepts of line segments of scanner area boundaries
        pos_intercepts.add(sensor_y - sensor_x + r + 1)
        pos_intercepts.add(sensor_y - sensor_x - r - 1)
        neg_intercepts.add(sensor_x + sensor_y + r + 1)
        neg_intercepts.add(sensor_x + sensor_y - r - 1)

    for a in pos_intercepts:
        for b in neg_intercepts:
            intersection_point = ((b - a) // 2, (a + b) // 2)
            # Check if intersection point is within required range
            if all(0 < coord < area_size for coord in intersection_point):
                # And that it is not within reach of any scanner
                if all(manhattan_distance(intersection_point, scanner) > radius[scanner] for scanner in radius.keys()):
                    return area_size * intersection_point[0] + intersection_point[1]


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data, y_line=2_000_000)}")
    print(f"Part 2: {p2(data, area_size=4_000_000)}")
