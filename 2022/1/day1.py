def parse(data):
    return [list(map(int, items.split("\n"))) for items in data]


def p2(calories):
    sums = [sum(elf) for elf in calories]
    sums.sort(reverse=True)
    return sum(sums[0:3])


if __name__ == '__main__':
    with open("input.txt") as file:
        data_groups = file.read().strip().split("\n\n")

    elf_calories = parse(data_groups)

    print(f"Part 1: {max(sum(elf) for elf in elf_calories)}")
    print(f"Part 2: {p2(elf_calories)}")
