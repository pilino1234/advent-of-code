from typing import List


SCORES = {
    "A": {  # Rock
        "X": 3,  # Rock
        "Y": 6,  # Paper
        "Z": 0,  # Scissors
    },
    "B": {  # Paper
        "X": 0,  # Rock
        "Y": 3,  # Paper
        "Z": 6,  # Scissors
    },
    "C": {  # Scissors
        "X": 6,  # Rock
        "Y": 0,  # Paper
        "Z": 3,  # Scissors
    },
}


SHAPE_POINTS = {
    "X": 1,  # Rock
    "Y": 2,  # Paper
    "Z": 3,  # Scissors
}


SCORES2 = {
    "A": {  # Rock
        "X": 0 + 3,  # Lose
        "Y": 3 + 1,  # Draw
        "Z": 6 + 2,  # Win
    },
    "B": {  # Paper
        "X": 0 + 1,  # Lose
        "Y": 3 + 2,  # Draw
        "Z": 6 + 3,  # Win
    },
    "C": {  # Scissors
        "X": 0 + 2,  # Lose
        "Y": 3 + 3,  # Draw
        "Z": 6 + 1,  # Win
    },
}


def play_round(move1, move2):
    return SCORES[move1][move2] + SHAPE_POINTS[move2]


def p1(rounds: List[str]):
    total_score = 0
    for round_ in rounds:
        move1, move2 = round_.split()
        total_score += play_round(move1, move2)
    return total_score


def p2(rounds: List[str]):
    total_score = 0
    for round_ in rounds:
        move1, outcome = round_.split()
        total_score += SCORES2[move1][outcome]
    return total_score


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    # data = parse(rounds)

    print(f"Part 1: {p1(lines)}")
    print(f"Part 2: {p2(lines)}")
