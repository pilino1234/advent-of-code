up = lambda p: (p[0], p[1] + 1)
down = lambda p: (p[0], p[1] - 1)
left = lambda p: (p[0] - 1, p[1])
right = lambda p: (p[0] + 1, p[1])

MOVES = {
    "U": up,
    "D": down,
    "L": left,
    "R": right,
}

neighbours9 = ((0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1), (0, 0))


def nb9(pos: tuple[int, int]) -> list[tuple[int, int]]:
    return [(pos[0] + dx, pos[1] + dy) for dx, dy in neighbours9]


def update_tail(head: tuple[int, int], tail: tuple[int, int]) -> tuple[int, int]:
    if tail in nb9(head):
        # No movement needed
        return tail

    dx = head[0] - tail[0]
    dy = head[1] - tail[1]

    if dx > 0:
        tail = right(tail)
    elif dx < 0:
        tail = left(tail)

    if dy > 0:
        tail = up(tail)
    elif dy < 0:
        tail = down(tail)

    return tail


def print_map(head: tuple[int, int], tails: list[tuple[int, int]]):
    xs = [0] + [head[0]] + [p[0] for p in tails]
    ys = [0] + [head[1]] + [p[1] for p in tails]
    minx = min(xs)
    maxx = max(xs)
    miny = min(ys)
    maxy = max(ys)

    for row in range(maxy + 1, miny - 2, -1):
        for col in range(minx - 1, maxx + 2):
            if (col, row) == head:
                char = "H"
            elif (col, row) in tails:
                char = tails.index((col, row)) + 1
            elif (col, row) == (0, 0):
                char = "s"
            else:
                char = "."
            print(char, end='')
        print()
    print()


def simulate_ropes(moves, knots: int = 1):
    head = (0, 0)
    knots = [(0, 0)] * knots

    seen_tail_positions = {knots[-1]}

    for move in moves:
        direction, steps = move.split()

        for _ in range(int(steps)):
            head = MOVES[direction](head)
            prev = head
            for idx, knot in enumerate(knots):
                knots[idx] = update_tail(prev, knot)
                prev = knots[idx]

            seen_tail_positions.add(knots[-1])

    return len(seen_tail_positions)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    print(f"Part 1: {simulate_ropes(lines, knots=1)}")
    print(f"Part 2: {simulate_ropes(lines, knots=9)}")
