def parse(line_data) -> list[tuple[range, range]]:
    parsed = []
    for line in line_data:
        elf1, elf2 = line.split(",")
        range1 = range(*map(int, elf1.split("-")))
        range2 = range(*map(int, elf2.split("-")))
        parsed.append((range1, range2))
    return parsed


def p1(ranges: list[tuple[range, range]]):
    counter = 0
    for e1, e2 in ranges:
        if (e1.start <= e2.start and e2.stop <= e1.stop) or (e2.start <= e1.start and e1.stop <= e2.stop):
            counter += 1
    return counter


def p2(ranges: list[tuple[range, range]]):
    counter = 0
    for e1, e2 in ranges:
        if (e2.start <= e1.start <= e2.stop) or (e1.start <= e2.start <= e1.stop):
            counter += 1
    return counter


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
