from collections import defaultdict


def parse(line_data):
    trees = {}

    for y, row in enumerate(line_data):
        for x, tree_height in enumerate(row):
            trees[(x, y)] = int(tree_height)

    return trees


up = lambda p: (p[0], p[1] - 1)
down = lambda p: (p[0], p[1] + 1)
left = lambda p: (p[0] - 1, p[1])
right = lambda p: (p[0] + 1, p[1])


def check_visible(delta, trees, tree_pos, tree_height):
    tree_visible = True
    target = delta(tree_pos)
    while True:
        try:
            target_height = trees[target]
            if target_height >= tree_height:
                tree_visible = False
                break
            target = delta(target)
        except KeyError:
            # Reached the edge
            break

    return tree_visible


def p1(trees, width, height):
    visible = defaultdict(bool)

    for pos, tree_height in trees.items():
        if pos[0] == 0 or pos[0] == width-1 or pos[1] == 0 or pos[1] == height-1:
            visible[pos] = True
        else:
            visible[pos] |= check_visible(up, trees, pos, tree_height)
            visible[pos] |= check_visible(down, trees, pos, tree_height)
            visible[pos] |= check_visible(left, trees, pos, tree_height)
            visible[pos] |= check_visible(right, trees, pos, tree_height)

    return sum(visible.values())


def viewing_distance(delta, trees, tree_pos, tree_height):
    distance = 0
    target = delta(tree_pos)
    while True:
        try:
            target_height = trees[target]
            if target_height >= tree_height:
                distance += 1
                break
            target = delta(target)
            distance += 1
        except KeyError:
            # Reached the edge
            break

    return distance


def p2(trees, width, height):
    scenic_scores = defaultdict(lambda: 1)

    for pos, tree_height in trees.items():
        if pos[0] == 0 or pos[0] == width-1 or pos[1] == 0 or pos[1] == height-1:
            scenic_scores[pos] = 0
        else:
            scenic_scores[pos] *= viewing_distance(up, trees, pos, tree_height)
            scenic_scores[pos] *= viewing_distance(down, trees, pos, tree_height)
            scenic_scores[pos] *= viewing_distance(left, trees, pos, tree_height)
            scenic_scores[pos] *= viewing_distance(right, trees, pos, tree_height)

    return max(scenic_scores.values())


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(data, len(lines[0]), len(lines))}")
    print(f"Part 2: {p2(data, len(lines[0]), len(lines))}")
