from typing import Generator

import networkx



def nb4(pos: tuple[int, int]) -> Generator[tuple[int, int], None, None]:
    for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
        yield pos[0] + dx, pos[1] + dy


def parse(line_data: list[str]):
    start = None
    end = None
    heightmap = {}

    for row, line in enumerate(line_data):
        for col, char in enumerate(line):
            if char == "S":
                start = (row, col)
                heightmap[(row, col)] = ord("a")
                continue
            elif char == "E":
                end = (row, col)
                heightmap[(row, col)] = ord("z")
                continue
            else:
                heightmap[(row, col)] = ord(char)

    graph = networkx.DiGraph()

    for pos, elevation in heightmap.items():
        for nb in nb4(pos):
            try:
                nb_elev = heightmap[nb]
                if nb_elev - elevation <= 1:
                    graph.add_edge(pos, nb)
                    graph.nodes[pos]["elevation"] = elevation
                    graph.nodes[nb]["elevation"] = nb_elev
            except KeyError:
                continue

    return graph, start, end


def p1(graph: networkx.DiGraph, start, end):
    return networkx.shortest_path_length(graph, start, end)


def p2(graph: networkx.DiGraph, _, end):
    starting_points = {node[0] for node in graph.nodes(data=True) if node[1]["elevation"] == ord("a")}

    path_lengths = []
    for start in starting_points:
        try:
            path_lengths.append(networkx.shortest_path_length(graph, start, end))
        except networkx.exception.NetworkXNoPath:
            continue

    return min(path_lengths)


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    data = parse(lines)

    print(f"Part 1: {p1(*data)}")
    print(f"Part 2: {p2(*data)}")
