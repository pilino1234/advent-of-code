from typing import Sequence


def sliding_window(seq: Sequence, n: int = 3):
    return (seq[idx: idx + n] for idx in range(0, len(seq) - n))


def process_buffer(buffer: str, n_unique: int = 4):
    for idx, win in enumerate(sliding_window(buffer, n_unique)):
        if len(set(win)) == n_unique:
            return idx + n_unique


if __name__ == '__main__':
    with open("input.txt") as file:
        line = file.readline().strip()

    assert process_buffer("mjqjpqmgbljsphdztnvjfqwrcgsmlb") == 7
    assert process_buffer("bvwbjplbgvbhsrlpgdmjqwftvncz") == 5
    assert process_buffer("nppdvjthqldpwncqszvftbrmjlhg") == 6
    assert process_buffer("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg") == 10
    assert process_buffer("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw") == 11

    print(f"Part 1: {process_buffer(line)}")

    assert process_buffer("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14) == 19
    assert process_buffer("bvwbjplbgvbhsrlpgdmjqwftvncz", 14) == 23
    assert process_buffer("nppdvjthqldpwncqszvftbrmjlhg", 14) == 23
    assert process_buffer("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 14) == 29
    assert process_buffer("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 14) == 26

    print(f"Part 2: {process_buffer(line, 14)}")
