import json


def parse(pair_data):
    return [tuple(json.loads(l) for l in pair.split("\n")) for pair in pair_data]


def compare(a: list | int, b: list | int, depth: int = 0) -> bool | None:
    # print(f"{'  ' * depth}- Comparing {a} vs {b}")
    match a, b:
        case int(), int():
            if a == b:
                return None
            return a < b
        case list(), list():
            for v_a, v_b in zip(a, b):
                if (result := compare(v_a, v_b, depth=depth + 1)) is not None:
                    return result
            else:
                # If no decision yet, check if one list was shorter
                return compare(len(a), len(b), depth=depth)
        case list(), int():
            return compare(a, [b], depth=depth + 1)
        case int(), list():
            return compare([a], b, depth=depth + 1)

    raise RuntimeError


def p1(packet_pairs: list[tuple]) -> int:
    sum_of_indices = 0
    for idx, (packet1, packet2) in enumerate(packet_pairs, start=1):
        # print(f"\n== Pair {idx} ==")
        if compare(packet1, packet2):
            sum_of_indices += idx
    return sum_of_indices


def p2(packet_pairs: list[tuple]):
    flattened_packets = [packet for pair in packet_pairs for packet in pair]
    smaller_than_2 = sum(compare(p, json.loads("[[2]]")) for p in flattened_packets)
    smaller_than_6 = sum(compare(p, json.loads("[[6]]")) for p in flattened_packets)

    # 1-based indexing, and [[2]] comes before [[6]] so add another extra index
    return (smaller_than_2 + 1) * (smaller_than_6 + 2)


if __name__ == '__main__':
    with open("input.txt") as file:
    # with open("test1.txt") as file:
        pairs = file.read().strip().split("\n\n")

    data = parse(pairs)

    print(f"Part 1: {p1(data)}")
    print(f"Part 2: {p2(data)}")
