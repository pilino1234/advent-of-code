import re


def parse_crates(line_data: list[str]):
    stack_nums = line_data.pop(-1)
    parsed_stacks = {}
    for stack_idx in stack_nums.split():
        parsed_stacks[stack_idx] = []

    for level, crates in enumerate(reversed(line_data)):
        for x_pos, crate_label in enumerate(crates):  # type: int, str
            if not crate_label.isalpha():
                continue
            stack_num = ((x_pos - 1) // 4) + 1
            # Sanity check. level is 0-indexed, so it should be equal to the length _before_ appending
            assert len(parsed_stacks[str(stack_num)]) == level
            parsed_stacks[str(stack_num)].append(crate_label)

    return parsed_stacks


def parse_instructions(lines: list[str]):
    pat = re.compile(r"^move (?P<move>\d+) from (?P<from>\d+) to (?P<to>\d+)$")
    return [pat.fullmatch(line) for line in lines]


def p1(stacks, instructions):
    for move in instructions:
        for _ in range(int(move["move"])):
            stacks[move["to"]].append(stacks[move["from"]].pop(-1))

    return "".join(stack[-1] for stack in stacks.values())


def p2(stacks, instructions):
    for move in instructions:
        to_move = int(move["move"])
        stacks[move["to"]].extend(stacks[move["from"]][-to_move:])
        del stacks[move["from"]][-to_move:]

    return "".join(stack[-1] for stack in stacks.values())


if __name__ == '__main__':
    with open("input.txt") as file:
        initial, instruction_lines = file.read().split("\n\n")

    crane_instructions = parse_instructions(instruction_lines.strip().split("\n"))

    crate_stacks = parse_crates(initial.split("\n"))
    print(f"Part 1: {p1(crate_stacks, crane_instructions)}")
    crate_stacks = parse_crates(initial.split("\n"))
    print(f"Part 2: {p2(crate_stacks, crane_instructions)}")
