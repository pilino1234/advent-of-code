from collections import defaultdict, Counter
from math import prod
from operator import mul, itemgetter, add
from typing import NamedTuple, Literal, Callable

from util import extract_all_numbers


class Monkey(NamedTuple):
    id: int
    items: list[int]
    operator: Callable[[int, int], int]
    operation_arg: int | None
    test_value: int
    if_true_pass_to: int
    if_false_pass_to: int

    def operation(self, old: int) -> int:
        return self.operator(old, old if self.operation_arg is None else self.operation_arg)


def parse(line_data: list[str]) -> dict[int, Monkey]:
    monkeys = {}

    for definition in line_data:
        definition = definition.split("\n")
        id_ = extract_all_numbers(definition[0])[0]
        items = extract_all_numbers(definition[1])

        operation_func = mul if "*" in definition[2] else add
        if nums := extract_all_numbers(definition[2]):
            operation_arg = nums[0]
        else:
            operation_arg = None

        test_value = extract_all_numbers(definition[3])[0]

        *_, if_true = definition[4].rsplit(" ")
        *_, if_false = definition[5].rsplit(" ")

        monkeys[id_] = Monkey(
            id=id_,
            items=items,
            operator=operation_func,
            operation_arg=operation_arg,
            test_value=test_value,
            if_true_pass_to=int(if_true),
            if_false_pass_to=int(if_false),
        )
    return monkeys


def business_round(monkeys: dict[int, Monkey], part: Literal[1, 2] = 1) -> dict[int, int]:
    inspection_counts = defaultdict(int)

    modulus = prod(monkey.test_value for monkey in monkeys.values())

    for id_, monkey in monkeys.items():

        for item in monkey.items:
            inspection_counts[id_] += 1

            # Inspect item
            worry_level = item
            worry_level = monkey.operation(worry_level)

            if part == 1:
                # Relief
                worry_level //= 3
            worry_level %= modulus  # Part 2

            # Test and throw
            if worry_level % monkey.test_value == 0:
                monkeys[monkey.if_true_pass_to].items.append(worry_level)
            else:
                monkeys[monkey.if_false_pass_to].items.append(worry_level)
        monkey.items.clear()

    return inspection_counts


if __name__ == '__main__':
    with open("input.txt") as file:
        monkey_defs = file.read().strip().split("\n\n")

    m = parse(monkey_defs)

    total_counts = Counter()
    for _ in range(20):
        total_counts.update(business_round(m))
    monkey_business_level = prod(map(itemgetter(1), total_counts.most_common(2)))
    print(f"Part 1: {monkey_business_level}")

    m = parse(monkey_defs)
    total_counts = Counter()
    for _ in range(10_000):
        total_counts.update(business_round(m, part=2))
    monkey_business_level = prod(map(itemgetter(1), total_counts.most_common(2)))
    print(f"Part 2: {monkey_business_level}")
