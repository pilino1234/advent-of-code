from dataclasses import dataclass
from typing import Self


@dataclass
class Node:
    name: str
    parent: Self | None

    def __len__(self):
        raise NotImplementedError


@dataclass
class Dir(Node):
    children: dict[str, Self]

    def __len__(self):
        return sum(len(child) for child in self.children.values())


@dataclass
class File(Node):
    size: int

    def __len__(self):
        return self.size


def parse_terminal(output_lines) -> Dir:
    root = Dir(name="/", parent=None, children={})
    cwd = root

    for line in output_lines:  # type: str
        if line.startswith("$"):
            # Command
            command = line.strip("$ ")
            command, *args = command.split()

            if command == "cd":
                assert len(args) == 1
                dest = args.pop()
                if dest == "/":
                    cwd = root
                elif dest == "..":
                    cwd = cwd.parent
                else:
                    cwd = cwd.children[dest]

            elif command == "ls":
                ...
        else:
            # Output
            meta, name = line.split()  # type: str, str

            if name in cwd.children:
                print(f"Already know about {name} in {cwd.name}, skipping")
                continue

            if meta == "dir":
                cwd.children[name] = Dir(name, parent=cwd, children={})
            elif meta.isnumeric():
                cwd.children[name] = File(name, parent=cwd, size=int(meta))
            else:
                raise RuntimeError

    return root


def p1(fs_root: Dir):
    result = []

    to_check: list[Dir] = [fs_root]
    while to_check:
        cwd = to_check.pop(0)
        to_check.extend(child for child in cwd.children.values() if isinstance(child, Dir))

        if len(cwd) < 100_000:
            result.append(len(cwd))

    return sum(result)


def p2(fs_root: Dir, total_space: int = 70_000_000, needed_space: int = 30_000_000):
    starting_space: int = len(fs_root)
    space_to_free = starting_space - (total_space - needed_space)

    dir_sizes = {}
    to_check: list[Dir] = [fs_root]
    while to_check:
        cwd = to_check.pop(0)
        to_check.extend(child for child in cwd.children.values() if isinstance(child, Dir))

        if len(cwd) > space_to_free:
            dir_sizes[cwd.name] = len(cwd)

    return min(dir_sizes.values())


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    filesystem = parse_terminal(lines)
    print(len(filesystem))

    print(f"Part 1: {p1(filesystem)}")
    print(f"Part 2: {p2(filesystem)}")
