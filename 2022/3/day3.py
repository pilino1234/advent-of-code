from typing import List


def priority(item: str) -> int:
    if item.isupper():
        return ord(item) - 38
    else:
        return ord(item) - 96


def p1(sacks):
    priorities = 0

    for sack in sacks:
        comp1, comp2 = sack[:len(sack)//2], sack[len(sack)//2:]
        items_in_both = set(comp1).intersection(comp2)
        assert len(items_in_both) == 1
        priorities += priority(items_in_both.pop())

    return priorities


def groups(seq: List, n: int = 3):
    return (seq[idx: idx + n] for idx in range(0, len(seq), n))


def p2(sacks):
    priorities = 0

    for g1, g2, g3 in groups(sacks, 3):
        common = set(g1).intersection(g2).intersection(g3)
        priorities += priority(common.pop())

    return priorities


if __name__ == '__main__':
    with open("input.txt") as file:
        lines = [line.strip() for line in file]

    print(f"Part 1: {p1(lines)}")
    print(f"Part 2: {p2(lines)}")
