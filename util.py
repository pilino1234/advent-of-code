import re
from typing import List, Tuple, Iterable


def to_ints(list_of_strings: List[str]) -> List[int]:
    """Convert all elements in a list to integers"""
    return list(map(int, list_of_strings))


def extract_all_numbers(input_line: str) -> List[int]:
    """
    Extract all numbers from a string

        asdf123 -> [123]
        asdf123qwer456 -> [123, 456]
    """
    return list(map(int, re.findall(r'-?\d+', input_line)))


def add_coord(pa: Tuple[int, int], pb: Tuple[int, int]) -> Tuple[int, int]:
    """Element-wise addition for tuples"""
    return pa[0] + pb[0], pa[1] + pb[1]


def prod(iterable: Iterable, start=1) -> float:
    """
    Calculate the product of all elements in the input iterable.
    """
    result = start
    for i in iterable:
        result *= i
    return result
