from math import sqrt


def coprocess(instructions):
    registers = {letter: 0 for letter in "abcdefgh"}

    pc = 0

    mul_count = 0

    while 0 <= pc < len(instructions):
        parts = instructions[pc].split()
        op = parts[0]
        p1 = parts[1]
        p2 = parts[2]
        if p2.isalpha():
            p2 = registers[p2]
        p2 = int(p2)

        if op == "set":
            registers[p1] = p2
        elif op == "sub":
            registers[p1] -= p2
        elif op == "mul":
            registers[p1] *= p2
            mul_count += 1
        elif op == "jnz":

            if p1.isalpha():
                p1 = registers[p1]

            if p1 != 0:
                pc += int(p2)
                continue

        pc += 1

    return mul_count


# Reverse engineered from assembly (see notes.txt)
def part2():
    b = (84 * 100) + 100000
    c = b + 17000
    factor_counter = 0

    while True:
        found_factors = False

        for fac1 in range(2, int(sqrt(b)+1)):
                if b % fac1 == 0:
                    found_factors = True

        if found_factors:
            factor_counter += 1

        if b == c:
            break

        b += 17

    return factor_counter


# A cleaned-up version of part 2
def part2_cleaned():
    b = 108400

    factor_counter = 0

    for _ in range(1001):
        for fac in range(2, int(sqrt(b) + 1)):
            if b % fac == 0:
                factor_counter += 1
                break

        b += 17

    return factor_counter


if __name__ == '__main__':
    with open("input.txt") as file:
        read_lines = [line.strip() for line in file.readlines()]

    print(coprocess(read_lines))

    plain_p2 = part2()
    clean_p2 = part2_cleaned()
    assert plain_p2 == clean_p2

    print(clean_p2)


